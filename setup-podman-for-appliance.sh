#!/bin/bash

# export REGISTRY=default-route-openshift-image-registry.apps.test-cluster.redhat.com
export REGISTRY=localhost:5000
export REGISTRY_PATH=mergetb/portal/services/merge
export XDC_PATH=mergetb/portal/services/merge/xdc
export TAG=latest

export DOCKER=podman
export DOCKER_PUSH_ARGS=--tls-verify=false

unset KUBECONFIG
