from mergexp import *

def node(name):
    return net.node(name, metal == True)

def virt1(name):
    return net.node(name, metal == False)

def virt2(name):
    return net.node(name, memory.capacity >= gb(32))

net = Network('test')

n0 = node("n0")
n1 = virt1("v1")
n2 = virt1("v2")
n3 = virt2("v3")

net.connect([n0, n1], latency == ms(10))
net.connect([n1, n2], latency == ms(10))
net.connect([n2, n3], latency == ms(10))
net.connect([n3, n0], latency == ms(10))
net.connect([n0, n1, n2, n3], latency == ms(10))

experiment(net)
