from mergexp import *

def node(name):
    return net.node(name, metal == True)

net = Network('test')

n0 = node("n0")
n1 = node("n1")
n2 = node("n2")
n3 = node("n3")

net.connect([n0, n1])
net.connect([n1, n2])
net.connect([n2, n3])
net.connect([n3, n0])
net.connect([n0, n1, n2, n3])

experiment(net)
