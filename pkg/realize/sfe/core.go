package sfe

import (
	"sort"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"

	xpa "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/xir/v0.3/go"
)

type Cost struct {
	Exclusive  float64
	MTA        float64
	Emulation  float64
	Simulation float64
}

type Host struct {
	Device *xir.Device
	Cost   Cost
	Order  uint
	Guests []*Guest
}

type Guest struct {
	Device *xir.Device
}

func Hosts(device *xir.Device) []*Host {

	log.Info("building host list")

	hs := hosts(device, 0)
	sort.SliceStable(hs, func(i, j int) bool {
		return natural.Less(hs[i].Device.Id(), hs[j].Device.Id())
	})

	for i, h := range hs {
		h.Order = uint(i)
	}
	log.Infof("found %d hosts", len(hs))

	return hs

}

func hosts(device *xir.Device, order uint) []*Host {

	if device.Visited {
		return nil
	}
	device.Visited = true

	var hs []*Host

	r := device.Resource()

	if r.HasRole(xir.Role_TbNode) || r.HasRole(xir.Role_Hypervisor) {

		hs = append(hs, &Host{
			Device: device,
			Order:  order,
		})

	}

	if !traversable(r) {
		return hs
	}

	for _, nbr := range infranetNeighbors(r, device) {

		hs = append(hs, hosts(nbr.Remote.Device, order)...)

	}

	return hs

}

func traversable(r *xir.Resource) bool {

	return r.HasRole(xir.Role_InfraSwitch, xir.Role_Gateway)

}

func infranetNeighbors(r *xir.Resource, x *xir.Device) []*xir.Neighbor {

	var ns []*xir.Neighbor

	for _, ifx := range x.Interfaces {

		/*
			if ifx.Data.(*xir.Port).Role != xir.LinkRole_XpLink &&
				ifx.Data.(*xir.Port).Role != xir.LinkRole_GatewayLink {
				continue
			}
		*/

		if ifx.Edge != nil {
			for _, edge := range ifx.Edge.Connection.Edges {
				if edge.Interface != nil {

					if edge.Interface.Device.Visited {
						continue
					}

					if !edge.Interface.Device.Resource().HasRole(
						xir.Role_Gateway,
						xir.Role_InfraSwitch,
						xir.Role_TbNode,
					) {
						continue
					}

					ns = append(ns, &xir.Neighbor{
						Local:  ifx,
						Remote: edge.Interface,
					})

				}
			}
		}

	}

	return ns

}

func Guests(net *xir.Topology) []*Guest {

	log.Info("building guest list")

	var gs []*Guest

	ComputeInterfaceConstraints(net)

	for _, x := range net.Devices {
		gs = append(gs, &Guest{
			Device: x,
		})
	}

	return gs

}

func Sort(hs []*Host) ([]*Host, []*Host) {

	log.Info("sorting hosts")

	var metal, virt []*Host
	for _, x := range hs {

		r := x.Device.Data.(*xir.Resource)

		// Don't even consider resources marked as no-alloc.
		if r.HasAllocMode(xir.AllocMode_NoAlloc) {
			continue
		}

		if r.HasAllocMode(xir.AllocMode_Physical) {
			metal = append(metal, x)
		}

		has_virtual := r.HasAllocMode(xir.AllocMode_Virtual)
		is_hypervisor := r.HasRole(xir.Role_Hypervisor)

		// Hypervisor without AllocMode_Virtual is a feasible configuration.
		// For example, a facility operator may want to dynamically disable
		// virtual machines on a node for a period of time to preserve it for
		// bare metal allocations.
		if !has_virtual && is_hypervisor {
			log.Debugf(
				"resource %s has Role_Hypervisor but does not have AllocMode_Virtual. Not using as hypervisor",
				r.Id,
			)
		}

		// On the other hand, AllocMode_Virtual without a Hypervisor role is
		// nonsensical and very likely a modeling error. Throw a warning
		if has_virtual && !is_hypervisor {
			log.Warnf(
				"resource %s has AllocMode_Virtual but does not have Role_Hypervisor."+
					" Not using as hypervisor. Consider updating your model to add Role_Hypervisor",
				r.Id,
			)
		}

		if has_virtual && is_hypervisor {
			virt = append(virt, x)
		}

	}

	// metal
	sort.SliceStable(metal, func(i, j int) bool {
		if metal[i].Cost.Exclusive == metal[j].Cost.Exclusive {
			return metal[i].Order < metal[j].Order
		}
		return metal[i].Cost.Exclusive < metal[j].Cost.Exclusive
	})

	// virt
	sort.SliceStable(virt, func(i, j int) bool {
		if virt[i].Cost.MTA == virt[j].Cost.MTA {
			return virt[i].Order < virt[j].Order
		}
		return virt[i].Cost.MTA < virt[j].Cost.MTA
	})

	return metal, virt

}

func (g *Guest) WantsMetal() bool {

	n := g.Device.Data.(*xir.Node)

	if n.Metal != nil {
		// metal == true
		if n.Metal.Value && n.Metal.Op == xir.Operator_EQ {
			return true
		}
		// metal != false
		if !n.Metal.Value && n.Metal.Op == xir.Operator_NE {
			return true
		}

	}

	if n.Virt != nil {
		// virt == false
		if !n.Virt.Value && n.Virt.Op == xir.Operator_EQ {
			return true
		}
		// virt != true
		if n.Virt.Value && n.Virt.Op == xir.Operator_NE {
			return true
		}
	}

	return false

}

func applyAllocs(hosts []*Host, a *xpa.AllocationTable) {

	for _, h := range hosts {

		allocs, ok := a.Resource[h.Device.Id()]
		if !ok {
			continue
		}
		for _, alloc := range allocs.Value {
			h.Guests = append(h.Guests, &Guest{
				&xir.Device{
					Data: alloc.Model,
				},
			})
			//log.Printf("%v", alloc)
		}

	}

}
