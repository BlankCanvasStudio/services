package sfe

import (
	"fmt"
	"sort"
	"strings"

	"github.com/maruel/natural"

	"gitlab.com/mergetb/xir/v0.3/go"
)

func (h Host) String() string {

	return fmt.Sprintf("%s $%.0f/%.0f %d",
		h.Device.Id(),
		h.Cost.Exclusive,
		h.Cost.MTA,
		h.Order,
	)

}

func (h Guest) String() string {

	return fmt.Sprintf("%s",
		h.Device.Id(),
	)

}

func (e *Embedding) String() string {

	if e == nil {
		return "nil"
	}

	// Print Nodes

	s := fmt.Sprintf("NODES (%d)\n", len(e.Nodes))

	nodes := make([]string, 0, len(e.Nodes))
	for n := range e.Nodes {
		nodes = append(nodes, n)
	}
	sort.Sort(natural.StringSlice(nodes))

	for _, g := range nodes {
		s += fmt.Sprintf("%s ~> %s\n", g, e.Nodes[g].Resource.Id())
	}

	// Sort Links

	type kv struct {
		Key   string
		Value *xir.Connection
	}

	ss := make([]kv, 0, len(e.Links))
	for k := range e.Links {
		ss = append(ss, kv{k.Label(), k})
	}
	sort.Slice(ss,
		func(i, j int) bool {
			return natural.Less(ss[i].Key, ss[j].Key)
		},
	)

	// Print Links

	s += fmt.Sprintf("LINKS (%d)\n", len(e.Links))

	for _, x := range ss {
		s += fmt.Sprintln(x.Key)
		s += e.Links[x.Value].ToString("  ")
	}

	s += fmt.Sprintln("")

	return s

}

func (e Embedding) Dump() {
	fmt.Println(e.String())
}

func FormatUnits(s string) string {

	// get rid of iB in GiB,MiB... and 1G instead of 1 G
	s = strings.Replace(s, " ", "", -1)
	s = strings.Replace(s, "iB", "", -1)
	s = strings.Replace(s, "B", "", -1)
	return s

}

func (wp *Waypoint) Dump() {

	wp.dump("")

}

func (wp *Waypoint) dump(prefix string) {

	if wp.In != nil {
		r := wp.In.Ref()
		prefix += r.Element + "." + fmt.Sprintf("%d", r.Index) + "~"
	}
	for out, x := range wp.Out {
		r := out.Interface.Ref()
		x.dump(
			prefix + r.Element + "." + fmt.Sprintf("%d", r.Index) + " ",
		)
	}
	if wp.In.Device != nil && wp.In.Device.Data.(*xir.Resource).HasRole(
		xir.Role_TbNode,
		xir.Role_NetworkEmulator,
		xir.Role_StorageServer,
		xir.Role_InfraServer,
		xir.Role_InfrapodServer,
	) {

		fmt.Println(prefix[:len(prefix)-1])

	}

}

func (wp *Waypoint) JSONDump() {
	fmt.Println(wp.jsondump(""))
}

// Waypoints is a recursive data structure, so Marshall won't work on it
func (wp *Waypoint) jsondump(indent string) string {
	indent_per := "  "

	if wp == nil {
		return "{nil}"
	}

	s := fmt.Sprintf("{\n%s\"In\": \"%s:%s\",", indent+indent_per, wp.In.Device.Id(), wp.In.PortName())
	s += fmt.Sprintf("\n%s\"Out\": [", indent+indent_per)

	for out, x := range wp.Out {
		s += fmt.Sprintf(
			"\n%s{\n%s\"%s:%s\": %s\n%s},",
			indent+indent_per+indent_per,
			indent+indent_per+indent_per+indent_per,
			out.Interface.Device.Id(), out.Interface.PortName(),
			x.jsondump(indent+indent_per+indent_per+indent_per),
			indent+indent_per+indent_per,
		)
	}

	if len(wp.Out) == 0 {
		s += fmt.Sprintf("]")
	} else {
		s = s[0 : len(s)-1]
		s += fmt.Sprintf("\n%s]", indent+indent_per)
	}

	s += fmt.Sprintf("\n%s}", indent)

	return s
}

func StringTPAs(tbx *xir.Topology) string {

	s := ""

	for _, dev := range tbx.Devices {
		s += fmt.Sprintf("%s: %s\n", dev.Id(), TPA(dev.Resource().TPA))

		for _, ifx := range dev.Interfaces {
			s += fmt.Sprintf("%s.%s", dev.Id(), ifx.PortName())
		}
	}

	return s
}

func (r Route) String() string {
	return fmt.Sprintf("%s/%d via %s.%d [%d]",
		r.Destination,
		r.Mask,
		r.Dev.Device.Id(),
		r.Dev.Ref().Index,
		r.Hops,
	)
}

func (r Route) StringWithId(id string) string {
	return fmt.Sprintf("%s (%s/%d) via %s.%d [%d]",
		id,
		r.Destination,
		r.Mask,
		r.Dev.Device.Id(),
		r.Dev.Ref().Index,
		r.Hops,
	)
}

func (rt RoutingTable) String(tbx *xir.Topology) string {

	s := ""

	tpa_map := make(map[TPA]string)

	if tbx != nil {
		for _, d := range tbx.Devices {
			tpa_map[TPA(d.Resource().TPA)] = d.Id()

			for _, ifx := range d.Interfaces {
				tpa_map[TPA(ifx.Port().TPA)] = fmt.Sprintf("%s.%s", d.Id(), ifx.PortName())
			}
		}
	}

	for node, routes := range rt {

		s += fmt.Sprintf("%s:\n", node)
		for _, route := range routes {
			id, ok := tpa_map[route.Destination]

			if ok {
				s += fmt.Sprintf("  %s\n", route.StringWithId(id))

			} else {
				s += fmt.Sprintf("  %s\n", route)
			}

		}
	}

	return s
}

func (rt RoutingTable) Dump(tbx *xir.Topology) {
	fmt.Print(rt.String(tbx))
}

type DevAddrMap map[*xir.Device]TPA
type IfxAddrMap map[*xir.Interface]TPA

func (dam DevAddrMap) Dump() {

	for k, v := range dam {
		fmt.Printf("%4s: %s\n", k.Id(), v)
	}

}

func (iam IfxAddrMap) Dump() {

	for k, v := range iam {
		fmt.Printf("%4s.%d: %s\n", k.Device.Id(), k.Ref().Index, v)
	}

}
