package sfe

import (
	"fmt"
	"math"

	rz "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/xir/v0.3/go"
)

func min64(a, b uint64) uint64 {
	if a < b {
		return a
	}
	return b
}

func min32(a, b uint32) uint32 {
	if a < b {
		return a
	}
	return b
}

func ResolveNode(
	r *xir.Resource, s *xir.Node, as []*xir.ResourceAllocation, mzid string,
) (*xir.ResourceAllocation, rz.Diagnostics) {

	var ds rz.Diagnostics

	a := xir.AggregateResourceAllocations(as)

	var d rz.Diagnostics
	ra := new(xir.ResourceAllocation)
	ra.Mzid = mzid
	ra.Facility = r.Facility
	ra.Resource = r.Id
	ra.Node = s.Id
	ra.Model = s

	ds = append(ds, ResolveImage(r, s)...)

	// default: 1 core
	if s.Proc == nil {
		s.Proc = &xir.ProcSpec{
			Cores: &xir.Uint32Constraint{
				Op:    xir.Operator_GE,
				Value: 1,
			},
		}
	}

	ra.Procs, d = ResolveProc(r, s.Proc, a.Procs)
	ds = append(ds, d...)

	// default: 1 GB memory
	if s.Memory == nil {
		s.Memory = &xir.MemorySpec{
			Capacity: &xir.Uint64Constraint{
				Op:    xir.Operator_GE,
				Value: 1 << 30,
			},
		}
	}

	ra.Memory, d = ResolveMemory(r, s.Memory, a.Memory)
	ds = append(ds, d...)

	if s.Disks != nil {
		ra.Disks, d = ResolveDisks(r, s.Disks, a.Disks)
		ds = append(ds, d...)
	}

	if s.NIC != nil {
		ra.NICs, d = ResolveNIC(r, s.NIC, a.NICs)
		ds = append(ds, d...)
	}

	return ra, ds

}

func ResolveImage(r *xir.Resource, s *xir.Node) rz.Diagnostics {

	var ds rz.Diagnostics

	if s.Image == nil {

		// no image specified and no default
		if r.OS == nil || r.OS.DefaultImage == "" {
			ds = append(ds, &rz.Diagnostic{
				Message: "no image specified and no default image",
				Level:   rz.DiagnosticLevel_Error,
				Guest:   s.Id,
				Host:    r.Id,
			})
			return ds
		}

		// apply default image
		s.Image = &xir.StringConstraint{
			Op:    xir.Operator_EQ,
			Value: r.OS.DefaultImage,
		}

	}

	return ds

}

// resolve a processor specification embodied by a set of constraints against a
// resource definition containing a set of processors and return a set of
// virtual processors as a unit of allocation, given an existing set of
// allocations
func ResolveProc(
	r *xir.Resource, s *xir.ProcSpec, a *xir.ProcAllocation,
) (*xir.ProcAllocation, rz.Diagnostics) {

	var ds rz.Diagnostics
	result := new(xir.ProcAllocation)

	// determine how many cores to allocate
	cores := uint32(1)
	if s.Cores != nil {
		switch s.Cores.Op {
		case xir.Operator_GT, xir.Operator_GE, xir.Operator_EQ:
			cores = s.Cores.Value
		}
	}

	// compute the number of cores per socket (stride)
	stride := cores
	if s.Sockets != nil {

		if int(s.Sockets.Value) > len(r.Procs) {
			msg := fmt.Sprintf("processor sockets oversubscribed: %d > %d",
				s.Sockets.Value, len(r.Procs),
			)
			ds = append(ds, Oversubscribed(msg))
			return nil, ds
		}

		switch s.Cores.Op {
		case xir.Operator_GT, xir.Operator_GE, xir.Operator_EQ:
			stride = uint32(math.Ceil(float64(cores) / float64(s.Sockets.Value)))
		}

	}

	// attempt to place cores
	for i, p := range r.Procs {

		avail := p.Cores
		if a != nil {
			used, ok := a.Alloc[uint32(i)]
			if ok {
				avail -= used.Cores
			}
		}

		var alloc uint32
		if avail > stride {
			alloc = min32(stride, cores)
		} else {
			alloc = min32(avail, cores)
		}

		result.Allocate(uint32(i), &xir.SocketAllocation{Cores: alloc})
		cores -= alloc

		if cores <= 0 {
			break
		}

	}

	if cores > 0 {
		ds = append(ds, Oversubscribed("processor cores"))
		return nil, ds
	}

	return result, ds

}

func ResolveMemory(
	r *xir.Resource, s *xir.MemorySpec, a *xir.MemoryAllocation,
) (*xir.MemoryAllocation, rz.Diagnostics) {

	var ds rz.Diagnostics
	result := new(xir.MemoryAllocation)

	// determine how much memory to allocate
	capacity := xir.MB(512)
	if s.Capacity != nil {
		switch s.Capacity.Op {
		case xir.Operator_GT, xir.Operator_GE, xir.Operator_EQ:
			capacity = s.Capacity.Value
		}
	}

	stride := capacity
	if s.Modules != nil {

		if int(s.Modules.Value) > len(r.Memory) {
			msg := fmt.Sprintf("memory channels oversubscribed: %d > %d",
				s.Modules.Value, len(r.Memory),
			)
			ds = append(ds, Oversubscribed(msg))
			return nil, ds
		}

		switch s.Modules.Op {
		case xir.Operator_GT, xir.Operator_GE, xir.Operator_EQ:
			stride = uint64(math.Ceil(float64(capacity) / float64(s.Capacity.Value)))
		}

	}

	// attempt to place memory
	for i, d := range r.Memory {

		avail := d.Capacity
		if a != nil {
			used, ok := a.Alloc[uint32(i)]
			if ok {
				// watch out for underflow
				if used.Capacity > avail {
					avail = 0
				} else {
					avail -= used.Capacity
				}
			}
		}

		var alloc uint64
		if avail > stride {
			alloc = min64(stride, capacity)
		} else {
			alloc = min64(avail, capacity)
		}

		result.Allocate(uint32(i), &xir.DimmAllocation{Capacity: alloc})
		capacity -= alloc

		if capacity <= 0 {
			break
		}

	}

	if capacity > 0 {
		ds = append(ds, Oversubscribed("memory capacity"))
		return nil, ds
	}

	return result, ds

}

func ResolveDisks(
	r *xir.Resource, s *xir.DiskSpec, a *xir.DisksAllocation,
) (*xir.DisksAllocation, rz.Diagnostics) {

	var ds rz.Diagnostics
	result := new(xir.DisksAllocation)

	// determine how much disk to allocate
	capacity := xir.MB(512)
	if s.Capacity != nil {
		switch s.Capacity.Op {
		case xir.Operator_GT, xir.Operator_GE, xir.Operator_EQ:
			capacity = s.Capacity.Value
		}
	}

	stride := capacity
	if s.Disks != nil {

		if int(s.Disks.Value) > len(r.Disks) {
			msg := fmt.Sprintf("disks oversubscribed: %d > %d",
				s.Disks.Value, len(r.Disks),
			)
			ds = append(ds, Oversubscribed(msg))
			return nil, ds
		}

		switch s.Disks.Op {
		case xir.Operator_GT, xir.Operator_GE, xir.Operator_EQ:
			stride = uint64(math.Ceil(float64(capacity) / float64(s.Capacity.Value)))
		}

	}

	// attempt to place memory
	for i, d := range r.Disks {

		avail := d.Capacity
		if a != nil {
			used, ok := a.Alloc[uint32(i)]
			if ok {
				avail -= used.Capacity
			}
		}

		var alloc uint64
		if avail > stride {
			alloc = min64(stride, capacity)
		} else {
			alloc = min64(avail, capacity)
		}

		result.Allocate(uint32(i), &xir.DiskAllocation{Capacity: alloc})
		capacity -= alloc

		if capacity <= 0 {
			break
		}

	}

	if capacity > 0 {
		ds = append(ds, Oversubscribed("disk capacity"))
		return nil, ds
	}

	return result, ds

}

// ResolveNIC: find the NICs/Ports that we can allocate to the requested NICSpec
// return the resulting xir.NICsAllocation
func ResolveNIC(
	r *xir.Resource, s *xir.NICSpec, a *xir.NICsAllocation,
) (*xir.NICsAllocation, rz.Diagnostics) {

	if s == nil {
		return nil, nil
	}

	var ds rz.Diagnostics
	result := new(xir.NICsAllocation)

	// build a list of all possible nic candidates' indicies  matching spec's constraints
	var nic_ids []int
	for ni, nic := range r.NICs {
		if nic.Satisfies(s) {
			nic_ids = append(nic_ids, ni)
		}
	}

	ports := len(s.Ports)

	for _, p := range s.Ports {

		// if the ask is for nothing, guarantee at least 100mbps
		if p.Capacity == nil {
			p.Capacity = &xir.Uint64Constraint{
				Op:    xir.Operator_EQ,
				Value: xir.Mbps(100),
			}
		}

		//TODO check queues
		for _, ni := range nic_ids {
			for pi, np := range r.NICs[ni].Ports {

				if np.Role != xir.LinkRole_XpLink {
					continue
				}

				if np.Satisfies(p) {

					avail := np.Capacity
					if a != nil {
						na, ok := a.Alloc[uint32(ni)]
						var pa *xir.PortAllocation
						if ok {
							pa, ok = na.Alloc[uint32(pi)]
							if ok {
								avail -= pa.Capacity
							}
						}
					}

					if avail >= p.Capacity.Value {
						result.Allocate(
							uint32(ni),
							uint32(pi),
							&xir.PortAllocation{
								Capacity: p.Capacity.Value,
							},
						)

						ports--

						break

					}

				}
			}
		}
		if ports <= 0 {
			break
		}
	}

	if ports > 0 {
		ds = append(ds, Oversubscribed("network ports"))
		return nil, ds
	}

	return result, ds

}
