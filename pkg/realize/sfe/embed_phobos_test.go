package sfe

import (
	"testing"

	"gitlab.com/mergetb/xir/v0.3/go"
)

/*
	Helpers
*/

func initPhobosFacility() (*xir.Topology, *xir.Device, error) {
	tbx, err := loadFacility("facilities/phobos.xir")
	if err != nil {
		return nil, nil, err
	}
	AssignTPAsInfraNet(0x004D000000000000, tbx)
	AssignTPAsXpNet(0x004E000000000000, tbx)

	return tbx, tbx.Device("ifr"), nil
}

func testPhobos_Experiment(t *testing.T, result ExpectedResult, expected, filename string) {
	tbx, root, err := initPhobosFacility()
	if err != nil {
		t.Errorf("%+v", err)
		return
	}

	testExperiment(t, tbx, root, result, expected, filename)
}
