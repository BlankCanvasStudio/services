package sfe

import (
	log "github.com/sirupsen/logrus"
	"sort"

	"gitlab.com/mergetb/xir/v0.3/go"
)

// This function iterates through the set of connections (links) in a model
// looking for port capacity constraints, and copies the constraint into the
// sockets and NIC structures of the associated xir.Node endpoints
//
// Constraints other than capacity that affect interface selection may need to be processed
// here as well

func ComputeInterfaceConstraints(tbx *xir.Topology) {

	ports := make(map[*xir.Node]map[int32]*xir.PortSpec)

	for _, cnx := range tbx.Connections {
		for _, e := range cnx.Edges {
			if e.Interface != nil {
				// copy into the socket PortSpec
				sock := e.Interface.Data.(*xir.Socket)
				if sock.Port == nil {
					sock.Port = &xir.PortSpec{}
				}

				// TODO: constraints other than capacity that need to be pulled from the cnx?
				c := cnx.Data.(*xir.Link).Capacity
				if c != nil {
					sock.Port.Capacity = c
				}

				// save for future copy into the node NICSpec
				node := e.Interface.Device.Data.(*xir.Node)
				if _, ok := ports[node]; !ok {
					ports[node] = make(map[int32]*xir.PortSpec)
				}

				ports[node][sock.Index] = sock.Port
			}
		}
	}

	// copy into the node NICSpec
	for node, smap := range ports {
		// parse the existing port specifications in node.NICSpec, favoring
		// them over ones derived above from connections
		if node.NIC != nil {
			for pidx, port := range node.NIC.Ports {
				log.Warnf("'%s'.NICSpec.Ports[%d] defined by model; using it over spec inferred from tbx connection",
					node.Id, pidx,
				)
				smap[int32(pidx)] = port
			}

			// blow away the old array, will be re-generated below
			node.NIC = nil
		}

		// need to iterate the sockets in order so that the node's PortSpec list
		// is populated in the same order as indicated by the socket.Index field
		idx := make([]int, 0)
		for sidx, _ := range smap {
			idx = append(idx, int(sidx))
		}
		sort.Ints(idx)

		for _, sidx := range idx {
			// create a copy
			pspec := new(xir.PortSpec)
			*pspec = *smap[int32(sidx)]

			if node.NIC == nil {
				node.NIC = &xir.NICSpec{
					Ports: []*xir.PortSpec{},
				}
			}

			node.NIC.Ports = append(node.NIC.Ports, pspec)
		}
	}

}
