package sfe

import (
	"math/rand"
	"runtime"
	"sync"
	"time"

	//log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/xir/v0.3/go"
)

const (
	SwitchMask = uint8(32)
	HostMask   = uint8(16)
)

// A RoutingTable associates the name of a host with a set of routes.
type RoutingTable map[string][]Route
type Route struct {
	Dev         *xir.Interface
	Destination TPA
	Mask        uint8
	Hops        uint8
	NextHop     *xir.Interface
	Connection  *xir.Connection
}
type RouteMessage struct {
	Node string
	Route
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func (rt RoutingTable) GetRandomShortestRouteTo(host string, destination TPA) *Route {

	result := rt.GetShortestRoutesTo(host, destination)

	switch len(result) {
	case 0:
		return nil
	case 1:
		return &result[0]
	default:
		return &result[rand.Intn(len(result))]
	}
}

func (rt RoutingTable) GetShortestRoutesTo(host string, destination TPA) []Route {

	var result []Route
	var min_hops uint8 = 255

	for _, r := range rt[host] {
		if r.Contains(destination) {
			if r.Hops < min_hops {
				min_hops = r.Hops
				result = nil
			}

			if r.Hops == min_hops {
				result = append(result, r)
			}

			// As the routes are in sorted order by the numbers of hops,
			// we can exit early if we find something bigger
			if r.Hops > min_hops {
				break
			}
		}
	}

	return result
}

func (rt RoutingTable) GetRouteUsing(ifx *xir.Interface) *Route {

	for _, r := range rt[ifx.Device.Id()] {

		if ifx == r.Dev {
			return &r
		}

	}

	return nil

}

func (rt RoutingTable) GetRouteTo(host string, destination TPA) []Route {

	var result []Route

	for _, r := range rt[host] {

		if r.Contains(destination) {
			result = append(result, r)
		}

	}

	return result

}

func (r Route) Contains(x TPA) bool {

	m := (uint64(1)<<(64-r.Mask) - 1) << r.Mask
	return m&uint64(r.Destination) == m&uint64(x)

}

func Equals(x, other Route) bool {

	return x.Destination == other.Destination &&
		x.Mask == other.Mask &&
		x.Dev == other.Dev

}

func Shorter(x, other Route) bool {

	return x.Destination == other.Destination &&
		x.Mask == other.Mask &&
		x.Hops < other.Hops

}

func BuildXpRoutingTable(tbx *xir.Topology) RoutingTable {

	return BuildRoutingTable(
		tbx,
		[]xir.Role{
			xir.Role_XpSwitch,
			xir.Role_Gateway,
		},
		[]xir.Role{
			xir.Role_TbNode,
			xir.Role_NetworkEmulator,
		},
		xir.LinkRole_XpLink,
	)

}

func BuildInfraRoutingTable(tbx *xir.Topology) RoutingTable {

	return BuildRoutingTable(
		tbx,
		[]xir.Role{
			xir.Role_InfraSwitch,
			//xir.Role_Gateway, TODO probably will need this if/when we want to support multi-site experiments
		},
		[]xir.Role{
			xir.Role_TbNode,
			xir.Role_NetworkEmulator,
			xir.Role_StorageServer,
			xir.Role_InfrapodServer,
			xir.Role_InfraServer,
			xir.Role_MinIOHost,
			xir.Role_SledHost,
			xir.Role_RallyHost,
			xir.Role_Hypervisor,
		},
		xir.LinkRole_InfraLink,
	)

}

/*
	Routes found on switches:
		- All dist=1 routes to hosts under the switch
		- For each interface:
			- The shortest route to all other switches
	Routes found on hosts:
		- For each interface:
			- The shortest route to all switches

	The algorithm to compute the routing table is basically memoized, parallelized breadth first search:

		1. Add a route from every device to each of its neighbors
		2. Add a route from every device to each of its neighbor's most recently found destinations,
			as long as we haven't seen the destination before
		3. Repeat 2 until we find no more new routes

		To paralellize this, during step 1 and 2, each device's routes can be computed independently,
		as long as we block between invocations of step 1 and 2

		This is memoized, as we don't call breadth first search on every device.
		Instead, we essentially save the results of a device's breadth first search of a particular distance,
		and reuse its answers instead of recursively calling breadth first search on the neighbor.

	As a bonus, the routes are returned in sorted order by number of hops.
*/

func BuildRoutingTable(
	tbx *xir.Topology,
	switchRoles, hostRoles []xir.Role,
	linkRole xir.LinkRole,
) RoutingTable {

	devices := tbx.Select(func(n *xir.Device) bool {
		return n.Resource().HasRole(append(switchRoles, hostRoles...)...)
	})

	table := make(RoutingTable)
	added := true

	for dist := uint8(1); added; dist++ {
		table, added = updateRoutingTableByDist(devices, table, dist, switchRoles, hostRoles, linkRole)
	}

	return table
}

/*
	updateRoutingTableByDist is just code to parallelize getRoutesOfDist
*/
func updateRoutingTableByDist(
	devices []*xir.Device,
	rt RoutingTable,
	dist uint8,
	switchRoles, hostRoles []xir.Role,
	linkRole xir.LinkRole,
) (RoutingTable, bool) {

	// Get threads
	threads := runtime.NumCPU()

	if len(devices) < threads {
		threads = len(devices)
	}

	// build up a queue of devices to drain
	queue := make(chan *xir.Device, len(devices))
	for _, x := range devices {
		queue <- x
	}
	close(queue)

	// Copy table
	table := make(RoutingTable)
	for k, v := range rt {
		new_v := make([]Route, len(v))
		copy(new_v, v)
		table[k] = new_v
	}

	// a channel for workers to send routes to the aggregator
	add := make(chan []RouteMessage, threads)

	added := false

	// consumption thread
	var consume sync.WaitGroup
	consume.Add(1)
	go func() {
		for {

			rs, ok := <-add
			if !ok {
				consume.Done()
				break
			}
			for _, r := range rs {
				add := true

				for _, x := range table[r.Node] {
					if Equals(x, r.Route) {
						if r.Route.Hops < x.Hops {
							x.Hops = r.Route.Hops
						}
						add = false
						break
					}
				}

				if add {
					added = true
					table[r.Node] = append(table[r.Node], r.Route)
				}
			}
		}
	}()

	// production threads
	var produce sync.WaitGroup
	for i := 0; i < threads; i++ {
		produce.Add(1)
		go func() {
			var rs []RouteMessage
			for {
				x, ok := <-queue
				if !ok {
					add <- rs
					produce.Done()
					return
				}
				rs = append(rs, getRoutesOfDist(x, rt, dist, switchRoles, hostRoles, linkRole)...)
			}
		}()
	}

	produce.Wait()
	close(add)
	consume.Wait()

	return table, added
}

func getRoutesOfDist(
	x *xir.Device,
	rt RoutingTable,
	dist uint8,
	switchRoles, hostRoles []xir.Role,
	linkRole xir.LinkRole,
) []RouteMessage {

	var routes []RouteMessage

	for _, ifx := range x.Interfaces {

		if ifx.Edge != nil {

			// role check
			p := ifx.Port()
			if p.Role != linkRole {
				continue
			}

			for _, nbr := range ifx.Edge.Connection.Edges {

				if nbr == ifx.Edge {
					continue
				}

				if dist == 1 {

					if !nbr.Interface.Device.Resource().HasRole(append(switchRoles, hostRoles...)...) {
						continue
					}

					if nbr.Interface.Device.Resource().HasRole(hostRoles...) {
						r := RouteMessage{
							Node: x.Id(),
							Route: Route{
								Dev:         ifx,
								NextHop:     nbr.Interface,
								Connection:  nbr.Connection,
								Destination: TPA(nbr.Interface.Port().TPA),
								Mask:        HostMask,
								Hops:        1,
							},
						}
						routes = append(routes, r)
					} else {
						r := RouteMessage{
							Node: x.Id(),
							Route: Route{
								Dev:         ifx,
								NextHop:     nbr.Interface,
								Connection:  nbr.Connection,
								Destination: TPA(nbr.Interface.Device.Resource().TPA),
								Mask:        SwitchMask,
								Hops:        1,
							},
						}
						routes = append(routes, r)
					}

				} else {
					// only propagate switch roles, not host routes
					if !nbr.Interface.Device.Resource().HasRole(append(switchRoles)...) {
						continue
					}

					// add a route to every destination of the neighbors, if it's ok
					for _, route := range rt[nbr.Interface.Device.Id()] {

						if route.Hops == dist-1 &&
							route.Mask == SwitchMask &&
							TPA(x.Resource().TPA) != TPA(route.Destination) {

							r := RouteMessage{
								Node: x.Id(),
								Route: Route{
									Dev:         ifx,
									NextHop:     nbr.Interface,
									Connection:  nbr.Connection,
									Destination: route.Destination,
									Mask:        SwitchMask,
									Hops:        dist,
								},
							}

							routes = append(routes, r)
						}
					}
				}

			}
		}
	}

	return routes
}

/*
func consolidateBonds(es []*xir.Endpoint) []*xir.Endpoint {

	m := make(map[string][]*xir.Endpoint)

	for _, e := range es {

		link, ok := e.Props["link"].(*sys.Link)
		if ok {
			if link.Parent != nil {
				if len(link.Parent.BondMembers) > 0 {
					key := strings.Join(link.Parent.BondMembers, "-")
					m[key] = append(m[key], e)
					//fmt.Printf("conso: %s\n", key)
					continue
				}
			}
		}

		m[e.Label()] = append(m[e.Label()], e)

	}

	var result []*xir.Endpoint
	for _, es := range m {
		result = append(result, es[0])
	}
	return result

}

func consolidateBreakouts(nbrs []*xir.Neighbor) []*xir.Neighbor {

	return nbrs

}

*/
