package sfe

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/xir/v0.3/go"
)

const (
	CPUCostPerCore    = 45.0
	MemoryCostPerByte = 7.0 / 1024 / 1024 / 1024
	NetworkCostPerBit = 8.0 / 1024 / 1024 / 1024
	DiskCostPerByte   = 3.0 / 1024 / 1024 / 1024
)

func ComputeCosts(hs []*Host) {

	log.Info("computing costs")

	for _, h := range hs {

		h.Cost.Exclusive = cpuCost(h.Device) +
			memoryCost(h.Device) +
			portCost(h.Device) +
			diskCost(h.Device)

		if h.Device.Data.(*xir.Resource).HasAllocMode(xir.AllocMode_Virtual) {
			cc := coreCount(h.Device)
			h.Cost.MTA = h.Cost.Exclusive / cc
			h.Cost.Exclusive = h.Cost.Exclusive * cc
		}

	}

}

func memoryCost(d *xir.Device) float64 {

	v := 0.0

	for _, x := range d.Data.(*xir.Resource).Memory {
		v += float64(x.Capacity) * MemoryCostPerByte
	}

	return float64(v)

}

func cpuCost(d *xir.Device) float64 {

	v := 0.0

	for _, x := range d.Data.(*xir.Resource).Procs {
		v += float64(x.Cores) * CPUCostPerCore
	}

	return float64(v)

}

func portCost(d *xir.Device) float64 {

	v := 0.0

	for _, x := range d.Data.(*xir.Resource).NICs {
		for _, p := range x.Ports {
			v += float64(p.Capacity) * NetworkCostPerBit
		}
	}

	return float64(v)

}

func diskCost(d *xir.Device) float64 {

	v := 0.0

	for _, x := range d.Data.(*xir.Resource).Disks {
		v += float64(x.Capacity) * DiskCostPerByte
	}

	return float64(v)

}

func coreCount(d *xir.Device) float64 {

	v := 0.0
	for _, x := range d.Data.(*xir.Resource).Procs {
		v += float64(x.Cores)
	}
	return v

}
