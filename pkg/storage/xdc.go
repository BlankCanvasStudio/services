package storage

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"

	clientv3 "go.etcd.io/etcd/client/v3"
)

type XDC struct {
	*portal.XDCStorage

	UpdateRequest *portal.XDCStorage
}

func NewXDC(creator, name, project string) *XDC {

	return &XDC{
		XDCStorage: &portal.XDCStorage{
			Creator: creator,
			Name:    name,
			Project: project,
		},
	}
}

// The canonical way to name an xdc.
func XdcId(name, project string) string {
	return name + "." + project
}

func (x *XDC) XdcId() string {
	return XdcId(x.Name, x.Project)
}

func (x *XDC) Bucket() string {

	return "xdcs"
}

func (x *XDC) Id() string {

	return x.Name + "." + x.Project
}

func (x *XDC) Key() string {

	return "/" + x.Bucket() + "/" + x.XdcId()
}

func (x *XDC) Value() interface{} {

	return x.XDCStorage
}

func (x *XDC) Substrate() Substrate {

	return Etcd
}

func (x *XDC) Read() error {

	_, err := etcdTx(readOps(x)...)
	return err
}

func (x *XDC) GetVersion() int64 {

	return x.Ver
}

func (x *XDC) SetVersion(v int64) {

	x.Ver = v
}

func (x *XDC) Create() (*Rollback, error) {

	log.Debugf("[%s.%s] creating XDC for %s", x.Name, x.Project, x.Creator)

	err := x.Read()
	if err != nil {
		return nil, fmt.Errorf("xdc read: %w", err)
	}

	if x.Ver > 0 {
		return nil, fmt.Errorf("XDC exists")
	}

	if x.Project == "" || x.Name == "" {
		return nil, fmt.Errorf("incomplete creation information")
	}

	p := NewProject(x.Project)
	err = p.Read()
	if err != nil {
		return nil, fmt.Errorf("project read: %w", err)
	}

	if p.Ver == 0 {
		return nil, fmt.Errorf("project %s does not exist", x.Project)
	}

	log.Debugf("writing XDC: %+v", x)

	r, err := etcdTx(writeOps(x)...)
	if err != nil {
		return nil, fmt.Errorf("xdc write: %w", err)
	}

	return r, nil
}

func (x *XDC) Update() (*Rollback, error) {

	log.Infof("XDC update")

	// If there is no update, there is nothing to do.
	if x.UpdateRequest == nil {
		return nil, nil
	}

	err := x.Read()
	if err != nil {
		return nil, fmt.Errorf("xdc read: %w", err)
	}
	if x.Ver == 0 {
		return nil, fmt.Errorf("XDC does not exist")
	}

	upd := x.UpdateRequest

	if upd.Creator != "" {
		c := NewUser(upd.Creator)
		err = c.Read()
		if err != nil {
			return nil, fmt.Errorf("read user %s: %w", upd.Creator, err)
		}

		if c.Ver == 0 {
			return nil, fmt.Errorf("user %s does not exist", upd.Creator)
		}
	}

	if upd.Image != "" {
		x.Image = upd.Image
	}
	if upd.MemLimit != 0 {
		x.MemLimit = upd.MemLimit
	}
	if upd.CpuLimit != 0 {
		x.CpuLimit = upd.CpuLimit
	}

	r, err := etcdTx(writeOps(x)...)
	if err != nil {
		return nil, fmt.Errorf("xdc write: %w", err)
	}

	return r, nil
}

func (x *XDC) SetMaterialization(mtz string) (*Rollback, error) {

	err := x.Read()
	if err != nil {
		return nil, fmt.Errorf("xdc read: %w", err)
	}
	if x.Ver == 0 {
		return nil, fmt.Errorf("XDC does not exist")
	}

	x.Materialization = mtz

	r, err := etcdTx(writeOps(x)...)
	if err != nil {
		return nil, fmt.Errorf("xdc write: %w", err)
	}

	return r, nil
}

func (x *XDC) Delete() (*Rollback, error) {

	err := x.Read()
	if err != nil {
		return nil, fmt.Errorf("xdc read: %w", err)
	}

	if x.Ver == 0 {
		return nil, fmt.Errorf("xdc does not exist")
	}

	dels := []StorOp{}
	dels = append(dels, delOps(x)...)

	ops, err := x.DelLinkedOps()
	if err != nil {
		log.Warnf("Unable to get linked delete ops for xdc %s", x.Name)
	}

	dels = append(dels, ops...)

	r, err := etcdTx(dels...)
	if err != nil {
		return nil, fmt.Errorf("xdc del: %w", err)
	}

	return r, nil
}

// DelLinkedOps ...
func (x *XDC) DelLinkedOps() ([]StorOp, error) {

	if x.Materialization != "" {
		c := NewXdcWgClient(&portal.AttachXDCRequest{Project: x.Project, Xdc: x.Name})
		err := c.Read()
		if err != nil {
			return nil, status.Error(codes.Internal, "XDC attach data read")
		}

		log.Infof("Found attachment %x -> %s. Deleting attachment key.", x.Name, x.Materialization)

		return delOps(c), nil
	}

	return nil, nil
}

func ListXDCs(pid string) ([]*XDC, error) {

	var result []*XDC

	x := &XDC{}
	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/"+x.Bucket(), clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get xdcs: %w", err)
	}

	for _, kv := range resp.Kvs {

		x := new(portal.XDCStorage)
		err = proto.Unmarshal(kv.Value, x)
		if err != nil {
			return nil, fmt.Errorf("malformed xdc data at %s: %w", string(kv.Key), err)
		}

		if x.Project == pid {
			result = append(result, &XDC{XDCStorage: x})
		}
	}

	return result, nil
}
