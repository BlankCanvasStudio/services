package storage

import (
	"fmt"
	"gitlab.com/mergetb/api/portal/v1/go"
)

type JupyterCfg struct {
	*portal.JupyterCfg
	host string
}

func NewJupyterCfg(host string) *JupyterCfg {
	return &JupyterCfg{
		JupyterCfg: &portal.JupyterCfg{
			Host: host,
		},
	}
}

func (j *JupyterCfg) Bucket() string {

	return "jupytercfg"
}

func (j *JupyterCfg) Id() string {

	return fmt.Sprintf("%s", j.Host)
}

func (j *JupyterCfg) Key() string {

	return fmt.Sprintf("/%s/%s", j.Bucket(), j.Id())
}

func (j *JupyterCfg) Value() interface{} {

	return j.JupyterCfg
}

func (j *JupyterCfg) Substrate() Substrate {

	return Etcd
}

func (j *JupyterCfg) Read() error {

	_, err := etcdTx(readOps(j)...)
	return err
}

func (j *JupyterCfg) Create() (*Rollback, error) {

	rb, err := etcdTx(writeOps(j)...)
	if err != nil {
		return nil, fmt.Errorf("jupcfg write: %w", err)
	}

	return rb, nil
}

func (j *JupyterCfg) Update() (*Rollback, error) {

	return j.Create()
}

func (j *JupyterCfg) Delete() (*Rollback, error) {

	err := j.Read()
	if err != nil {
		return nil, fmt.Errorf("jc read: %w", err)
	}

	return etcdTx(delOps(j)...)
}

func (j *JupyterCfg) GetVersion() int64 {

	return j.Ver
}

func (j *JupyterCfg) SetVersion(v int64) {

	j.Ver = v
}
