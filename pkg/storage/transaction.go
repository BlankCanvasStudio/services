package storage

import (
	"bytes"
	"context"
	"fmt"
	"strings"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
)

// TODO: Deprecate with the new potpourri storage library

type Operation int

const (
	Read Operation = iota + 1
	Write
	Delete
)

type StorOp struct {
	Op     Operation
	Object ObjectIO
}

func (so *StorOp) String() string {
	op := ""
	switch so.Op {
	case Read:
		op = "Read"
	case Write:
		op = "Write"
	case Delete:
		op = "Delete"
	}

	return fmt.Sprintf("[%s %s]", op, so.Object.Key())
}

func opsString(ops []StorOp) string {
	vals := []string{}
	for _, v := range ops {
		vals = append(vals, v.String())
	}
	return strings.Join(vals, ", ")
}

func readOps(objects ...ObjectIO) []StorOp {

	var ops []StorOp
	for _, o := range objects {
		ops = append(ops, StorOp{Op: Read, Object: o})
	}
	return ops

}

func writeOps(objects ...ObjectIO) []StorOp {

	var ops []StorOp
	for _, o := range objects {
		ops = append(ops, StorOp{Op: Write, Object: o})
	}
	return ops

}

func delOps(objects ...ObjectIO) []StorOp {

	var ops []StorOp
	for _, o := range objects {
		ops = append(ops, StorOp{Op: Delete, Object: o})
	}
	return ops
}

type Transaction []StorOp

func (t Transaction) Exec() error {

	var etcd, minio []StorOp

	for _, action := range t {

		switch action.Object.Substrate() {

		case Etcd:
			etcd = append(etcd, action)

		case MinIO:
			minio = append(minio, action)

		}

	}

	rollback, err := etcdTx(etcd...)
	if err != nil {
		return err
	}

	err = minioTx(minio)
	if err != nil {
		_, rerr := rollback.Etcd()
		if rerr != nil {
			log.WithError(err).Error(
				"etcd rollback failed, data may be inconsistent")
		}
		return err
	}

	return nil

}

type RbStack []*Rollback

func (rbs RbStack) Push(rb *Rollback) RbStack {

	if rb == nil {
		// don't push things we cannot unwind
		return rbs
	}

	return append(rbs, rb)

}

func (rbs RbStack) PushAll(rs RbStack) RbStack {
	return append(rbs, rs...)
}

func (rbs RbStack) Pop() (*Rollback, RbStack) {

	n := len(rbs)
	return rbs[n-1], rbs[:n-1]

}

func (rbs RbStack) Unwind() error {
	for i := len(rbs) - 1; i >= 0; i-- {
		if rbs[i].Etcd != nil {
			resp, err := rbs[i].Etcd()
			if err != nil {
				return err
			}
			if !resp.Succeeded {
				return fmt.Errorf("transaction failed")
			}
		}
	}
	return nil
}

type Rollback struct {
	Etcd EtcdRollback
	//TODO MinIO rollback?
}

type EtcdRollback func() (*clientv3.TxnResponse, error)

// Keep track of ongoing updates to data while gathering all the
// operations for a single transaction.
type TransactionCacheType int64

const (
	PTOrg TransactionCacheType = iota
	PTProj
	PTExp
	PTUser
	PTFac
	PTPool
)

type TransactionCache struct {
	// Cache objects by type and etcd key.
	Objs map[TransactionCacheType]map[string]Object
}

func NewTransactionCache() *TransactionCache {
	return &TransactionCache{
		Objs: map[TransactionCacheType]map[string]Object{
			PTOrg:  make(map[string]Object),
			PTProj: make(map[string]Object),
			PTExp:  make(map[string]Object),
			PTUser: make(map[string]Object),
			PTFac:  make(map[string]Object),
			PTPool: make(map[string]Object),
		},
	}
}

// Create a new thing or give back a cahed version of the thing.
// args are what is used to New...() the object type requestsed.
func (tc *TransactionCache) NewObj(t TransactionCacheType, args ...string) Object {
	var obj Object
	switch t {
	case PTOrg:
		obj = NewOrganization(args[0])
	case PTProj:
		obj = NewProject(args[0])
	case PTExp:
		obj = NewExperiment(args[0], args[1])
	case PTUser:
		obj = NewUser(args[0])
	case PTFac:
		obj = NewFacility(args[0])
	case PTPool:
		obj = NewPool(args[0])
	}
	if o, ok := tc.Objs[t][obj.Key()]; ok {
		// cache hit
		return o
	}
	// now cache it and return it
	tc.Objs[t][obj.Key()] = obj
	return obj
}

// Read objects using cache.
func (tc *TransactionCache) Read() (*Rollback, error) {

	robjs := []ObjectIO{}
	for _, objs := range tc.Objs {
		for _, o := range objs {
			if o.GetVersion() == 0 {
				// Not read yet, add it to the list.
				robjs = append(robjs, o)
			}
		}
	}
	if len(robjs) > 0 {
		// Read unread things.
		rb, err := etcdTx(readOps(robjs...)...)
		if err != nil {
			return rb, err
		}
	}
	return nil, nil
}

// deconflicting actions means two things: removing duplicates
// and choosing which duplicate to keep based on the operation.
// Operation precedence is delete > write > read. That is if
// there is a delete operation that will always be preferred.
// The reasoning here is if an object is getting deleted,
// there is no reason to read/write it.
func deconflictActions(actions []StorOp) ([]StorOp, error) {

	// Map of objects my keys. Ops are put in operation buckets.
	// Duplicates in the same bucket are ignored.
	seen := map[string]map[Operation]StorOp{}
	for _, a := range actions {
		k := a.Object.Key()
		if _, ok := seen[k]; !ok {
			seen[k] = make(map[Operation]StorOp)
		}
		if _, ok := seen[k][a.Op]; !ok {
			seen[k][a.Op] = a
		}
		// else skipping a duplicate
	}

	ret := []StorOp{}

	// Now for each key, choose the single op to add to the list
	// of actions that will be executed. delete > write > read.
	for _, op := range seen {
		if o, ok := op[Delete]; ok {
			ret = append(ret, o)
		} else if o, ok := op[Write]; ok {
			ret = append(ret, o)
		} else if o, ok := op[Read]; ok {
			ret = append(ret, o)
		}
	}

	return ret, nil
}

func etcdTx(passedActions ...StorOp) (*Rollback, error) {

	c := EtcdClient
	kvc := clientv3.NewKV(c)

	var update []clientv3.Op
	var revision []clientv3.Cmp

	// Because we are using cached objects, we can safely delete duplicate
	// actions as duplicate actions are all acting on the same in-memory data.
	// Order for deleting actions: delete > (write | read). If there is a delete
	// keep that one.
	actions, err := deconflictActions(passedActions)
	if err != nil {
		return nil, err
	}

	log.Debugf("actions to execute: %s", opsString(actions))

	// attempt to run the transaction

	for _, a := range actions {

		switch a.Op {

		case Write:
			var data []byte
			var err error

			val := a.Object.Value()

			switch val.(type) {
			case proto.Message:
				data, err = proto.Marshal(val.(proto.Message))
			case StorageMessage:
				data, err = val.(StorageMessage).Marshal()
			default:
				return nil, fmt.Errorf("bad storage type: %T", a.Object.Value())
			}

			if err != nil {
				return nil, err
			}

			update = append(update,
				clientv3.OpPut(a.Object.Key(), string(data), clientv3.WithPrevKV()),
			)
			revision = append(revision,
				clientv3.Compare(
					clientv3.Version(a.Object.Key()),
					"=",
					a.Object.GetVersion(),
				),
			)

		case Delete:
			// TODO should we check revision on delete?
			update = append(update,
				clientv3.OpDelete(a.Object.Key(), clientv3.WithPrevKV()),
			)

		case Read:
			update = append(update, clientv3.OpGet(a.Object.Key()))

		}
	}

	txr, err := kvc.Txn(context.TODO()).If(revision...).Then(update...).Commit()
	if err != nil {
		return nil, err
	}
	if !txr.Succeeded {
		//TODO better message from transaction response?
		return nil, fmt.Errorf("transaction failed")
	}

	// if we are here the transaction succeeded, provide a rollback transaction
	// for callers to use if this update needs to be undone

	var rollback []clientv3.Op

	for i, r := range txr.Responses {

		// collect responses to put operations in the update transaction
		rp := r.GetResponsePut()
		if rp != nil {

			kv := rp.PrevKv

			if kv == nil {
				actions[i].Object.SetVersion(1)

				// this means the key was not present before the update, so we
				// need to delete it on rollback
				k := string(update[i].KeyBytes())
				rollback = append(rollback, clientv3.OpDelete(k))
			} else {
				actions[i].Object.SetVersion(kv.Version + 1)

				// the previous revisionof the key was provided, use this as the
				// rollback
				rollback = append(rollback,
					clientv3.OpPut(string(kv.Key), string(kv.Value)),
				)
			}

		}

		// collect responses to delete operations in the update transaction
		rd := r.GetResponseDeleteRange()
		if rd != nil {

			for _, kv := range rd.PrevKvs {
				rollback = append(rollback,
					clientv3.OpPut(string(kv.Key), string(kv.Value)),
				)
			}
		}

		rr := r.GetResponseRange()
		if rr != nil {

			for _, kv := range rr.Kvs {

				var err error
				o := actions[i].Object

				switch o.(type) {
				case proto.Message:
					err = proto.Unmarshal(kv.Value, actions[i].Object.(proto.Message))
				case StorageMessage:
					err = o.(StorageMessage).Unmarshal(kv.Value)
				default:
					return nil, fmt.Errorf("unsupported storage type")
				}

				if err != nil {
					return nil, err
				}

				actions[i].Object.SetVersion(kv.Version)

			}
		}

	}

	return &Rollback{
		Etcd: func() (*clientv3.TxnResponse, error) {
			return kvc.Txn(context.TODO()).Then(rollback...).Commit()
		},
	}, nil

}

// XXX
func minioTx(actions []StorOp) error {

	c := MinIOClient

	for _, a := range actions {

		switch a.Op {

		case Write:
			data, err := proto.Marshal(a.Object.Value().(proto.Message))
			if err != nil {
				return err
			}
			_, err = c.PutObject(
				context.Background(),
				a.Object.Bucket(),
				a.Object.Id(),
				bytes.NewReader(data),
				int64(len(data)),
				minio.PutObjectOptions{},
			)
			if err != nil {
				return err
			}

		}

	}

	return nil

}
