package storage

import (
	"context"
	"fmt"
	"io/ioutil"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/portal/v1/go"

	me "gitlab.com/mergetb/portal/services/pkg/merror"
)

type RealizeRequest struct {
	*portal.RealizeRequest
}

func NewRealizeRequest(name, experiment, project string) *RealizeRequest {

	return &RealizeRequest{
		RealizeRequest: &portal.RealizeRequest{
			Realization: name,
			Project:     project,
			Experiment:  experiment,
		},
	}

}

// RealizeRequest Storage Object interface implementation =====================

func (r *RealizeRequest) Bucket() string {

	return "realizations"

}

func (r *RealizeRequest) Id() string {

	return fmt.Sprintf("%s/%s/%s", r.Project, r.Experiment, r.Realization)

}

func (r *RealizeRequest) Key() string {

	return fmt.Sprintf("/%s/%s", r.Bucket(), r.Id())

}

func (r *RealizeRequest) Value() interface{} {

	return r.RealizeRequest

}

func (r *RealizeRequest) Substrate() Substrate {

	return Etcd

}

func (r *RealizeRequest) Read() error {

	_, err := etcdTx(readOps(r)...)
	return err

}

func ReadRealizeRequests(rs []*RealizeRequest) error {

	var objs []ObjectIO
	for _, r := range rs {
		objs = append(objs, r)
	}

	_, err := etcdTx(readOps(objs...)...)
	return err

}

func (r *RealizeRequest) Create() (*Rollback, error) {

	log.Infof("creating rlz %s.%s.%s", r.Realization, r.Experiment, r.Project)

	if r.Project == "" {
		return nil, me.BadRequestError("project must be specified")
	}
	if r.Experiment == "" {
		return nil, me.BadRequestError("experiment must be specified")
	}
	if r.Realization == "" {
		return nil, me.BadRequestError("realization must have a name")
	}

	//TODO just roll with latest commit if absent?
	if r.Revision == "" {
		return nil, me.BadRequestError("realization must specify a revision")
	}

	p := NewProject(r.Project)
	err := p.Read()
	if err != nil {
		return nil, fmt.Errorf("project read: %v", err)
	}

	if p.Ver == 0 {
		return nil, fmt.Errorf("project %s does not exist", r.Project)
	}

	e := NewExperiment(r.Experiment, r.Project)
	err = e.Read()
	if err != nil {
		return nil, fmt.Errorf("experiment read: %v", err)
	}

	if e.Ver == 0 {
		return nil, fmt.Errorf("experiment %s does not exist", r.Experiment)
	}

	if e.Models == nil {
		e.Models = make(map[string]*portal.ExperimentModel)
	}

	// sanity checks, but these should have already passed
	model, ok := e.Models[r.Revision]
	if !ok {
		return nil, fmt.Errorf("experiment %s.%s does not contain revision %s",
			r.Experiment, r.Project, r.Revision,
		)
	}

	if model.Compiled == false {
		return nil, fmt.Errorf("experiment %s.%s contains revision %s, but it is not compiled",
			r.Experiment, r.Project, r.Revision,
		)
	}

	found := false
	for _, rlz := range model.Realizations {
		if rlz == r.Realization {
			log.Errorf("BUG: experiment already has reference to new realization %s", rlz)
			found = true
		}
	}

	if !found {
		e.Models[r.Revision].Realizations = append(e.Models[r.Revision].Realizations, r.Realization)
	}

	rb, err := etcdTx(writeOps(r, e)...)
	if err != nil {
		return nil, fmt.Errorf("create realization request write commit: %v", err)
	}

	return rb, nil

}

func (r *RealizeRequest) Delete() (*Rollback, error) {

	log.Infof("deleting rlz %s.%s.%s", r.Realization, r.Experiment, r.Project)

	err := r.Read()
	if err != nil {
		return nil, fmt.Errorf("read realization: %v", err)
	}

	e := NewExperiment(r.Experiment, r.Project)
	err = e.Read()
	if err != nil {
		return nil, fmt.Errorf("read experiment: %v", err)
	}

	// This must be done here and not in DeleteLinkedOps as this effects the parent.
	// When deleting this RealizeRequest, we update the parent experiment. When the
	// Experiment (the parent) is deleted, the models are deleted as part of the experiment deletion.
	if e.Models != nil {
		_, ok := e.Models[r.Revision]
		if ok {
			var lst []string
			for _, rlz := range e.Models[r.Revision].Realizations {
				if rlz == r.Realization {
					continue
				}
				lst = append(lst, rlz)
			}
			e.Models[r.Revision].Realizations = lst
		}
	}

	ops := []StorOp{}

	mreq := NewMaterializeRequest(r.Realization, r.Experiment, r.Project)
	err = mreq.Read()
	if err != nil {
		return nil, fmt.Errorf("mtz req read error: %+v", err)
	}

	if mreq.GetVersion() == 0 { // no mtz
		log.Debug("no associated mtz for this rlz")
	} else {
		log.Debug("found associated mtz. deleting.")
		ops = append(ops, delOps(mreq)...)
	}

	ops = append(ops, delOps(r)...)
	ops = append(ops, writeOps(e)...)
	rb, err := etcdTx(ops...)
	if err != nil {
		return nil, fmt.Errorf("delete realization request commit: %v", err)
	}

	return rb, nil

}

func (r *RealizeRequest) GetVersion() int64 {

	return r.Ver

}

func (r *RealizeRequest) SetVersion(v int64) {

	r.Ver = v

}

func ReadRealizationResult(project, experiment, realization string) (*portal.RealizationResult, error) {

	bucket := fmt.Sprintf("realize-%s-%s-%s", project, experiment, realization)

	// realization

	obj, err := MinIOClient.GetObject(
		context.TODO(),
		bucket,
		"realization",
		minio.GetObjectOptions{},
	)
	if err != nil {
		return nil, err
	}

	buf, err := ioutil.ReadAll(obj)
	if err != nil {
		return nil, err
	}

	rz := new(portal.Realization)
	err = proto.Unmarshal(buf, rz)
	if err != nil {
		return nil, err
	}

	// diagnostics

	obj, err = MinIOClient.GetObject(
		context.TODO(),
		bucket,
		"diagnostics",
		minio.GetObjectOptions{},
	)
	if err != nil {
		return nil, fmt.Errorf("minio get diagnostics: %v", err)
	}

	buf, err = ioutil.ReadAll(obj)
	if err != nil {
		return nil, fmt.Errorf("minio diagnostics read: %v", err)
	}

	dl := new(portal.DiagnosticList)
	err = proto.Unmarshal(buf, dl)
	if err != nil {
		return nil, fmt.Errorf("diagnostics unmarshal: %v", err)
	}

	return &portal.RealizationResult{
		Realization: rz,
		Diagnostics: dl,
	}, nil
}

func (r *RealizeRequest) DeleteLinkedOps() ([]StorOp, error) {

	rid := fmt.Sprintf("%s.%s.%s", r.Realization, r.Experiment, r.Project)
	log.Infof("gathering delete linked ops for rlz %s", rid)

	ops := []StorOp{}

	mreq := NewMaterializeRequest(r.Realization, r.Experiment, r.Project)
	err := mreq.Read()
	if err != nil {
		return nil, fmt.Errorf("mtz req read error: %+v", err)
	}

	if mreq.GetVersion() == 0 { // no mtz
		log.Debug("no associated mtz for this rlz")
	} else {
		log.Infof("adding del ops for mtz %s", rid)
		ops = append(ops, delOps(mreq)...)

		delMtzOps, err := mreq.DeleteLinkedOps()
		if err != nil {
			return nil, fmt.Errorf("get linked mtz opts: %+v", err)
		}

		ops = append(ops, delMtzOps...)

		// We handle the minio mtz data async'ly. This is not great. The minio ObjectIO interface
		// should be made to handle this correctly via minioTx(). Alas. TODO.
		// materialization reconciler ends up calling this to remove the data.
		// DeleteMaterialization(r.Project, r.Experiment, r.Realization)
	}

	return ops, nil
}
