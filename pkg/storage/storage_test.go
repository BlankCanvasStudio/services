package storage

import (
	"context"
	"fmt"
	"os"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	clientv3 "go.etcd.io/etcd/client/v3"
)

func InitLogging() {
	value, ok := os.LookupEnv("LOGLEVEL")
	if ok {
		lvl, err := log.ParseLevel(value)
		if err != nil {
			log.SetLevel(log.InfoLevel)
			log.Errorf("bad LOGLEVEL env var: %s. ignoring", value)
		} else {
			log.Infof("setting log level to %s", value)
			log.SetLevel(lvl)
		}
	}
}

func etcdInit() error {

	endpoint := os.Getenv("TEST_ETCD_ENDPOINT") // may be set be CI or other scripts
	if endpoint == "" {
		endpoint = "localhost:2379"
	}

	cli, err := clientv3.New(clientv3.Config{
		Endpoints: []string{endpoint},
	})
	if err != nil {
		return fmt.Errorf("etcd client: %v", err)
	}

	EtcdClient = cli

	return nil
}

func TestMain(m *testing.M) {

	InitLogging()

	err := etcdInit()
	if err != nil {
		fmt.Printf("init error: %v", err)
		os.Exit(1)
	}

	os.Exit(m.Run())
}

// expectedUid == -1 means we don't care about the uid/gid.
func userCreate(
	t *testing.T, name string, expectedUid int32) (*User, *Rollback) {

	// create user

	u := NewUser(name)

	rollback, err := u.Create()
	assert.Nil(t, err)

	//keydump(t)

	assert.EqualValues(t, 1, u.Ver, "expected user verion to be 1")

	_u := NewUser(name)

	// user read back checks

	err = _u.Read()
	assert.Nil(t, err)

	assert.EqualValues(t, 1, _u.Ver, "expected user verion to be 1 (post read)")

	if expectedUid >= 0 {
		assert.EqualValues(t, expectedUid, u.Uid, "uid check failed")
		assert.EqualValues(t, expectedUid, u.Gid, "user gid check failed")
	}

	// personal project read back checks

	p := NewProject(name)
	err = p.Read()
	assert.Nil(t, err)

	assert.EqualValues(t, 1, p.Ver, "expected project version to be 1")
	assert.Equal(t, p.Name, name)

	if expectedUid >= 0 {
		assert.EqualValues(t, expectedUid, p.Gid, "project gid check failed")
	}

	if assert.NotNil(t, p.Members[name]) {
		m := p.Members[name]
		assert.EqualValues(t, m.Role, portal.Member_Creator)
		assert.EqualValues(t, m.State, portal.Member_Active)
	}

	return u, rollback

}

func projectCreate(t *testing.T,
	name string,
	creator string,
	maintainers ...string,
) (*Project, *Rollback) {

	p := NewProject(name)
	p.Members = map[string]*portal.Member{
		creator: {
			Role:  portal.Member_Creator,
			State: portal.Member_Active,
		},
	}
	for _, m := range maintainers {
		p.Members[m] = &portal.Member{
			Role:  portal.Member_Maintainer,
			State: portal.Member_Active,
		}
	}

	rb, err := p.Create()
	assert.Nil(t, err)

	// read back checks

	_p := NewProject(name)
	err = _p.Read()
	assert.Nil(t, err)

	assert.Equal(t, p.Id(), _p.Id())
	assert.Equal(t, len(p.Members), len(_p.Members))
	for k, v := range p.Members {
		assert.Equal(t, v.Role, _p.Members[k].Role)
		assert.Equal(t, v.State, _p.Members[k].State)
	}

	return p, rb

}

func orgCreate(t *testing.T, name string, creator string) (*Organization, *Rollback) {

	o := NewOrganization(name)

	o.Members = map[string]*portal.Member{
		creator: {
			Role:  portal.Member_Creator,
			State: portal.Member_Active,
		},
	}

	rb, err := o.Create()
	assert.Nil(t, err)

	return o, rb
}

func facilityCreate(t *testing.T,
	name string,
	address string,
	creator string,
	maintainers ...string,
) (*Facility, *Rollback) {

	f := NewFacility(name)
	f.Address = address
	f.Description = fmt.Sprintf("The %s facility", name)

	f.Members = map[string]*portal.Member{
		creator: {
			Role:  portal.Member_Creator,
			State: portal.Member_Active,
		},
	}
	for _, m := range maintainers {
		f.Members[m] = &portal.Member{
			Role:  portal.Member_Maintainer,
			State: portal.Member_Active,
		}
	}

	rb, err := f.Create()
	assert.Nil(t, err)

	// read back checks
	_f := NewFacility(name)
	err = _f.Read()
	assert.Nil(t, err)

	assert.Equal(t, f.Name, _f.Name)
	assert.Equal(t, f.Address, _f.Address)
	assert.Equal(t, f.Description, _f.Description)

	for k, v := range f.Members {
		assert.Equal(t, v.Role, _f.Members[k].Role)
		assert.Equal(t, v.State, _f.Members[k].State)
	}

	return f, rb

}

func experimentCreate(t *testing.T,
	name string,
	project string,
	creator string,
	maintainers ...string,
) (*Experiment, *Rollback) {

	e := NewExperiment(name, project)
	e.Creator = creator
	rb, err := e.Create()
	assert.Nil(t, err)

	// read back checks

	_e := NewExperiment(name, project)
	err = _e.Read()
	assert.Nil(t, err)

	assert.Equal(t, e.Id(), _e.Id())

	return e, rb

}

func realizeRequestCreate(
	t *testing.T,
	realization string,
	e *Experiment,
) (*RealizeRequest, *Rollback) {

	r := NewRealizeRequest(realization, e.Name, e.Project)
	for rev := range e.Models {
		r.Revision = rev
		break // use first rev
	}
	rb, err := r.Create()
	assert.Nil(t, err)

	// read back checks

	_r := NewRealizeRequest(realization, e.Name, e.Project)
	err = _r.Read()
	assert.Nil(t, err)

	assert.Equal(t, r.Revision, _r.Revision)

	return r, rb

}

func materializeRequestCreate(t *testing.T,
	realization string,
	experiment string,
	project string,
) (*MaterializeRequest, *Rollback) {

	m := NewMaterializeRequest(realization, experiment, project)
	rb, err := m.Create()
	assert.Nil(t, err)

	// read back checks
	_m := NewMaterializeRequest(realization, experiment, project)
	err = _m.Read()
	assert.Nil(t, err)

	assert.Equal(t, m.Realization, _m.Realization)

	return m, rb
}

func TestUserCreate(t *testing.T) {

	_, rollback := userCreate(t, "murphy", 1000)

	// rollback

	resp, err := rollback.Etcd()
	assert.Nil(t, err)
	assert.Equal(t, true, resp.Succeeded, "rollback txn failed")

	checkPristine(t)

}

func TestUsersCreate(t *testing.T) {

	var succeeded, failed []*User
	var rbs RbStack

	N := 100

	type CreateResult struct {
		User     *User
		Error    error
		Rollback *Rollback
	}

	// create N users concurrently

	ch := make(chan CreateResult)
	for i := 0; i < N; i++ {

		user := fmt.Sprintf("user%d", i)

		create := func(username string) {

			u := NewUser(username)
			rollback, err := u.Create()
			ch <- CreateResult{
				User:     u,
				Error:    err,
				Rollback: rollback,
			}

		}

		go create(user)

	}

	// collect creation results

	for i := 0; i < N; i++ {
		result := <-ch
		if result.Error != nil {
			failed = append(failed, result.User)
		} else {
			succeeded = append(succeeded, result.User)
			rbs = rbs.Push(result.Rollback)
		}
	}

	t.Logf("succeeded: %d", len(succeeded))
	t.Logf("failed: %d", len(failed))

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// check users

	// test that the number of users in the database is the number of successes,
	// e.g. no failures that left lingering state

	kvc := clientv3.NewKV(EtcdClient)

	resp, err := kvc.Get(context.TODO(), "/users", clientv3.WithPrefix())
	assert.Nil(t, err)

	assert.Equal(t, len(succeeded), len(resp.Kvs),
		"number of users is not the number created")

	// user property checks
	for i, x := range succeeded {
		assert.EqualValues(t, 1000+i, x.Uid)
		assert.EqualValues(t, 1000+i, x.Gid)
	}

	// check projects

	// test that the number of projects in the database is the number of
	// successes, e.g. no failures that left lingering state

	resp, err = kvc.Get(context.TODO(), "/projects", clientv3.WithPrefix())
	assert.Nil(t, err)

	assert.Equal(t, len(succeeded), len(resp.Kvs),
		"number of projects is not the number created")

	for _, x := range succeeded {

		p := NewProject(x.User.Username)
		err = p.Read()
		assert.Nil(t, err)

		assert.EqualValues(t, x.Gid, p.Gid)

	}

}

func TestUserFuncs(t *testing.T) {
	var rbs RbStack
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	murph, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	olive, rb := userCreate(t, "olive", 1001)
	rbs = rbs.Push(rb)

	users, err := ListUsers()
	a.Nil(err)
	a.Len(users, 2)

	a.True(UserExists("olive"))
	a.True(UserExists("murphy"))
	a.False(UserExists("notauser"))

	a.Equal(users[0].Substrate(), Etcd)

	userlist := []*User{murph, olive}
	err = ReadUsers(userlist)
	a.Nil(err)
}

func TestProjectCreate(t *testing.T) {

	var rbs RbStack
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	u, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	p, rb := projectCreate(t, "battlestar", "murphy")
	rbs = rbs.Push(rb)

	a.Contains(p.Members, u.Username)

	// create with not a user
	p = NewProject("broken")
	p.Members = map[string]*portal.Member{
		"notauser": {
			Role:  portal.Member_Creator,
			State: portal.Member_Active,
		},
	}
	_, err := p.Create()
	a.NotNil(err)

	// create with not an org
	p = NewProject("broken")
	p.Organization = "notanorg"
	_, err = p.Create()
	a.NotNil(err)

	// create with an org.
	o, rb := orgCreate(t, "orgname", u.Username)
	rbs = rbs.Push(rb)

	p = NewProject("projwithorg")
	p.Organization = "orgname"
	_, err = p.Create()
	a.NotNil(err) // no org membership info.

	p.OrgMembership = &portal.Member{
		Role:  portal.Member_Member,
		State: portal.Member_Active,
	}

	// should work now.
	rb, err = p.Create()
	a.Nil(err)
	rbs = rbs.Push(rb)

	p.Read()
	o.Read()

	a.Contains(o.Projects, "projwithorg")
	a.Equal(p.Organization, "orgname")
}

func TestProjectsCreate(t *testing.T) {

	var users []ObjectIO
	var succeeded, failed []*Project
	var rbs RbStack

	M := 50 // number of concurrent users creating projects
	N := 3  // number of projects per user

	type CreateResult struct {
		Project  *Project
		Error    error
		Rollback *Rollback
	}

	// create a set of users

	for i := 0; i < M; i++ {
		user := NewUser(fmt.Sprintf("user%d", i))
		users = append(users, user)
		rb, err := user.Create()
		rbs = rbs.Push(rb)
		assert.Nil(t, err)
	}

	// create N projects from M users concurrently, each user attempts to create
	// the project "<user>_battlestar" N times, so we expect there to be M successfull
	// creates and M*(N-1) failed creates (no dupe projects)

	ch := make(chan CreateResult)
	for _, user := range users {

		go func(username string) {

			for i := 0; i < N; i++ {
				p := NewProject(fmt.Sprintf("%s_battlestar", username))
				p.Members = map[string]*portal.Member{
					username: {
						Role:  portal.Member_Creator,
						State: portal.Member_Active,
					},
				}
				rb, err := p.Create()
				ch <- CreateResult{Project: p, Error: err, Rollback: rb}
			}

		}(user.Id())

	}

	// collect results

	for i := 0; i < N*M; i++ {

		result := <-ch
		if result.Error != nil {
			failed = append(failed, result.Project)
		} else {
			succeeded = append(succeeded, result.Project)
			rbs = rbs.Push(result.Rollback)
		}

	}

	t.Logf("succeeded: %d", len(succeeded))
	t.Logf("failed: %d", len(failed))

	assert.EqualValues(t, M, len(succeeded))
	assert.EqualValues(t, M*(N-1), len(failed))

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// test that the number of users in the database is the number of successes,
	// e.g. no failures that left lingering state

	kvc := clientv3.NewKV(EtcdClient)

	resp, err := kvc.Get(context.TODO(), "/projects", clientv3.WithPrefix())
	assert.Nil(t, err)

	// expected projects is personal projects + created projects
	expectedProjects := len(users) + len(succeeded)
	assert.Equal(t, expectedProjects, len(resp.Kvs))

	// project property checks
	for i, x := range succeeded {
		assert.EqualValues(t, 1000+len(users)+i, x.Gid)
	}

	// test that all users now have 2 projects, their personal project and the
	// one successfull create from the test run

	_, err = etcdTx(readOps(users...)...)
	assert.Nil(t, err)

	for _, obj := range users {

		user := obj.Value().(*portal.User)
		assert.EqualValues(t, 2, len(user.Projects))

		personal := user.Projects[user.Username]
		assert.NotNil(t, personal)

		battlestar := user.Projects[fmt.Sprintf("%s_battlestar", user.Username)]
		if assert.NotNil(t, battlestar) {
			assert.Equal(t, battlestar.Role, portal.Member_Creator)
			assert.Equal(t, battlestar.State, portal.Member_Active)
		}

	}

	// delete created projects

	for _, p := range succeeded {
		_, err := p.Delete()
		assert.Nil(t, err)
	}

	// check that total projects is just personal projects now

	resp, err = kvc.Get(context.TODO(), "/projects", clientv3.WithPrefix())
	assert.Nil(t, err)
	assert.Equal(t, len(users), len(resp.Kvs))

	// test that all users are back to having just 1 project reference and that
	// project is their personal project

	_, err = etcdTx(readOps(users...)...)
	assert.Nil(t, err)

	for _, obj := range users {

		user := obj.Value().(*portal.User)
		assert.EqualValues(t, 1, len(user.Projects))

		personal := user.Projects[user.Username]
		assert.NotNil(t, personal)

	}

}

func TestUserWithProjectCreate(t *testing.T) {

	var rbs RbStack

	u, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	p, rb := projectCreate(t, "battlestar", "murphy")
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// check project properties

	assert.EqualValues(t, 1, p.Ver)
	assert.EqualValues(t, 1001, p.Gid)

	_p := NewProject("battlestar")
	err := _p.Read()
	assert.Nil(t, err)

	assert.EqualValues(t, 1, _p.Ver)
	assert.EqualValues(t, 1001, _p.Gid)
	if assert.NotNil(t, _p.Members["murphy"]) {
		m := _p.Members["murphy"]
		assert.EqualValues(t, m.Role, portal.Member_Creator)
		assert.EqualValues(t, m.State, portal.Member_Active)
	}

	//keydump(t)

	_, err = u.Delete()
	assert.Nil(t, err)

	// after deleting the user and the counters, the transitive delete should
	// take out our user created project as well
	delprefix(t, "/counters")
	checkPristine(t)

}

func TestProjectCreatorWithOtherMembersDeleteMember(t *testing.T) {

	var rbs RbStack

	_, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	olive, rb := userCreate(t, "olive", 1001)
	rbs = rbs.Push(rb)

	p, rb := projectCreate(t, "battlestar", "murphy", "olive")
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, err := olive.Delete()
	assert.Nil(t, err)

	p.Read()
	assert.Equal(t, 1, len(p.Members),
		"member ref should drop when user deleted")

}

func TestProjectCreatorWithOtherMembersDeleteCreator(t *testing.T) {

	var rbs RbStack

	murphy, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	olive, rb := userCreate(t, "olive", 1001)
	rbs = rbs.Push(rb)

	_, rb = projectCreate(t, "battlestar", "murphy", "olive")
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	olive.Read()
	assert.Equal(t, 2, len(olive.Projects),
		"olive should be a member in two projects")

	_, err := murphy.Delete()
	assert.Nil(t, err)

	olive.Read()
	assert.Equal(t, 1, len(olive.Projects),
		"project ref should drop when deleted")

}

func TestProjectCreatorWithOtherMembersDeleteProject(t *testing.T) {

	var rbs RbStack

	_, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	olive, rb := userCreate(t, "olive", 1001)
	rbs = rbs.Push(rb)

	p, rb := projectCreate(t, "battlestar", "murphy", "olive")
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	olive.Read()
	assert.Equal(t, 2, len(olive.Projects),
		"olive should be a member in two projects")

	_, err := p.Delete()
	assert.Nil(t, err)

	olive.Read()
	assert.Equal(t, 1, len(olive.Projects),
		"project ref should drop when deleted")

}

func TestAddProjectMemberWithNoMembers(t *testing.T) {
	var rbs RbStack
	a := assert.New(t)

	u, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// create with no members.
	p := NewProject("broken")
	p.Members = nil
	rb, err := p.Create()
	rbs = rbs.Push(rb)
	a.Nil(err)

	m := &portal.Member{
		Role:  portal.Member_Creator,
		State: portal.Member_Active,
	}

	// Add a member.
	mu := &portal.MembershipUpdate{}
	mu.Set = make(map[string]*portal.Member)
	mu.Set[u.Username] = m
	p.UpdateRequest = &portal.UpdateProjectRequest{Members: mu}

	rb, err = p.Update()
	rbs = rbs.Push(rb)
	a.Nil(err)
}

func TestUserDelete(t *testing.T) {
	var rbs RbStack
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	uid := "murphy"

	u, rb := userCreate(t, uid, 1000)
	rbs = rbs.Push(rb)

	a.Equal(u.Username, uid)

	pid := "anotherproject"
	p, rb := projectCreate(t, pid, uid)
	rbs = rbs.Push(rb)
	a.Equal(int(p.GetVersion()), 1)

	rb, err := u.Delete()
	a.Nil(err)
	rbs = rbs.Push(rb)

	// confirm the project is gone.
	p = NewProject(pid)
	err = p.Read()
	a.Nil(err)
	a.Equal(int(p.GetVersion()), 0)
}

func TestUserUpdate(t *testing.T) {

	var rbs RbStack
	a := assert.New(t)
	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	olive, rb := userCreate(t, "olive", 1001)
	rbs = rbs.Push(rb)

	p, rb := projectCreate(t, "battlestar", "murphy")
	rbs = rbs.Push(rb)

	olive.Read()
	a.Equal(1, len(olive.Projects))
	a.Equal("", olive.Name)

	// Update Olive, setting a colloquial name and adding to the battlestar
	// project

	name := "Olive Furball"
	olive.UpdateRequest = &portal.UpdateUserRequest{
		Name: name,
		Projects: &portal.MembershipUpdate{
			Set: map[string]*portal.Member{
				p.Name: {
					Role:  portal.Member_Maintainer,
					State: portal.Member_Active,
				},
			},
		},
	}
	rb, err := olive.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = olive.Read()
	a.Nil(err)
	a.Equal(name, olive.Name)
	a.Equal(2, len(olive.Projects))

	// admin
	a.False(olive.Admin)
	olive.UpdateRequest = &portal.UpdateUserRequest{
		ToggleAdmin: true,
	}

	rb, err = olive.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = olive.Read()
	a.Nil(err)
	a.True(olive.Admin)

	// another update will toggle admin settings as the update toogle is still true
	rb, err = olive.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = olive.Read()
	a.Nil(err)
	a.False(olive.Admin)

	// update just the name
	newName := "Miss Kitty Fantastico"
	olive.UpdateRequest = &portal.UpdateUserRequest{
		Name: newName,
	}

	rb, err = olive.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = olive.Read()
	a.Nil(err)
	a.Equal(newName, olive.Name)

	// experiments.
	exp := "EncounterAtFarpoint"
	olive.UpdateRequest = &portal.UpdateUserRequest{
		Experiments: []string{exp},
	}
	rb, err = olive.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = olive.Read()
	a.Nil(err)
	a.Contains(olive.Experiments, exp)

	// now remove the experiment and add another
	exp2 := "TheNakedNow"
	olive.UpdateRequest = &portal.UpdateUserRequest{
		Experiments: []string{exp, exp2},
	}
	rb, err = olive.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = olive.Read()
	a.Nil(err)
	a.NotContains(olive.Experiments, exp)
	a.Contains(olive.Experiments, exp2)

	// project membership
	olive.UpdateRequest = &portal.UpdateUserRequest{
		Projects: &portal.MembershipUpdate{
			Remove: []string{p.Name},
		},
	}
	rb, err = olive.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = olive.Read()
	a.Nil(err)
	err = p.Read()
	a.Nil(err)

	a.NotContains(olive.Projects, p.Name)
	a.NotContains(p.Members, olive.Username)

	// user status auto logout on inactive state
	us := NewUserStatus(olive.Username)
	us.Loggedin = true
	rb, err = us.Create()
	a.Nil(err)
	rbs = rbs.Push(rb)

	olive.UpdateRequest = &portal.UpdateUserRequest{
		State: &portal.UserStateUpdate{
			Value: portal.UserState_Frozen,
		},
	}
	rb, err = olive.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = us.Read()
	a.Nil(err)

	a.False(us.Loggedin)
}

func TestProjectUpdate(t *testing.T) {

	var rbs RbStack

	_, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	olive, rb := userCreate(t, "olive", 1001)
	rbs = rbs.Push(rb)

	p, rb := projectCreate(t, "battlestar", "murphy")
	rbs = rbs.Push(rb)

	olive.Read()
	assert.Equal(t, 1, len(olive.Projects))
	assert.Equal(t, "", olive.Name)

	// Update battlestar, setting a description and adding Olive to the project

	description := "galactica"
	p.UpdateRequest = &portal.UpdateProjectRequest{
		Description: &portal.DescriptionUpdate{
			Value: description,
		},
		Members: &portal.MembershipUpdate{
			Set: map[string]*portal.Member{
				olive.Id(): {
					Role:  portal.Member_Maintainer,
					State: portal.Member_Active,
				},
			},
		},
	}
	rb, err := p.Update()
	assert.Nil(t, err)
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	err = p.Read()
	assert.Nil(t, err)

	err = olive.Read()
	assert.Nil(t, err)
	assert.Equal(t, description, p.Description)
	assert.Equal(t, 2, len(olive.Projects))

	// adding a not user as member should fail.
	p.UpdateRequest = &portal.UpdateProjectRequest{
		Members: &portal.MembershipUpdate{
			Set: map[string]*portal.Member{
				"npc": {
					Role:  portal.Member_Member,
					State: portal.Member_Active,
				},
			},
		},
	}

	rb, err = p.Update()
	assert.NotNil(t, err)
	rbs = rbs.Push(rb)
}

func TestExperimentCreate(t *testing.T) {

	var rbs RbStack

	_, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	_, rb = projectCreate(t, "battlestar", "murphy")
	rbs = rbs.Push(rb)

	_, rb = experimentCreate(t, "galactica", "battlestar", "murphy")
	rbs = rbs.Push(rb)

	//keydump(t)

	rbs.Unwind()
	checkPristine(t)

}

func TestExperimentUpdate(t *testing.T) {

	var rbs RbStack
	assert := assert.New(t)

	murphy, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	olive, rb := userCreate(t, "olive", 1001)
	rbs = rbs.Push(rb)

	_, rb = projectCreate(t, "battlestar", "murphy")
	rbs = rbs.Push(rb)

	e, rb := experimentCreate(t, "galactica", "battlestar", "murphy")
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	assert.Equal(e.Creator, murphy.Username)

	olive.Read()
	assert.Len(olive.Experiments, 0)
	assert.Zero(olive.Name) // full name of user
	assert.Equal(olive.Username, "olive")

	// Update battlestar, setting a description
	description := "adama's ship"
	e.UpdateRequest = &portal.UpdateExperimentRequest{
		Description: &portal.DescriptionUpdate{
			Value: description,
		},
	}
	rb, err := e.Update()
	assert.Nil(err)
	rbs = rbs.Push(rb)

	err = e.Read()
	assert.Nil(err)

	// creator
	e.UpdateRequest = &portal.UpdateExperimentRequest{
		Creator: olive.Username,
	}
	rb, err = e.Update()
	assert.Nil(err)
	rbs = rbs.Push(rb)

	err = e.Read()
	assert.Nil(err)
	assert.Equal(e.Creator, olive.Username)

	// empty to start
	assert.Zero(e.Maintainers)

	// make murphy a maintainer
	e.UpdateRequest = &portal.UpdateExperimentRequest{
		Maintainers: []string{murphy.Username},
	}

	rb, err = e.Update()
	assert.Nil(err)
	rbs = rbs.Push(rb)

	t.Logf("w/maintainer exp: %+v", e.Experiment)

	err = e.Read()
	assert.Nil(err)
	assert.Containsf(e.Maintainers, murphy.Username, "add maintainer")

	// remove murphy as maintainer (note the toggle functionality)
	rb, err = e.Update()
	assert.Nil(err)
	rbs = rbs.Push(rb)

	t.Logf("wout/maintainer exp: %+v", e.Experiment)

	err = e.Read()
	assert.Nil(err)
	assert.NotContainsf(e.Maintainers, murphy.Username, "rm maintainer")

	// mode
	assert.Zero(e.AccessMode)

	e.UpdateRequest = &portal.UpdateExperimentRequest{
		AccessMode: &portal.AccessModeUpdate{
			Value: portal.AccessMode_Protected,
		},
	}

	rb, err = e.Update()
	assert.Nil(err)
	rbs = rbs.Push(rb)

	t.Logf("protected exp: %+v", e.Experiment)

	err = e.Read()
	assert.Nil(err)
	assert.Equal(e.AccessMode, portal.AccessMode_Protected)
}

func TestNonProjectMemberCreateExperiment(t *testing.T) {

	var rbs RbStack
	a := assert.New(t)
	pid, eid := "battlestar", "atlantia"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	murphy, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	olive, rb := userCreate(t, "olive", 1001)
	rbs = rbs.Push(rb)

	maisie, rb := userCreate(t, "maisie", 1002)
	rbs = rbs.Push(rb)

	_, rb = projectCreate(t, pid, murphy.Username)
	rbs = rbs.Push(rb)

	e := NewExperiment(eid, pid)
	e.Creator = olive.Username // not a project member
	rb, err := e.Create()
	a.NotNil(err) // gives an error
	rbs = rbs.Push(rb)

	// make olive a project member, but not active.
	addProjectMembers(t, pid, portal.Member_Pending, olive.Username)
	rb, err = e.Create()
	a.NotNil(err) // gives an error
	rbs = rbs.Push(rb)

	// make maisie a project member and active.
	addProjectMembers(t, pid, portal.Member_Active, maisie.Username)
	e.Creator = maisie.Username
	rb, err = e.Create()
	a.Nil(err) // no error
	rbs = rbs.Push(rb)
}

func TestOrgCreate(t *testing.T) {
	var rbs RbStack
	a := assert.New(t)
	oid := "starfleet"
	uid := "picard"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, rb := userCreate(t, uid, -1)
	rbs = rbs.Push(rb)

	o, rb := orgCreate(t, oid, uid)
	rbs = rbs.Push(rb)

	a.Equal(oid, o.Name)
	a.Contains(o.Members, uid)
	a.Equal(o.Members[uid].Role, portal.Member_Creator)
	a.Equal(o.Members[uid].State, portal.Member_Active)
}

func addProjectToOrg(rbs RbStack, a *assert.Assertions, p *Project, o *Organization) {
	updateOrg(rbs, a, p, nil, o, true)
}

func addUserToOrg(rbs RbStack, a *assert.Assertions, u *User, o *Organization) {
	updateOrg(rbs, a, nil, u, o, true)
}

func delProjectFromOrg(rbs RbStack, a *assert.Assertions, p *Project, o *Organization) {
	updateOrg(rbs, a, p, nil, o, false)
}

func delUserFromOrg(rbs RbStack, a *assert.Assertions, u *User, o *Organization) {
	updateOrg(rbs, a, nil, u, o, false)
}

func updateOrg(rbs RbStack, a *assert.Assertions, p *Project, u *User, o *Organization, add bool) {

	if add {
		if u != nil {
			o.UpdateRequest = &portal.UpdateOrganizationRequest{
				Members: &portal.MembershipUpdate{
					Set: map[string]*portal.Member{
						u.Username: {
							Role:  portal.Member_Member,
							State: portal.Member_Active,
						},
					},
				},
			}
			rb, err := o.Update()
			a.Nil(err)
			rbs = rbs.Push(rb)
			err = o.Read()
			a.Nil(err)
			err = u.Read()
			a.Nil(err)
		}
		if p != nil {
			o.UpdateRequest = &portal.UpdateOrganizationRequest{
				Projects: &portal.MembershipUpdate{
					Set: map[string]*portal.Member{
						p.Name: {
							Role:  portal.Member_Member,
							State: portal.Member_Active,
						},
					},
				},
			}
			rb, err := o.Update()
			a.Nil(err)
			rbs = rbs.Push(rb)
			err = o.Read()
			a.Nil(err)
			err = p.Read()
			a.Nil(err)
		}
	} else {
		if u != nil {
			o.UpdateRequest = &portal.UpdateOrganizationRequest{
				Members: &portal.MembershipUpdate{
					Remove: []string{u.Username},
				},
			}
			rb, err := o.Update()
			a.Nil(err)
			rbs = rbs.Push(rb)
			err = o.Read()
			a.Nil(err)
			err = u.Read()
			a.Nil(err)
		}
		if p != nil {
			o.UpdateRequest = &portal.UpdateOrganizationRequest{
				Projects: &portal.MembershipUpdate{
					Remove: []string{p.Name},
				},
			}
			rb, err := o.Update()
			a.Nil(err)
			rbs = rbs.Push(rb)
			err = o.Read()
			a.Nil(err)
			err = p.Read()
			a.Nil(err)
		}
	}
}

func orgWithProjectMember(
	t *testing.T,
	uid, pid, oid string,
) (*User, *Project, *Organization, RbStack) {

	var rbs RbStack
	a := assert.New(t)

	u, rb := userCreate(t, uid, 1000)
	rbs = rbs.Push(rb)

	p, rb := projectCreate(t, pid, uid)
	rbs = rbs.Push(rb)

	o, rb := orgCreate(t, oid, uid)
	rbs = rbs.Push(rb)

	addProjectToOrg(rbs, a, p, o)

	return u, p, o, rbs
}

func TestOrgProject(t *testing.T) {

	var rbs RbStack
	oid := "starfleet"
	pid := "ncc1701"
	uid := "picard"
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, p, o, rb := orgWithProjectMember(t, uid, pid, oid)
	rbs = rbs.PushAll(rb)

	// confirm data.
	a.Contains(o.Projects, pid)
	a.Equal(oid, p.Organization)
}

func TestOrgDelete(t *testing.T) {
	var rbs RbStack
	a := assert.New(t)
	oid := "starfleet"
	uid := "picard"

	pid := "enterprise"
	captain := "kirk"

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, rb := userCreate(t, uid, -1)
	rbs = rbs.Push(rb)

	o, rb := orgCreate(t, oid, uid)
	rbs = rbs.Push(rb)

	_, rb = userCreate(t, captain, -1)
	rbs = rbs.Push(rb)

	p, rb := projectCreate(t, pid, captain)
	rbs = rbs.Push(rb)

	addProjectToOrg(rbs, a, p, o)

	seven, rb := userCreate(t, "sevenofnine", -1)
	rbs = rbs.Push(rb)

	addUserToOrg(rbs, a, seven, o)

	t.Logf("deleting org: %v", o)

	a.Equal(p.Organization, oid)
	a.Contains(o.Projects, pid)
	a.Contains(o.Members, uid)
	a.Contains(o.Members, seven.Username)
	a.Contains(seven.Organizations, oid)

	// delete Org. This will show up in code coverage reports
	// for org.Delete() and org.DeleteLinkedOps()
	rb, err := o.Delete()
	a.Nil(err)
	rbs = rbs.Push(rb)

	err = p.Read()
	a.Nil(err)
	err = seven.Read()
	a.Nil(err)

	a.Empty(p.Organization)
	a.Nil(p.OrgMembership)
	a.Empty(seven.Organizations)
}

func TestProjectOrgTransfer(t *testing.T) {

	var rbs RbStack
	oid := "starfleet"
	pid := "ncc1701"
	uid := "picard"
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, p, o, rbss := orgWithProjectMember(t, uid, pid, oid)
	rbs = rbs.PushAll(rbss)

	a.Equal(p.Organization, oid)

	// move the project to a new org and confirm updates.
	oid2 := "borg"
	o2, rb := orgCreate(t, oid2, uid)
	rbs = rbs.Push(rb)

	// NB: adding a project to an org does not remove it from the other.
	addProjectToOrg(rbs, a, p, o2)

	// assert things.
	a.Contains(o2.Projects, pid)    // new org has project
	a.Equal(oid2, p.Organization)   // project is in new org
	a.NotEqual(oid, p.Organization) // project is not in old org

	// remove it from the other org.
	delProjectFromOrg(rbs, a, p, o)

	// confirm it's gone.
	a.NotContains(o.Projects, pid) // org does not have project
}

func TestOrgFuncs(t *testing.T) {

	var rbs RbStack
	oid := "starfleet"
	pid := "ncc1701"
	uid := "picard"
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, _, o, rbss := orgWithProjectMember(t, uid, pid, oid)
	rbs = rbs.PushAll(rbss)

	orgs, err := ListOrganizations()
	a.Nil(err)
	a.Len(orgs, 1)
	a.Equal(orgs[0].Name, oid)

	orglist := []*Organization{o}
	err = ReadOrganizations(orglist)
	a.Nil(err)
}

func TestOrgDeleteThings(t *testing.T) {

	// when a project or user goes away confirm those deatils are removed from the org.

	var rbs RbStack
	oid := "orgname"
	pid := "projname"
	uid := "username"
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	u, p, o, rbss := orgWithProjectMember(t, uid, pid, oid)
	rbs = rbs.PushAll(rbss)

	// Adding the project a user is creator of does not add that user as an org member.
	addUserToOrg(rbs, a, u, o)

	pid2 := "proj2name"
	p2, rb := projectCreate(t, pid2, uid)
	rbs = rbs.Push(rb)

	addProjectToOrg(rbs, a, p2, o)

	uid2 := "user2name"
	u2, rb := userCreate(t, uid2, -1)
	rbs = rbs.Push(rb)

	addUserToOrg(rbs, a, u2, o)

	// get updates.
	for _, x := range []Object{o, p, p2, u, u2} {
		err := x.Read()
		a.Nil(err)
	}

	// assert all the things
	a.Contains(o.Projects, p.Name)
	a.Contains(o.Projects, p2.Name)
	a.Contains(o.Members, u.Username)
	a.Contains(o.Members, u2.Username)
	a.Equal(p.Organization, o.Name)
	a.Equal(p2.Organization, o.Name)
	a.Contains(u.Organizations, o.Name)
	a.Contains(u2.Organizations, o.Name)

	// remove the project
	t.Logf("deleting project %s", p2.Name)
	rb, err := p2.Delete()
	rbs = rbs.Push(rb)
	a.Nil(err)

	// get updates.
	for _, x := range []Object{o, p, u, u2} {
		err = x.Read()
		a.Nil(err)
	}

	// assert the project is gone from the org.
	a.NotContains(o.Projects, pid2)
	a.Contains(o.Projects, pid)

	// delete a user
	t.Logf("deleting user %s", u.Name)
	rb, err = u.Delete()
	rbs = rbs.Push(rb)
	a.Nil(err)

	// get updates.
	for _, x := range []Object{o, p, u2} {
		err = x.Read()
		a.Nil(err)
	}

	t.Logf("org members: %v", o.Members)

	// assert the user is gone from the org,
	a.NotContains(o.Members, uid)
	a.Contains(o.Members, uid2)
}

func TestProjFuncs(t *testing.T) {

	var rbs RbStack
	pid := "ncc1701"
	uid := "picard"
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, rb := userCreate(t, uid, -1)
	rbs = rbs.Push(rb)

	p, rb := projectCreate(t, pid, uid)
	rbs = rbs.Push(rb)

	ps, err := ListProjects()
	a.Nil(err)
	a.Len(ps, 2) // ncc1701 and picard's personal project
	a.Equal(ps[0].Name, p.Name)

	plist := []*Project{p}
	err = ReadProjects(plist)
	a.Nil(err)
}

func TestRealizeRequestCreate(t *testing.T) {

	var rbs RbStack

	_, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	_, rb = projectCreate(t, "battlestar", "murphy")
	rbs = rbs.Push(rb)

	e, rb := experimentCreate(t, "galactica", "battlestar", "murphy")
	rbs = rbs.Push(rb)

	e.AddCompilation("thisisthemodelrevision", nil)

	_, rb = realizeRequestCreate(t, "galactica", e)
	rbs = rbs.Push(rb)

	rbs.Unwind()
	checkPristine(t)

}

func TestRealizeRequestDelete(t *testing.T) {

	// create a rlz, and delete it. confirm the mtz associated with it is removed as well.
	var rbs RbStack
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	uid, pid, eid, rid := "murphy", "galactica", "battlestar", "raptor"

	_, rb := userCreate(t, uid, 1000)
	rbs = rbs.Push(rb)

	_, rb = projectCreate(t, pid, uid)
	rbs = rbs.Push(rb)

	e := NewExperiment(eid, pid)
	e.Creator = uid
	rb, err := e.Create()
	rbs = rbs.Push(rb)
	assert.Nil(t, err)

	err = e.AddCompilation("abababa", nil)
	assert.Nil(t, err)

	r := NewRealizeRequest(rid, eid, pid)
	r.Revision = "abababa"
	rb, err = r.Create()
	rbs = rbs.Push(rb)
	assert.Nil(t, err)

	m := NewMaterializeRequest(rid, eid, pid)
	rb, err = m.Create()
	rbs = rbs.Push(rb)
	assert.Nil(t, err)
	a.GreaterOrEqual(int64(1), m.GetVersion())

	rb, err = r.Delete()
	rbs = rbs.Push(rb)
	a.Nil(err)

	_m := NewMaterializeRequest(rid, eid, pid)
	err = _m.Read()
	a.Nil(err)
	a.Equal(int64(0), _m.GetVersion())
}

func TestRealizeDeleteSpecific(t *testing.T) {
	// make sure that deleting a realization only deletes that realization.
	// https://gitlab.com/mergetb/portal/services/-/issues/262
	// create a rlz, and delete it. confirm the mtz associated with it is removed as well.
	var rbs RbStack
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	uid, pid, eid, rid := "murphy", "galactica", "battlestar", "raptor"

	_, rb := userCreate(t, uid, 1000)
	rbs = rbs.Push(rb)

	_, rb = projectCreate(t, pid, uid)
	rbs = rbs.Push(rb)

	e := NewExperiment(eid, pid)
	e.Creator = uid
	rb, err := e.Create()
	rbs = rbs.Push(rb)
	a.Nil(err)

	err = e.AddCompilation("abababa", nil)
	assert.Nil(t, err)

	r := NewRealizeRequest(rid, eid, pid)
	r.Revision = "abababa"
	rb, err = r.Create()
	rbs = rbs.Push(rb)
	a.Nil(err)

	r1 := NewRealizeRequest(rid+"hello", eid, pid)
	r1.Revision = "abababa"
	rb, err = r1.Create()
	rbs = rbs.Push(rb)
	a.Nil(err)

	rb, err = r.Delete()
	rbs = rbs.Push(rb)
	a.Nil(err)

	// make sure r is gone and r1 is not.
	r = NewRealizeRequest(rid, eid, pid)
	err = r.Read()
	a.Nil(err)
	a.Equal(int64(0), r.GetVersion())

	r1 = NewRealizeRequest(rid+"hello", eid, pid)
	err = r1.Read()
	a.Nil(err)
	a.Less(int64(0), r1.GetVersion())
}

func TestRecursiveDelete(t *testing.T) {

	// create proj, exp, rlz, mtz, xdc, wg enc and delete the project
	// confirm all things go away.

	var rbs RbStack
	a := assert.New(t)
	var err error

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	uid, pid, eid, rid := "uid", "pid", "eid", "rid"

	_, rb := userCreate(t, uid, 1000)
	rbs = rbs.Push(rb)

	_, rb = projectCreate(t, pid, uid)
	rbs = rbs.Push(rb)

	e, rb := experimentCreate(t, eid, pid, uid)
	rbs = rbs.Push(rb)

	e.AddCompilation("thisisthemodelrevision", nil)

	r, rb := realizeRequestCreate(t, rid, e)
	rbs = rbs.Push(rb)

	m, rb := materializeRequestCreate(t, rid, eid, pid)
	rbs = rbs.Push(rb)
	a.GreaterOrEqual(int64(1), m.GetVersion())

	// read current state of project
	p := NewProject(pid)
	err = p.Read()
	a.Nil(err)

	rb, err = p.Delete()
	rbs = rbs.Push(rb)
	a.Nil(err)

	m = NewMaterializeRequest(rid, eid, pid)
	err = m.Read()
	a.Nil(err)
	a.Equal(int64(0), m.GetVersion(), "mtz")

	r = NewRealizeRequest(rid, eid, pid)
	err = r.Read()
	a.Nil(err)
	a.Equal(int64(0), r.GetVersion(), "rlz")

	e = NewExperiment(eid, pid)
	err = e.Read()
	a.Nil(err)
	a.Equal(int64(0), e.GetVersion(), "exp")

	p = NewProject(pid)
	err = p.Read()
	a.Nil(err)
	a.Equal(int64(0), p.GetVersion(), "proj")

	// TODO add and test for XDC, wg enclave and minio objects somehow
}

func TestFacilityCreate(t *testing.T) {

	var rbs RbStack

	murphy, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	_, rb = facilityCreate(t, "ds9", "ds9.space", "murphy")
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	err := murphy.Read()
	assert.Nil(t, err)

	assert.Equal(t, 1, len(murphy.Facilities))

}

func TestFacilityCreateDeleteCreator(t *testing.T) {

	var rbs RbStack

	murphy, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	_, rb = facilityCreate(t, "ds9", "ds9.space", "murphy")
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, err := murphy.Delete()
	assert.Nil(t, err)

	p := NewFacility("ds9")
	err = p.Read()
	assert.Nil(t, err)

	assert.EqualValues(t, 0, p.Ver)

}

func TestDeleteFacility(t *testing.T) {

	var rbs RbStack

	murphy, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	ds9, rb := facilityCreate(t, "ds9", "ds9.space", "murphy")
	rbs = rbs.Push(rb)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	err := murphy.Read()
	assert.Nil(t, err)

	assert.EqualValues(t, 1, len(murphy.Facilities))

	_, err = ds9.Delete()
	assert.Nil(t, err)

	err = murphy.Read()
	assert.Nil(t, err)

	assert.EqualValues(t, 0, len(murphy.Facilities))

}

func TestFacilityCreateDeleteMember(t *testing.T) {

	var rbs RbStack

	_, rb := userCreate(t, "murphy", 1000)
	rbs = rbs.Push(rb)

	olive, rb := userCreate(t, "olive", 1001)
	rbs = rbs.Push(rb)

	f, rb := facilityCreate(t, "ds9", "ds9.space", "murphy", "olive")
	rbs = rbs.Push(rb)

	assert.EqualValues(t, 2, len(f.Members))

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	_, err := olive.Delete()
	assert.Nil(t, err)

	err = f.Read()
	assert.Nil(t, err)

	assert.EqualValues(t, 1, len(f.Members))

}

func TestSSHJump(t *testing.T) {

	assert := assert.New(t)
	var rbs RbStack
	name, fqdn := "jump", "jump.mergetb.net"
	var port uint32 = 666

	one := NewSSHJump(name)
	one.Port = port
	one.Fqdn = fqdn

	rb, err := one.Create()
	if !assert.Nil(err) {
		t.Fail()
	}

	rbs = append(rbs, rb)
	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// equal Reads
	two := NewSSHJump(name)
	err = two.Read()
	assert.Nil(err)

	assert.Equal(one.Name, two.Name)
	assert.Equal(one.Port, two.Port)

	// Equal Updates
	var newPort uint32 = 668
	one.Port = newPort
	rb, err = one.Update()
	assert.Nil(err)
	rbs = append(rbs, rb)

	err = two.Read()
	assert.Nil(err)
	assert.Equal(newPort, one.Port)
	assert.Equal(newPort, two.Port)

	// double create. err should be non nil & br == nil
	rb, err = two.Create()
	assert.NotNil(err)
	assert.Nil(rb)

	// Delete
	jumps, err := ListSSHJumps()
	assert.Nil(err)
	assert.Equal(len(jumps), 1)

	_, err = one.Delete()
	assert.Nil(err)

	jumps, err = ListSSHJumps()
	assert.Nil(err)
	assert.Equal(len(jumps), 0)

	// bad data
	one = NewSSHJump("hello")
	one.Name = ""
	_, err = one.Create()
	assert.NotNil(err)

	assert.Equal(one.Substrate(), Etcd)

}

// Test Create(), Update(), Read(), and Delete() just via the interface methods.
func TestSimpleObjectCrud(t *testing.T) {

	uid, pid, xid, fid := "murphy", "murphy", "battlestar", "muffit"

	type ObjTest struct {
		Name string
		Obj  Object
		init func() RbStack // init the env/db for the create().
	}

	withUserAndProj := func() RbStack {
		var rbs RbStack
		_, rb := userCreate(t, uid, 1000) // create a user and a (personal) project
		rbs = rbs.Push(rb)
		return rbs
	}

	noopInit := func() RbStack {
		var rbs RbStack
		return rbs
	}

	// Facility needs some data manipulation before Create().
	fac := NewFacility(fid)
	fac.Address = "1.2.3.4"
	fac.Members = make(map[string]*portal.Member)
	fac.Members[uid] = &portal.Member{}

	// as does experiment.
	exp := NewExperiment(xid, pid)
	exp.Creator = uid

	cases := []ObjTest{
		{"uid", uidCounter(), noopInit},
		{"gid", gidCounter(), noopInit},
		{"exp", exp, withUserAndProj},
		{"fac", fac, withUserAndProj},
		{"jump", NewSSHJump(uid), noopInit},
		{"jup", NewJupyterCfg(uid), noopInit},
		// {"mtz", NewMaterializeRequest(s, s, s)}, // needs Update()
		{"proj", NewProject("hello"), withUserAndProj},
		{"key", NewPublicKey(uid, uid), noopInit},
		// {"rreq", NewRealizeRequest(s, s, s)}, // needs Update()
		{"userkey", NewSSHUserKeyPair(uid), noopInit},
		{"hostkey", NewSSHHostKeyPair(uid), noopInit},
		{"usercert", NewSSHUserCert(uid), noopInit},
		{"hostcert", NewSSHHostCert(uid), noopInit},
		// {"user", NewUser(s)},  /// UID and GID counters need to be reworked for this.
		{"userstat", NewUserStatus(uid), noopInit},
		// {"pool", NewPool("pool"), noopInit},
		// {"wgenc", NewWgEnclave(uid, uid, uid, uid), noopInit},
		{"wgcli", NewXdcWgClient(&portal.AttachXDCRequest{}), noopInit},
		{"xdc", NewXDC(uid, xid, pid), withUserAndProj},
	}

	for _, c := range cases {

		rbs := c.init()

		rs := objCrud(t, c.Obj, c.Name)
		if len(rs) != 0 {
			rbs = append(rbs, rs...)
		}

		rbs.Unwind()
		checkPristine(t)
	}
}

func objCrud(t *testing.T, o Object, name string) RbStack {

	assert := assert.New(t)
	var rbs RbStack

	count := func(s string) (int64, error) {
		c := clientv3.NewKV(EtcdClient)
		resp, err := c.Get(context.TODO(), s, clientv3.WithPrefix())
		if err != nil {
			return 0, err
		}
		return resp.Count, nil
	}

	rb, err := o.Create()
	if !assert.Nil(err, "create "+name) {
		return rbs
	}
	rbs = rbs.Push(rb)

	n, err := count(o.Key())
	if !assert.Nil(err, name) {
		return rbs
	}
	rbs = rbs.Push(rb)

	assert.EqualValues(n, 1, name)

	err = o.Read()
	if !assert.Nil(err, "read "+name) {
		return rbs
	}

	rb, err = o.Update()
	if !assert.Nil(err, "update "+name) {
		return rbs
	}
	rbs = rbs.Push(rb)

	// gotta get all that test coverage
	s := o.Substrate()
	assert.Condition(func() bool { return s == Etcd || s == MinIO }, name)

	rb, err = o.Delete()
	if !assert.Nil(err, "delete "+name) {
		return rbs
	}
	rbs = rbs.Push(rb)

	n, err = count(o.Key())
	if !assert.Nil(err, name) {
		return rbs
	}
	assert.Zero(n, name)

	return rbs
}

func poolCreate(t *testing.T, name string, projects []string, orgs []string) (*Pool, *Rollback) {

	p := NewPool(name)
	p.Description = "This is a description"
	p.Projects = projects
	p.Organizations = orgs

	rb, err := p.Create()
	assert.Nil(t, err)

	_p := NewPool(name)
	err = _p.Read()
	assert.Nil(t, err)

	assert.Equal(t, p.Name, _p.Name)
	assert.Equal(t, p.Description, _p.Description)
	assert.ElementsMatch(t, p.Projects, _p.Projects)

	return p, rb
}

func TestPoolCRUD(t *testing.T) {

	var rbs RbStack
	assert := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// setup. create users, projects, facilites, and resources.
	u := "murphy"
	projects := []string{"battlestar", "moya", "olive"}
	facs := []string{"ds9", "tng", "tos"}

	_, rb := userCreate(t, u, 1000)
	rbs = rbs.Push(rb)

	for _, name := range projects {
		_, rb = projectCreate(t, name, u)
		rbs = rbs.Push(rb)
	}

	for _, name := range facs {
		_, rb = facilityCreate(t, name, name+".space", u)
		rbs = rbs.Push(rb)
	}

	resources := make(map[string][]string)
	for _, f := range facs {
		// four nodes per facility - all named the same.
		resources[f] = []string{"one", "two", "three", "four"}
	}

	// create pool with first two projects and 2 resource sets
	startProjs := projects[:2]
	p, rb := poolCreate(t, "sisko", startProjs, nil)
	rbs = rbs.Push(rb)

	assert.ElementsMatch(startProjs, p.Projects)

	rb, err := p.AddFacility(facs[0], resources[facs[0]])
	assert.Nil(err)
	rbs = rbs.Push(rb)

	rb, err = p.AddFacility(facs[1], resources[facs[1]])
	assert.Nil(err)
	rbs = rbs.Push(rb)

	rb, err = p.RemoveProject("moya")
	assert.Nil(err)
	rbs = rbs.Push(rb)

	err = p.Read()
	assert.Nil(err)
	assert.Len(p.Projects, 1) // one fewer
	assert.Contains(p.Projects, "battlestar")

	rb, err = p.AddProject("olive")
	assert.Nil(err)
	rbs = rbs.Push(rb)

	err = p.Read()
	assert.Nil(err)
	assert.Len(p.Projects, 2) // one more
	assert.Contains(p.Projects, "olive")
	assert.Contains(p.Projects, "battlestar")

	assert.Len(p.Facilities, 2)
	rb, err = p.RemoveFacility(facs[0])
	assert.Nil(err)
	rbs = rbs.Push(rb)

	err = p.Read()
	assert.Nil(err)
	assert.Len(p.Facilities, 1)
	_, exists := p.Facilities[facs[1]]
	assert.True(exists)

	rb, err = p.AddFacility(facs[2], resources[facs[2]])
	assert.Nil(err)
	rbs = rbs.Push(rb)

	err = p.Read()
	assert.Nil(err)
	assert.Len(p.Facilities, 2)

	assert.Equal(Etcd, p.Substrate())

	pls, err := GetPools()
	assert.Nil(err)
	assert.Len(pls, 1)

	rb, err = p.Delete()
	assert.Nil(err)
	rbs = rbs.Push(rb)

	pls, err = GetPools()
	assert.Nil(err)
	rbs = rbs.Push(rb)
	assert.Len(pls, 0)
}

func TestPoolProjOrgDelete(t *testing.T) {
	var rbs RbStack
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	// setup. create users, projects, facilites, and resources.
	u := "murphy"
	projects := []string{"battlestar", "moya"}
	orgs := []string{"starfleet", "romulus"}
	fac := "ds9"

	pSlice := []*Project{}
	oSlice := []*Organization{}

	_, rb := userCreate(t, u, 1000)
	rbs = rbs.Push(rb)

	for _, name := range projects {
		proj, rb := projectCreate(t, name, u)
		rbs = rbs.Push(rb)
		pSlice = append(pSlice, proj)
	}

	for _, name := range orgs {
		org, rb := orgCreate(t, name, u)
		rbs = rbs.Push(rb)
		oSlice = append(oSlice, org)
	}

	_, rb = facilityCreate(t, fac, fac+".space", u)
	rbs = rbs.Push(rb)

	p, rb := poolCreate(t, "pool", projects, orgs)
	rbs = rbs.Push(rb)

	// sanity check
	for _, name := range projects {
		a.Contains(p.Projects, name)
	}
	for _, name := range orgs {
		a.Contains(p.Organizations, name)
	}

	// now delete a project and org and confirm they are gone from the
	// pool
	rb, err := pSlice[0].Delete()
	a.Nil(err)
	rbs = rbs.Push(rb)
	a.Nil(p.Read()) // read updates
	a.NotContains(p.Projects, pSlice[0].Name, "deleted project should be gone: "+pSlice[0].Name)
	a.Contains(p.Projects, pSlice[1].Name)
	a.Len(p.Projects, len(projects)-1)

	rb, err = oSlice[0].Delete()
	a.Nil(err)
	rbs = rbs.Push(rb)
	a.Nil(p.Read()) // read updates
	a.NotContains(p.Organizations, oSlice[0].Name, "deleted organization should be gone: "+oSlice[0].Name)
	a.Contains(p.Organizations, oSlice[1].Name)
	a.Len(p.Organizations, len(orgs)-1)

}

func TestWgEnclave(t *testing.T) {

	var rbs RbStack
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	encid := "rlz.eid.pid"
	clikey := "EhrbhY46drTTXpOWNWPiQZlrvNp/hXTMUtjS/TWKfgQ="
	gwkey := "W/3qSgxr4jE+gaYL4XCTwfAYrZyg+a7oD3xk5TO1fUM="

	cli_wgif := NewWgIfRequest(encid, clikey)
	cli_wgif.Gateway = false

	gw_wgif := NewWgIfRequest(encid, gwkey)
	gw_wgif.Gateway = true

	e := NewWgEnclave(encid)
	rb, err := e.Create()
	a.Nil(err)
	rbs = rbs.Push(rb)

	e.AddIf = cli_wgif.AddWgIfConfigRequest

	a.Len(e.Clients, 0)

	rb, err = e.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)
	a.Len(e.Clients, 1)

	e.AddIf = gw_wgif.AddWgIfConfigRequest

	a.Len(e.Gateways, 0)
	rb, err = e.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)
	a.Len(e.Gateways, 1)

	e.AddIf = nil
	e.DelIf = cli_wgif.AddWgIfConfigRequest

	a.Len(e.Gateways, 1)
	a.Len(e.Clients, 1)

	rb, err = e.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)
	a.Len(e.Gateways, 1)
	a.Len(e.Clients, 0)

	e.AddIf = nil
	e.DelIf = gw_wgif.AddWgIfConfigRequest

	rb, err = e.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)
	a.Len(e.Gateways, 0)
	a.Len(e.Clients, 0)
}

func TestPortalConfig(t *testing.T) {

	var rbs RbStack
	a := assert.New(t)

	defer func() {
		rbs.Unwind()
		checkPristine(t)
	}()

	api, grpc := "api.example.com", "grpc.example.com"

	c, err := GetPortalConfig()
	a.Nil(err)
	a.Empty(c.Config.APIEndpoint)

	c.Config.APIEndpoint = api
	c.Config.GRPCEndpoint = grpc

	rb, err := c.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	c, err = GetPortalConfig()
	a.Nil(err)

	a.Equal(c.Config.APIEndpoint, api)
	a.Equal(c.Config.GRPCEndpoint, grpc)

	c.Config.APIEndpoint = "one"
	rb, err = c.Update()
	a.Nil(err)
	rbs = rbs.Push(rb)

	c, err = GetPortalConfig()
	a.Nil(err)

	a.Equal(c.Config.APIEndpoint, "one")
}

func addProjectMembers(t *testing.T, pid string, state portal.Member_State, members ...string) *Rollback {

	p := NewProject(pid)
	err := p.Read()
	assert.Nil(t, err)

	set := make(map[string]*portal.Member)

	for _, m := range members {
		set[m] = &portal.Member{
			Role:  portal.Member_Member,
			State: state,
		}
	}

	p.UpdateRequest = &portal.UpdateProjectRequest{
		Members: &portal.MembershipUpdate{
			Set: set,
		},
	}

	rb, err := p.Update()
	assert.Nil(t, err)

	err = p.Read()
	assert.Nil(t, err)

	return rb
}
func delprefix(t *testing.T, prefix string) {

	kvc := clientv3.NewKV(EtcdClient)
	_, err := kvc.Delete(context.TODO(), prefix, clientv3.WithPrefix())
	assert.Nil(t, err)

}

func keydump(t *testing.T) {

	kvc := clientv3.NewKV(EtcdClient)

	resp, err := kvc.Get(context.TODO(), "", clientv3.WithPrefix())
	assert.Nil(t, err)

	t.Logf("etcd revision %d", resp.Header.Revision)

	t.Logf("there are %d keys", len(resp.Kvs))
	for _, x := range resp.Kvs {
		t.Logf("[%d] %s", x.Version, string(x.Key))
	}

}

func checkPristine(t *testing.T) {

	kvc := clientv3.NewKV(EtcdClient)

	resp, err := kvc.Get(context.TODO(), "", clientv3.WithPrefix())
	assert.Nil(t, err)

	assert.Equal(t, 0, len(resp.Kvs), "should be no keys in pristine state")
	if len(resp.Kvs) > 0 {
		for _, x := range resp.Kvs {
			t.Errorf("key %s should be gone", string(x.Key))
		}
		t.Error("pristine check failed")
	}

}
