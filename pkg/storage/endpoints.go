package storage

import (
	"context"
	"errors"
	"fmt"
	"math"
	"strconv"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"go.etcd.io/etcd/client/v3"
)

// RecordEndpoints stores the number of endpoints on the emulation server used for a realization.
// This information is used by the realization engine to load balance materializations across
// multiple emulation servers.
func RecordEndpoints(rlz *portal.Realization) error {

	if rlz.LinkEmulation == nil {
		return nil // no emu used for this mz
	}

	// emulation may be spread over multiple servers
	hist := make(map[string]int)

	for _, lnk := range rlz.LinkEmulation.Links {
		hist[lnk.Server] += len(lnk.Interfaces)
	}

	var ops []clientv3.Op

	for host, count := range hist {
		k := fmt.Sprintf("/endpoints/%s/%s.%s.%s", host, rlz.Id, rlz.Eid, rlz.Pid)
		ops = append(ops, clientv3.OpPut(k, fmt.Sprintf("%d", count)))

	}
	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Txn(context.Background()).If().Then(ops...).Commit()
	if err != nil {
		return fmt.Errorf("etcd transaction: %w", err)
	}
	if !resp.Succeeded {
		return fmt.Errorf("etcd transaction failed")
	}

	return nil
}

// DeleteEndpoints removes the etcd keys containing the emulation endpoint count for a realization
func DeleteEndpoints(rlz *portal.Realization) error {

	if rlz.LinkEmulation == nil {
		return nil // no emulation for this realization
	}

	// build list of emu hosts
	// use a map because there will be duplicates
	hosts := make(map[string]bool)
	for _, lnk := range rlz.LinkEmulation.Links {
		hosts[lnk.GetServer()] = true
	}

	var ops []clientv3.Op
	for h, _ := range hosts {
		k := fmt.Sprintf("/endpoints/%s/%s.%s.%s", h, rlz.Id, rlz.Eid, rlz.Pid)
		ops = append(ops, clientv3.OpDelete(k))
	}

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Txn(context.Background()).If().Then(ops...).Commit()
	if err != nil {
		return fmt.Errorf("etcd txn: %w", err)
	}
	if !resp.Succeeded {
		return errors.New("etcd transaction failed")
	}

	return nil
}

// Stores the number of endpoints each network emulation server is processing
type EmuServers map[string]int

// Alloc requests an emulation server with "count" endpoints
func (s EmuServers) Alloc(count int) string {
	min := math.MaxInt32
	ret := ""
	// find the server with the fewest endpoints
	for host, eps := range s {
		if eps < min {
			min = eps
			ret = host
		} else if eps == min && host < ret {
			// maps are unordered sets of keys, so the range operator is not guaranteed to return keys in the same order
			// for testing purposes, we need to return a stable set, so lexigraphically compare the server names when
			// the number of eps is the same
			ret = host
		}
	}
	if ret != "" {
		s[ret] += count
	}
	return ret
}

// Returns a map with the number of endpoints on each requested emulation server
func GetEmuServers(nodes []string) (EmuServers, error) {

	kvc := clientv3.NewKV(EtcdClient)
	ret := make(EmuServers)

	for _, host := range nodes {
		k := "/endpoints/" + host
		resp, err := kvc.Get(context.TODO(), k, clientv3.WithPrefix())
		if err != nil {
			return nil, fmt.Errorf("etcd get: %w", err)
		}
		sum := 0
		for _, kv := range resp.Kvs {
			n, err := strconv.Atoi(string(kv.Value))
			if err != nil {
				return nil, fmt.Errorf("atoi: %s: %w", kv.Value, err)
			}
			sum += n
		}
		ret[host] = sum
	}

	return ret, nil
}
