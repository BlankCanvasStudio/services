package storage

import (
	"context"
	"fmt"
	"sort"

	"github.com/golang/protobuf/proto"
	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

type Pool struct {
	//
	*portal.Pool

	// modify this, then call Update() to update the pool
	PoolUpdate *portal.Pool
}

func NewPool(name string) *Pool {

	return &Pool{
		&portal.Pool{
			Name: name,
		},
		nil,
	}
}

func (p *Pool) Bucket() string {
	return "pools"
}

func (p *Pool) Id() string {
	return p.Name
}

func (p *Pool) Key() string {
	return "/" + p.Bucket() + "/" + p.Id()
}

func (p *Pool) Value() interface{} {
	return p.Pool
}

func (p *Pool) Substrate() Substrate {
	return Etcd
}

func (p *Pool) GetVersion() int64 {
	return p.Pool.Ver
}

func (p *Pool) SetVersion(ver int64) {
	p.Pool.Ver = ver
}

func (p *Pool) Read() error {

	_, err := etcdTx(readOps(p)...)
	return err
}

func (p *Pool) Create() (*Rollback, error) {

	err := p.Read()
	if err != nil {
		return nil, fmt.Errorf("pool read: %w", err)
	}

	if p.Ver > 0 {
		return nil, fmt.Errorf("pool already exists")
	}

	sort.Sort(natural.StringSlice(p.Projects))

	rb, err := etcdTx(writeOps(p)...)
	if err != nil {
		return nil, fmt.Errorf("pool write: %w", err)
	}

	return rb, nil
}

func (p *Pool) Update() (*Rollback, error) {

	objs := []ObjectIO{p}

	err := p.Read()
	if err != nil {
		return nil, fmt.Errorf("pool read: %w", err)
	}

	if p.PoolUpdate.Description != "" {
		p.Description = p.PoolUpdate.Description
	}

	if p.PoolUpdate.Projects != nil {
		p.Projects = p.PoolUpdate.Projects
	}

	if p.PoolUpdate.Facilities != nil {
		p.Facilities = p.PoolUpdate.Facilities
	}

	log.Debugf("objs: %v", objs)

	rb, err := etcdTx(writeOps(objs...)...)
	if err != nil {
		return nil, fmt.Errorf("pool write: %+v", err)
	}

	return rb, nil
}
func (p *Pool) Delete() (*Rollback, error) {

	err := p.Read()
	if err != nil {
		return nil, fmt.Errorf("pool read: %w", err)
	}

	if p.Ver == 0 {
		return nil, fmt.Errorf("pool does not exist")
	}

	rb, err := etcdTx(delOps(p)...)
	if err != nil {
		return nil, fmt.Errorf("pool delete ops: %+v", err)
	}

	return rb, nil
}

// utils
func (p *Pool) AddProject(project string) (*Rollback, error) {

	prj := NewProject(project)
	err := prj.Read()
	if err != nil {
		return nil, fmt.Errorf("project read: %s", err)
	}

	if prj.Ver == 0 {
		return nil, fmt.Errorf("project does not exist")
	}

	for _, proj := range p.Projects {
		if proj == project {
			return nil, nil
		}
	}

	p.PoolUpdate = &portal.Pool{
		Projects: append(p.Projects, project),
	}

	rb, err := p.Update()
	if err != nil {
		return nil, err
	}

	return rb, nil
}

func (p *Pool) RemoveProject(project string) (*Rollback, error) {

	// build a projects slice without the project.
	update := &portal.Pool{}
	found := false
	for i, proj := range p.Projects {
		if proj == project {
			update.Projects = append(p.Projects[:i], p.Projects[i+1:]...)
			found = true
			break
		}
	}

	if !found {
		return nil, fmt.Errorf("project %s not found in pool %s", project, p.Name)
	}

	p.PoolUpdate = update

	rb, err := p.Update()
	if err != nil {
		return nil, err
	}

	return rb, nil
}

func (p *Pool) AddOrganization(organization string) (*Rollback, error) {

	org := NewOrganization(organization)
	err := org.Read()
	if err != nil {
		return nil, fmt.Errorf("organization read: %s", err)
	}

	if org.Ver == 0 {
		return nil, fmt.Errorf("organization does not exist")
	}

	for _, org := range p.Organizations {
		if org == organization {
			return nil, nil
		}
	}

	p.PoolUpdate = &portal.Pool{
		Organizations: append(p.Organizations, organization),
	}

	rb, err := p.Update()
	if err != nil {
		return nil, err
	}

	return rb, nil
}

func (p *Pool) RemoveOrganization(organization string) (*Rollback, error) {

	// build an organizations slice without the organization.
	update := &portal.Pool{}
	found := false
	for i, org := range p.Organizations {
		if org == organization {
			update.Organizations = append(p.Organizations[:i], p.Organizations[i+1:]...)
			found = true
			break
		}
	}

	if !found {
		return nil, fmt.Errorf("organization %s not found in pool %s", organization, p.Name)
	}

	p.PoolUpdate = update

	rb, err := p.Update()
	if err != nil {
		return nil, err
	}

	return rb, nil
}

func (p *Pool) AddFacility(facility string, resources []string) (*Rollback, error) {

	f := NewFacility(facility)
	err := f.Read()
	if err != nil {
		return nil, fmt.Errorf("facility read: %+v", err)
	}

	if f.Ver == 0 {
		return nil, fmt.Errorf("facility does not exist")
	}

	if p.Facilities == nil {
		p.Facilities = make(map[string]*portal.Pool_Resources)
	}

	facs := p.Facilities
	facs[facility] = &portal.Pool_Resources{Resources: resources}
	p.PoolUpdate = &portal.Pool{Facilities: facs}

	rb, err := p.Update()
	if err != nil {
		return nil, err
	}

	return rb, nil
}

func (p *Pool) RemoveFacility(facility string) (*Rollback, error) {

	if _, ok := p.Facilities[facility]; !ok {
		return nil, fmt.Errorf("facility not found in pool")
	}

	update := &portal.Pool{Facilities: p.Facilities}
	delete(update.Facilities, facility)
	p.PoolUpdate = update

	rb, err := p.Update()
	if err != nil {
		return nil, err
	}

	return rb, nil
}

func (p *Pool) UpdateResources(facility string, resources []string, strat *portal.PatchStrategy) (*Rollback, error) {

	res := []string{}
	switch strat.Strategy {
	case portal.PatchStrategy_replace:
		res = resources
	case portal.PatchStrategy_remove:
		break
	case portal.PatchStrategy_expand:
		seen := make(map[string]bool)
		for _, r := range p.Facilities[facility].Resources {
			seen[r] = true
			res = append(res, r)
		}

		for _, r := range resources {
			if !seen[r] {
				res = append(res, r)
			}
		}
	case portal.PatchStrategy_subtract:
		seen := make(map[string]bool)
		for _, r := range resources {
			seen[r] = true
		}

		for _, r := range p.Facilities[facility].Resources {
			if !seen[r] {
				res = append(res, r)
			}
		}
	}

	facs := p.Facilities
	facs[facility] = &portal.Pool_Resources{Resources: res}
	p.PoolUpdate = &portal.Pool{Facilities: facs}

	rb, err := p.Update()
	if err != nil {
		return nil, err
	}

	return rb, nil
}

func GetPools() ([]*Pool, error) {

	ps := []*Pool{}

	kvc := clientv3.NewKV(EtcdClient)
	kvs, err := kvc.Get(context.TODO(), "/pools/", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("read pools: %w", err)
	}

	for _, kv := range kvs.Kvs {

		p := &Pool{Pool: new(portal.Pool)}
		err = proto.Unmarshal(kv.Value, p.Pool)
		if err != nil {
			return nil, fmt.Errorf("proto pool unmarshal: %w", err)
		}

		ps = append(ps, p)
	}

	return ps, nil
}

func GetProjectPool(project string) *Pool {

	pools, err := GetPools()
	if err != nil {
		return nil
	}

	for _, pool := range pools {
		for _, proj := range pool.Projects {
			if proj == project {
				return pool
			}
		}
	}

	// if project is in no pool, it uses the default.
	pool := NewPool("default") // "default" should be parameterized
	err = pool.Read()
	if err != nil {
		return nil
	}

	return pool
}

func GetOrganizationPool(organization string) *Pool {

	pools, err := GetPools()
	if err != nil {
		return nil
	}

	for _, pool := range pools {
		for _, org := range pool.Organizations {
			if org == organization {
				return pool
			}
		}
	}

	// if organization is in no pool, it uses the default.
	pool := NewPool("default") // "default" should be parameterized
	err = pool.Read()
	if err != nil {
		return nil
	}

	return pool
}
