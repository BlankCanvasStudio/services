MergeTB Portal Storage
======================

This is the storage library for creating persistent objects in a MergeTB
portal. Support for managing objects in both etcd and MinIO is provided.

## Consistency

There are two types of locks that ensure data consistency

- Optimistic locks based on object revisions.
- Distributed locks implemented by etcd.

In almost all situations optimistic locking is to be used and is done
automatically by the `etcdTx` and `minioTx` functions respectively. The only
time distributed locks should be used is for truly global state. An example is
UID and PID counters. Because such objects are singular entities at global
scope, the probability for conflicts is high. All objects with multiplicity
however, such as users, projects, experiments etc. should be optimistically
locked.
