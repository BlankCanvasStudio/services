package storage

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"time"

	"google.golang.org/protobuf/proto"
	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/portal/services/internal"
)

const (
	VlanMergeReserved  = 100  // reserved for Merge
	VlanSwitchReserved = 1000 // reserved for Cumulus
	VlanMax            = 1<<12 - 1 - VlanSwitchReserved
	VlanMin            = VlanMergeReserved
	VtepMergeReserved  = 100 // reserved for Merge
	VtepSwitchReserved = 0
	VtepMax            = 1<<24 - 1 - VtepSwitchReserved
	VtepMin            = VtepMergeReserved
)

// Uses a set backend implementation and Etcd as a substrate, so
// must be kept small
type Vset struct {
	*internal.Set
	Ver int64
}

func NewVset(name string) *Vset {
	return &Vset{
		Set: internal.NewSet(name),
	}
}

// ObjectIO implementation ====================================================

func (v *Vset) Bucket() string       { return "vlist" }
func (v *Vset) Id() string           { return v.Name }
func (v *Vset) Key() string          { return fmt.Sprintf("/%s/%s", v.Bucket(), v.Id()) }
func (v *Vset) Value() interface{}   { return v.Set }
func (v *Vset) Substrate() Substrate { return Etcd }
func (v *Vset) GetVersion() int64    { return v.Ver }
func (v *Vset) SetVersion(x int64)   { v.Ver = x }

func (v *Vset) Create() (*Rollback, error) {
	return etcdTx(writeOps(v)...)
}

func (v *Vset) Read() error {
	_, err := etcdTx(readOps(v)...)
	return err
}

func (v *Vset) Update() (*Rollback, error) {
	return v.Create()
}

func (v *Vset) Delete() (*Rollback, error) {
	return etcdTx(delOps(v)...)
}

// Helpers ====================================================================

func (v *Vset) GetAny() (uint32, error) {
	val, err := v.Set.DelAnyElement()
	return uint32(val), err
}

func (v *Vset) Get(val uint32) error {
	return v.Set.DelElement(uint64(val))
}

func (v *Vset) Put(val uint32) error {
	return v.Set.AddElement(uint64(val))
}

func (v *Vset) Contains(val uint32) bool {
	return v.Set.Contains(uint64(val))
}

func ReadVsets(vs []*Vset) (*Rollback, error) {
	var objs []ObjectIO
	for _, v := range vs {
		objs = append(objs, v)
	}

	rb, err := etcdTx(readOps(objs...)...)
	if err != nil {
		return rb, err
	}

	// populate sets on first read
	for _, v := range vs {
		if v.Ver == 0 {
			for i := VlanMin; i < VlanMax; i++ {
				v.Put(uint32(i))
			}
		}
	}

	return rb, nil
}

func WriteVsets(vs []*Vset) (*Rollback, error) {
	var objs []ObjectIO
	for _, v := range vs {
		objs = append(objs, v)
	}

	return etcdTx(writeOps(objs...)...)
}

// Uses countset backend with minio as substrate. We need this because VNI lists can
// easily grow well beyond the 1.5 MiB etcd limit
//
// Though this is stored in minio, we use an etcd lock to protect it, which
// makes this a little bit of a messy object definition
type Vlist struct {
	*internal.CountSet
	Ver int64
}

// Storage API functions ======================================================

// We need to implement the object interface to make use of etcd locks
func (vl *Vlist) Bucket() string             { return "vlist" }
func (vl *Vlist) Id() string                 { return vl.CountSet.Name }
func (vl *Vlist) Key() string                { return fmt.Sprintf("/%s/%s", vl.Bucket(), vl.Id()) }
func (vl *Vlist) Value() interface{}         { return vl.CountSet }
func (vl *Vlist) Substrate() Substrate       { return MinIO }
func (vl *Vlist) GetVersion() int64          { return vl.Ver }
func (vl *Vlist) SetVersion(ver int64)       { vl.Ver = ver }
func (vl *Vlist) Create() (*Rollback, error) { return nil, fmt.Errorf("not implemented") }
func (vl *Vlist) Update() (*Rollback, error) { return nil, fmt.Errorf("not implemented") }
func (vl *Vlist) Delete() (*Rollback, error) { return nil, fmt.Errorf("not implemented") }

func (vl *Vlist) Read() error {
	bucket := "vlist"

	err := EnsureVlistBucket()
	if err != nil {
		return err
	}

	obj, err := MinIOClient.GetObject(
		context.TODO(),
		bucket,
		vl.Name,
		minio.GetObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("failed to get vlist minIO object: %v", err)
	}

	buf, err := ioutil.ReadAll(obj)
	if err != nil {
		// no data, assume first read
		log.Infof("could not read vlist -- assume doesn't yet exist")
		vl.CountSet = &internal.CountSet{
			Name:   vl.Id(),
			Offset: VtepMin,
			Size:   VtepMax - VtepMin,
			Values: nil,
		}
		return nil
	}

	err = proto.Unmarshal(buf, vl.CountSet)
	if err != nil {
		return fmt.Errorf("failed to unmarshal vlist minIO object: %v", err)
	}

	return nil
}

func (vl *Vlist) Write() error {
	bucket := "vlist"

	err := EnsureVlistBucket()
	if err != nil {
		return err
	}

	buf, err := proto.Marshal(vl.CountSet)
	if err != nil {
		return fmt.Errorf("failed to marshal vlist minIO object: %v", err)
	}

	_, err = MinIOClient.PutObject(
		context.TODO(),
		bucket,
		vl.Name,
		bytes.NewReader(buf),
		int64(len(buf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("failed to get vlist minIO object: %v", err)
	}

	return nil
}

// Helpers ====================================================================

func NewVlist(name string, size, offset uint32) *Vlist {
	return &Vlist{
		CountSet: &internal.CountSet{
			Name:   name,
			Size:   uint64(size),
			Offset: uint64(offset),
		},
	}
}

func EnsureVlistBucket() error {

	bucket := "vlist"

	found, err := MinIOClient.BucketExists(
		context.TODO(),
		bucket,
	)
	if err != nil {
		return fmt.Errorf("failed to check vlist bucket: %v", err)
	}

	if found {
		return nil
	}

	err = MinIOClient.MakeBucket(
		context.TODO(),
		bucket,
		minio.MakeBucketOptions{},
	)
	if err != nil {
		return fmt.Errorf("failed to make vlist bucket: %v", err)
	}

	return nil

}

func (vl *Vlist) Lock() (*Lock, error) {
	return lock(lockpath(vl))
}

func (vl *Vlist) Unlock(lk *Lock) error {
	return lk.Unlock()
}

func (vl *Vlist) Next() (uint32, error) {

	var val uint64
	var err error

	val, *vl.CountSet, err = vl.CountSet.Add()
	if err != nil {
		return 0, err
	}

	return uint32(val), nil
}

func (vl *Vlist) Free(val uint32) error {
	*vl.CountSet = vl.CountSet.Remove(uint64(val))
	return nil
}

// VlanAllocationTable ========================================================

type VlanAllocationTable struct {
	// hosts participating in the VID allocation
	VidHosts []string

	// name of the VNI table
	VniTable string
}

func NewVlanAllocationTable(hosts []string, vnitable string) *VlanAllocationTable {
	return &VlanAllocationTable{
		VidHosts: hosts,
		VniTable: vnitable,
	}
}

func (vat *VlanAllocationTable) tryAllocateVid() (uint32, bool, error) {
	var vsets []*Vset
	var vid uint32
	var err error
	var putBack []uint32

	for _, h := range vat.VidHosts {
		vsets = append(vsets, NewVset(h))
	}

	_, err = ReadVsets(vsets)
	if err != nil {
		return 0, false, fmt.Errorf("failed to read vsets: %v", err)
	}

	vid = 0

	for {
		// get any random free VID in the first set
		v, err := vsets[0].GetAny()
		if err != nil {
			log.Errorf("could not get free VID from set '%s': %v", vsets[0].Name, err)
			break
		}

		// see if this VID is free on the rest
		avail := true
		for _, vset := range vsets[1:] {
			if !vset.Contains(v) {
				avail = false
				break
			}
		}

		if avail {
			vid = v
			break
		}

		putBack = append(putBack, v)
	}

	// put unavailable VIDs back into vset 0
	for _, v := range putBack {
		vsets[0].Put(v)
	}

	if vid == 0 {
		return 0, false, fmt.Errorf("insufficient VIDs available")
	}

	// remove selected VID from each vset
	for _, vset := range vsets[1:] {
		err = vset.Get(vid)
		if err != nil {
			return 0, false, fmt.Errorf("vset internal err: %v", err)
		}
	}

	_, err = WriteVsets(vsets)
	if err != nil {
		return 0, true, fmt.Errorf("failed to write vsets: %v", err)
	}

	return vid, false, nil
}

func (vat *VlanAllocationTable) tryFreeVid(vid uint32) (bool, error) {
	var vsets []*Vset

	for _, h := range vat.VidHosts {
		vsets = append(vsets, NewVset(h))
	}

	_, err := ReadVsets(vsets)
	if err != nil {
		return false, fmt.Errorf("failed to read vsets: %v", err)
	}

	for _, vset := range vsets {
		err := vset.Put(vid)
		if err != nil {
			return false, fmt.Errorf("failed to free VID %d to vset %s: %v", vid, vset.Name, err)
		}
	}

	_, err = WriteVsets(vsets)
	if err != nil {
		return true, fmt.Errorf("failed to write vsets: %v", err)
	}

	return false, nil
}

// Find a VID that is free on each host in the VAT
// This involves read/modify/write over a collection of switches and hosts, and
// so could fail, particularly for large realizations. Use a retry loop to catch
// them
func (vat *VlanAllocationTable) AllocateVid() (uint32, error) {
	var err error

	retries := 5
	for i := 1; i <= retries; i++ {
		var txn_failed bool
		var vid uint32

		vid, txn_failed, err = vat.tryAllocateVid()

		// only retry on txn failures
		if err == nil || !txn_failed {
			return vid, err
		}

		log.Warnf("etcd txn failure on VlanAllocationTable ... %d retries left", retries-i)
		time.Sleep(time.Duration(i*10) * time.Millisecond)
	}

	if err != nil {
		return 0, fmt.Errorf("AllocateVid: %+v", err)
	}

	return 0, nil
}

// Release the VID back to each host set
func (vat *VlanAllocationTable) FreeVid(vid uint32) error {
	var err error

	retries := 5
	for i := 1; i <= retries; i++ {
		var txn_failed bool

		txn_failed, err = vat.tryFreeVid(vid)

		// only retry on txn failures
		if err == nil || !txn_failed {
			return err
		}

		log.Warnf("etcd txn failure on VlanAllocationTable ... %d retries left", retries-i)
		time.Sleep(time.Duration(i*10) * time.Millisecond)
	}

	return err
}

// Allocate a globally free VNI
// VNI is a global entity, use a lock
func (vat *VlanAllocationTable) AllocateVni() (uint32, error) {
	vl := NewVlist(vat.VniTable, VtepMin, VtepMax-VtepMin)

	lk, err := vl.Lock()
	if err != nil {
		return 0, fmt.Errorf("failed to lock vlist %s: %v", vl.Name, err)
	}
	defer vl.Unlock(lk)

	err = vl.Read()
	if err != nil {
		return 0, fmt.Errorf("failed to read vlist: %v", err)
	}

	vni, err := vl.Next()
	if err != nil {
		return 0, fmt.Errorf("failed to allocate VNI: %v", err)
	}

	err = vl.Write()
	if err != nil {
		return 0, fmt.Errorf("failed to write vlist: %v", err)
	}

	return vni, err
}

// Free a VNI back to the pool
// VNI is a global entity, use a lock
func (vat *VlanAllocationTable) FreeVni(vni uint32) error {
	vl := NewVlist(vat.VniTable, VtepMin, VtepMax-VtepMin)

	lk, err := vl.Lock()
	if err != nil {
		return fmt.Errorf("failed to lock vlist %s: %v", vl.Name, err)
	}
	defer vl.Unlock(lk)

	err = vl.Read()
	if err != nil {
		return fmt.Errorf("failed to read vlist: %v", err)
	}

	err = vl.Free(vni)
	if err != nil {
		return fmt.Errorf("failed to free VNI %d: %v", vni, err)
	}

	err = vl.Write()
	if err != nil {
		return fmt.Errorf("failed to write vlist: %v", err)
	}

	return nil
}
