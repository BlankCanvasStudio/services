package storage

type Substrate int

const (
	Etcd Substrate = iota + 1
	MinIO
)

// TODO: Deprecate with the new potpourri storage library

//TODO the original idea here was for the object interface to pull double
//transaction duty with etcd and minio, but this did not really pan out and we
//just wound up using it for etcd, so i think we can remove the minio specific
//trappings
type ObjectIO interface {
	Bucket() string //XXX
	Id() string     //XXX
	Key() string
	Value() interface{}
	Substrate() Substrate //XXX
	GetVersion() int64
	SetVersion(int64)
}

type Object interface {
	ObjectIO
	Create() (*Rollback, error)
	Read() error
	Update() (*Rollback, error)
	Delete() (*Rollback, error)
}

// Support for types that want to marshal and unmarshal themselves.
type StorageMessage interface {
	Marshal() ([]byte, error)
	Unmarshal([]byte) error
}
