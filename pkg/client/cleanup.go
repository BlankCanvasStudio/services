package client

import (
//log "github.com/sirupsen/logrus"
//"gitlab.com/mergetb/api/portal/v1/go"
//me "gitlab.com/mergetb/portal/services/pkg/merror"
)

// FreeUserResources - delete all resouces held by the user.
func FreeUserResources(uid string) error {

	/*TODO TODO TODO TODO
	log.Infof("Freeing user resources %s", uid)

	prjs, err := ListProjects()
	if err != nil {
		return me.Log(err)
	}

	for _, p := range prjs {

		ms, err := ReadProjectMembers(p.Name)
		if err != nil {
			return me.Log(err)
		}

		for username, m := range ms {

			if m.Username == uid {

				if m.Role == xp.Member_Creator {

					err = DeleteProject(uid, p.Name)
					if err != nil {
						return me.Log(err)
					}

					log.Infof("Deleted project %s created by %s", p.Name, uid)

				} else {

					err = DeleteProjectMember(p.Name, uid)
					if err != nil {
						return me.Log(err)
					}

					log.Infof("Deleted %s from project %s", uid, p.Name)
				}
			}
		}
	}
	*/

	return nil
}

// FreeProjectResources - free all experiment resources and project member resources.
func FreeProjectResources(pid string) error {

	/*TODO TODO TODO TODO
	log.WithFields(log.Fields{"pid": pid}).Info("forcably deleting project resources")

	exps, err := ListExperiments(pid)
	if err != nil {
		return err
	}

	for _, exp := range exps {
		err = FreeExperimentResources(pid, exp.Name)
		if err != nil {
			return err
		}
		err = DeleteExperiment(exp.Name, pid)
		if err != nil {
			return err
		}
	}

	xdcs, err := ListXdcs(pid)
	if err != nil {
		return nil
	}

	for _, xdc := range xdcs {

		if xdc.Attached != nil {
			DeleteClientContainerConnection(
				pid,
				xdc.Attached.Eid,
				xdc.Name,
				xdc.Attached.Rid,
			)
		}

		// GTL TODO/FIX: DestroyXdc does nothing with the user field. remove it from the func.
		err := DestroyXdc("", pid, xdc.Name)
		if err != nil {
			return err
		}

	}
	*/

	// GTL TODO free project member resources.

	return nil
}

// FreeExperimentResources - free the exeperiments XDCs and realizations.
func FreeExperimentResources(pid, eid string) error {

	/*TODO TODO TODO TODO
	log.WithFields(log.Fields{"pid": pid, "eid": eid}).Info("forcably deleting exp resources")

	rlzs, err := ListRealizations(pid, eid)
	if err != nil {
		return nil
	}

	for _, rid := range rlzs {
		err = DeleteRealization(pid, eid, rid)
		if err != nil {
			return err
		}
	}

	*/
	// GTL do we need to free experiment member resources here?

	return nil // \o/
}
