package workspace

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"sync"
	"testing"
	"time"
)

func TestPasswdMerge(t *testing.T) {
	// passwd_1 is:
	// root:x:0:0:root:/home/root:/bin/bash
	// sshd:x:1:0:sshd:/home/sshd:/bin/bash
	// murphy:x:1000:1000:murphy:/home/murphy:/bin/bash
	p1, err := OpenPasswd("data/passwd_1")
	if err != nil {
		t.Fatal(err)
	}
	defer p1.Close()

	// passwd_2 is a "normal" passwd file that has at least another root
	// and an "_apt" user.
	p2, err := OpenPasswd("data/passwd_2")
	if err != nil {
		t.Fatal(err)
	}
	defer p2.Close()

	t.Logf("p1:\n%s", p1.String())
	t.Logf("p2:\n%s", p1.String())

	err = p1.Merge(p2)
	if err != nil {
		t.Fatal(err)
	}

	if _, ok := p1.Entries["_apt"]; !ok {
		t.Fatal("merge fail")
	}
	if _, ok := p1.Entries["murphy"]; !ok {
		t.Fatal("merge fail")
	}

	pe := &PasswdEntry{
		Username: "geoff",
		Uid:      6666,
		Gid:      6667,
		Shell:    "/bin/bash",
	}
	p2.Add(pe)

	if ok := p2.Contains(pe); !ok {
		t.Fatal("add/contains fail")
	}

	err = p1.Merge(p2)
	if err != nil {
		t.Fatal(err)
	}

	if ok := p1.Contains(pe); !ok {
		t.Fatal("merge 2 fail")
	}

	t.Logf("merged:\n%s", p1.String())
}

func TestGroupsMerge(t *testing.T) {
	// g1:
	// root:x:0:
	// admin:x:1:root
	// _apt:x:2:
	g1, err := OpenGroup("data/group1")
	if err != nil {
		t.Fatal(err)
	}
	defer g1.Close()

	// g2:
	// admin:x:1:murphy
	// murphy:x:1000:
	g2, err := OpenGroup("data/group2")
	if err != nil {
		t.Fatal(err)
	}
	defer g2.Close()

	t.Logf("g1:\n %s", g1.String())
	t.Logf("g2:\n %s", g2.String())

	g1.Merge(g2)

	if _, ok := g1.Entries["murphy"]; !ok {
		t.Fatalf("bad group merge\n\n%s", g1.String())
	}

	if len(g1.Entries["admin"].Users) != 2 {
		t.Fatalf("bad group users merge\n\n%s", g1.String())
	}

	t.Logf("merged:\n %s", g1.String())

	err = g1.AddUser("admin", "olive")
	if err != nil {
		t.Fatal(err)
	}

	// a little awkward but it gets the job done.
	if !strings.Contains(strings.Join(g1.Entries["admin"].Users, ","), "olive") {
		t.Fatal("error adding user to group")
	}

	t.Logf("admin w/olive:\n%s", g1.String())
}

func TestAddToGroup(t *testing.T) {

	g, err := OpenGroup("data/group1")
	if err != nil {
		t.Fatal(err)
	}
	defer g.Close()

	err = g.AddAllToGroup("admin", "data/passwd_1")
	if err != nil {
		t.Fatal("add all to group")
	}

	// This should've added murphy and henry to admin.
	admin := g.Entries["admin"]
	if !admin.userExists("henry") {
		t.Fatal("add to group fail")
	}
	if !admin.userExists("murphy") {
		t.Fatal("add to group fail")
	}

	t.Logf("group: %s", g.String())

	cnt := len(admin.Users)

	// should not add dups/re-add all users
	err = g.AddAllToGroup("admin", "data/passwd_1")
	if err != nil {
		t.Fatal("add all to group")
	}

	if cnt != len(g.Entries["admin"].Users) {
		t.Logf("failed group:\n%s", g.String())
		t.Fatal("added dups to group")
	}
}

func addGroup(delay int, wg *sync.WaitGroup, t *testing.T, name string, ID int) error {

	defer wg.Done()

	time.Sleep(time.Duration(delay) * time.Millisecond)

	g, err := OpenGroup("data/group")
	if err != nil {
		t.Fatal("bad group open")
	}
	defer g.Close()

	g.AddEntry(&GroupEntry{
		Name:  name,
		Id:    ID,
		Users: []string{"paul", "ringo"},
	})

	g.Write()

	return nil
}

func TestMultiAddGroup(t *testing.T) {

	var wg sync.WaitGroup

	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	lines := []byte("root:x:0:\nlp:x:1:")
	ioutil.WriteFile("data/group", lines, 0755)

	for i := 1000; i <= 1100; i++ {
		wg.Add(1)
		go addGroup(r.Intn(1000), &wg, t, fmt.Sprintf("group%d", i), i)
	}

	wg.Wait()

	os.Remove("data/group")
}
