package reconcile

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// Return the heartbeat interval from portal config
// Returns 0 if no config present
func HeartbeatIntervalFromConfig() uint64 {
	config, err := storage.GetPortalConfig()
	if err != nil {
		log.Errorf("failed to read portal config: %+v", err)
		return 0
	}

	return uint64(config.Config.Reconcile.HeartbeatIntervalSec)
}

// Return the heartbeat grace period from portal config
// Returns -1 if heartbeat monitoring is disabled
// Returns 0 if no config present
func HeartbeatGracePeriodFromConfig() int64 {
	// disable by request
	if !internal.MonitorEtcdServer {
		return -1
	}

	config, err := storage.GetPortalConfig()
	if err != nil {
		log.Errorf("failed to read portal config: %+v", err)
		return 0
	}

	return int64(config.Config.Reconcile.HeartbeatGracePeriodSec)
}
