package merror

import (
	"errors"
	"fmt"

	mcc "gitlab.com/mergetb/mcc/pkg"
)

var (
	ErrMxCompileError    = errors.New("MX failed to compile")
	ErrReticulationError = errors.New("Reticulation Error")
)

func MxCompileError(errStack string) error {
	me := NewMergeError(ErrMxCompileError)
	me.Detail = "Compilation Failure"
	me.Type = "https://mergetb.org/errors/mx-failed-to-compile"

	// GTL TODO parse out error stack into a resonably readable set of things.
	me.Evidence = errStack
	return me
}

func ModelReticulationError(err error) error {

	me := NewMergeError(ErrReticulationError)
	me.Detail = err.Error()

	if errors.Is(err, mcc.ErrNoAddressOnEndpoint) {
		me.Type = "https://mergetb.org/errors/routes-need-addresses"
		me.Err = fmt.Errorf("%w", err)
	} else if errors.Is(err, mcc.ErrDuplicateSubnets) {
		me.Type = "https://mergetb.org/errors/duplicate-subnets-in-addressing"
		me.Err = fmt.Errorf("%w", err)
	} else if errors.Is(err, mcc.ErrMultipleLinkSubnets) {
		me.Type = "https://mergetb.org/errors/multiple-link-subnets-in-addressing"
		me.Err = fmt.Errorf("%w", err)
	}

	return me
}
