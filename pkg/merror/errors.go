package merror

import (
	"errors"
	"fmt"
	"time"
)

// MergeError encapsulates an error in a Merge Portal.
// This currently mirrors the model.Error, but is written out again here
// to keep the API data structures separate from the internal Merge
// data structures. It also wraps the underlying error for tpe comparison
// and any other data it holds
//
// Informational fields in the struct are based on RFC 7807
// https://tools.ietf.org/html/rfc7807
type MergeError struct {
	Type      string `json:"type,omitempty"`
	Title     string `json:"title,omitempty"`
	Detail    string `json:"detail,omitempty"`
	Instance  string `json:"instance,omitempty"`
	Evidence  string `json:"evidence,omitempty"`
	Timestamp string `json:"timestamp,omitempty"`
	Err       error  `json:"err,omitempty"`
}

func NewMergeError(err error) *MergeError {

	title := "Untitled Error"
	if err != nil {
		title = err.Error()
	}

	return &MergeError{
		Type:      "",
		Title:     title,
		Detail:    "",
		Instance:  "",
		Evidence:  "",
		Timestamp: time.Now().String(),
		Err:       err,
	}
}

//
// golang Error Interface.
//

// Error implements the go error interface.
func (me MergeError) Error() string {
	e := "None"
	if me.Err != nil {
		e = me.Err.Error()
	}
	return fmt.Sprintf(
		"Type: %s, Title: %s, Detail: %s, Instance: %s, Evidence: %s, Timestamp: %s, Err: %s",
		me.Type,
		me.Title,
		me.Detail,
		me.Instance,
		me.Evidence,
		me.Timestamp,
		e,
	)
}

func (me *MergeError) Unwrap() error {
	return me.Err
}

func (me *MergeError) Is(target error) bool {
	_, ok := target.(*MergeError)
	return ok
}

//
// Standard Errors
//
var (
	// Standard Errors
	ErrBadRequest    = errors.New("Bad Request")
	ErrUnauthorized  = errors.New("Unauthorized")
	ErrForbidden     = errors.New("Forbidden")
	ErrUserInactive  = errors.New("User Inactive")
	ErrNotFound      = errors.New("Not Found")
	ErrAlreadyExists = errors.New("Already Exists")
	ErrInternal      = errors.New("Internal Error")
)

func BadRequestError(detail string) *MergeError {
	me := NewMergeError(ErrBadRequest)
	me.Detail = detail
	me.Type = "https://mergetb.org/errors/bad-request-error"
	return me
}

func UnauthorizedError(detail string) *MergeError {
	me := NewMergeError(ErrUnauthorized)
	me.Detail = detail
	me.Type = "https://mergetb.org/errors/unauthorized-error"
	return me
}

func ForbiddenError(detail string) *MergeError {
	me := NewMergeError(ErrForbidden)
	me.Detail = detail
	me.Type = "https://mergetb.org/errors/forbidden-error"
	return me
}

func UserInactiveError(detail string) *MergeError {
	me := NewMergeError(ErrUserInactive)
	me.Detail = detail
	me.Type = "https://mergetb.org/errors/userinactive-error"
	return me
}

func NotFoundError(what, instance string) *MergeError {
	me := NewMergeError(ErrNotFound)
	if what != "" {
		me.Detail = fmt.Sprintf("%s: %s not found", what, instance)
	} else {
		me.Detail = fmt.Sprintf("%s not found", instance)
	}
	me.Type = "https://mergetb.org/errors/not-found-error"
	me.Instance = instance
	return me
}

func AlreadyExistsError(what, instance string) *MergeError {
	me := NewMergeError(ErrAlreadyExists)
	me.Detail = fmt.Sprintf("%s already exists", what)
	me.Type = "https://mergetb.org/errors/already-exists-error"
	me.Instance = instance
	return me
}

// InternalError is wrapped in a MergeError
func InternalError(title, detail, eType string, err error) *MergeError {
	if err != nil {
		err = fmt.Errorf("%w", err)
	}
	me := NewMergeError(err)
	me.Detail = detail
	me.Title = title
	me.Type = fmt.Sprintf("https://mergetb.org/errors/%s", eType)
	return me
}

func UncategorizedError(detail string, err error) *MergeError {
	if err != nil {
		return InternalError("Uncategorized Error", detail, "uncategorized-error", err)
	}

	return InternalError(
		"Uncategorized Error",
		detail,
		"uncategorized-error",
		errors.New(detail),
	)
}

func DatabaseError(detail string, err error) *MergeError {
	return InternalError("Database Error", detail, "database-error", err)
}

func DataCorruptionError(name string, err error) *MergeError {
	e := NewMergeError(err)
	e.Detail = fmt.Sprintf("Corrupt %s detected: %v", name, err)
	e.Instance = name
	e.Type = "https://mergetb.org/errors/data-corruption"
	return e
}
