package merror

import (
	"fmt"
)

func SvcConnectionError(name string, err error) *MergeError {
	e := NewMergeError(err)
	e.Detail = fmt.Sprintf("Unable to connect to %s service", name)
	e.Instance = name
	e.Type = "https://mergetb.org/errors/merge-service-connection"
	return e
}
