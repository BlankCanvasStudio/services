package merror

import (
	//"encoding/json"
	"errors"
	//"fmt"
	//log "github.com/sirupsen/logrus"
	//rz "gitlab.com/mergetb/api/portal/v1/go/realize"
)

var (
	ErrEmptyExperiment = errors.New("Empty Experiment")
	ErrEmptyTestbed    = errors.New("Empty Testbed")
	ErrRlzBadXir       = errors.New("Bad Realzation XIR")
	ErrRlzInternal     = errors.New("Realization Internal")

	ErrFailedToRealize  = errors.New("Failed to Realize")
	ErrBadRealizeAction = errors.New("Bad Realize Action")

	ErrNoProjectResources = errors.New("No Resources For Project")
)

func EmptyExperimentError() *MergeError {
	return InternalError(
		"Empty Experiment",
		"",
		"empty-experiment",
		ErrEmptyExperiment,
	)
}

func EmptyTestbedError() *MergeError {
	return InternalError(
		"Empty Testbed",
		"",
		"empty-testbed",
		ErrEmptyTestbed,
	)
}

func BadRealizeActionError(badaction, instance string) *MergeError {

	e := NewMergeError(ErrBadRealizeAction)
	e.Detail = "Unallowed realization action. Must be one of \"accept\" or \"reject\""
	e.Instance = instance
	e.Evidence = badaction

	return e
}

/*
func FailedToRealizeError(rid string, diags []*rz.Diagnostic) *MergeError {

	e := NewMergeError(ErrFailedToRealize)
	e.Detail = fmt.Sprintf("Realization %s failed", rid)
	e.Type = "https://mergetb.org/errors/failed-to-realize"
	e.Instance = rid
	e.Evidence = marshalDiagnostics(diags)

	return e
}
*/

func NoProjectResources(pid string) *MergeError {

	e := NewMergeError(ErrNoProjectResources)
	e.Detail = "There are no resources available for this project"
	e.Type = "https://mergetb.org/errors/no-project-resources-available"
	e.Instance = pid

	return e
}

type diagnostic struct {
	Message string
	Level   string
	Data    map[string]string
	Guest   string
	Host    string
}

/*
func marshalDiagnostics(ds []*rz.Diagnostic) string {

	log.Infof("marshalling diags: %+v", ds)

	diags := []diagnostic{}

	for _, d := range ds {
		diags = append(diags, diagnostic{
			Message: d.Message,
			Level:   d.Level.String(),
			Guest:   d.Guest,
			Host:    d.Host,
		})
	}

	result, err := json.Marshal(diags)
	if err != nil {
		log.Info("failed to marshal json diagnostics")
		return ""
	}

	return string(result)
}
*/

// ExplainDiagnostics generates human readable string about the
// diagnostics suitable for display to a human.

// TODO redo for new diagnostics format
func ExplainDiagnostics(diagStr string) ([]string, error) {

	/*
		diags := []diagnostic{}
		err := json.Unmarshal([]byte(diagStr), &diags)
		if err != nil {
			return nil, err
		}

		// TBD more actual smart diagnostics parsing/analysis.
		// NOTE: THIS IS STUPID CODE DONE POORLY. MUST BE MADE SMARTER.
		msgs := []string{}

		def := make(map[string]map[string]int)
		notpres := make(map[string]map[string]int)

		for _, d := range diags {
			switch d.Code {
			case "ConstraintDeficient":
				switch d.Constraint {
				case "available nodes":
					msgs = append(msgs, "Too few available nodes to realize the model")
				default:
					if d.ExpNode != "" && d.Constraint != "" {
						if _, ok := def[d.ExpNode]; !ok {
							def[d.ExpNode] = make(map[string]int)
						}
						def[d.ExpNode][d.Constraint]++
					}
				}
			case "ConstraintNotPresent":
				if d.ExpNode != "" && d.Constraint != "" {
					if _, ok := notpres[d.ExpNode]; !ok {
						notpres[d.ExpNode] = make(map[string]int)
					}
					notpres[d.ExpNode][d.Constraint]++
				}
			case "PhysicallyAllocated":
				// NOOP
			default:
				// NOOP
			}
		}

		for n, cs := range def {
			for c, cnt := range cs {
				msgs = append(msgs, fmt.Sprintf("Node %s: %s deficient %d times", n, c, cnt))
			}
		}
		for n, cs := range notpres {
			for c, cnt := range cs {
				msgs = append(msgs, fmt.Sprintf("Node %s: %s not present %d times", n, c, cnt))
			}
		}

		return msgs, nil
	*/
	return nil, nil
}
