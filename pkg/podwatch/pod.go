package podwatch

import (
	"context"
	"encoding/json"
	"fmt"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

//
// Pod related functions.
//

// Format here is from https://www.rfc-editor.org/rfc/rfc6902 "JavaScript Object Notation (JSON) Patch"
type patchStringValue struct {
	Op    string `json:"op"`
	Path  string `json:"path"`
	Value string `json:"value"`
}

// AddLabel adds the given label to the given pod.
func AddLabel(pod *v1.Pod, key, value string) error {
	return updateLabel(pod, "replace", key, value)
}

// RemoveLabel removes the given label from the given pod.
func RemoveLabel(pod *v1.Pod, key string) error {
	return updateLabel(pod, "remove", key, "")
}

// AddPodLabel adds a label to the XDC pod with the given name.
func AddPodLabel(name, project, namespace, key, value string) error {

	p, err := GetPod(namespace, name, project)
	if err != nil {
		return err
	}

	return AddLabel(p, key, value)
}

// RemovePodLabel - removes the label
func RemovePodLabel(name, project, namespace, key string) error {

	p, err := GetPod(namespace, name, project)
	if err != nil {
		return err
	}

	return RemoveLabel(p, key)
}

// GetPod returns the k8s Pod data gtiven a name and project.
func GetPod(namespace, name, project string) (*v1.Pod, error) {

	pods, err := k8c.CoreV1().Pods(namespace).List(
		context.TODO(),
		metav1.ListOptions{
			LabelSelector: fmt.Sprintf("proj=%s, name=%s", project, name),
		},
	)
	if err != nil {
		return nil, fmt.Errorf("failed to list k8s pods")
	}
	if len(pods.Items) == 0 {
		return nil, fmt.Errorf("pod not found")
	}

	return &pods.Items[0], nil
}

func updateLabel(pod *v1.Pod, op, key, value string) error {

	cli := K8SClient()

	payload := []patchStringValue{{
		Op:   op,
		Path: "/metadata/labels/" + key,
	}}

	if value != "" {
		payload[0].Value = value
	}

	payloadBuf, err := json.Marshal(payload)

	_, err = cli.CoreV1().Pods(pod.GetNamespace()).Patch(
		context.Background(),
		pod.GetName(),
		types.JSONPatchType,
		payloadBuf,
		metav1.PatchOptions{},
	)

	if err != nil {
		return err
	}

	return nil
}
