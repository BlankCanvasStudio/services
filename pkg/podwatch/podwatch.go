package podwatch

import (
	"fmt"
	"time"

	ants "github.com/panjf2000/ants/v2"
	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
)

var (
	k8c *kubernetes.Clientset
)

// PodWatcher encapsulates a client that gets pod events. Podwatcher gets events
// from k8s, filters out the noise, then invokes the given callbacks (OnPodadd, OnPodUpdate,
// OnPodDelete), in a worker thread.
type PodWatcher struct {

	// On* functcions get called when that event happens. The interface is a k8s core API *v1.Pod.
	OnPodAdd    func(pod *v1.Pod)
	OnPodDelete func(obj *v1.Pod)
	OnPodUpdate func(prevObj, newObj *v1.Pod)

	// The namespace in which to watch for events. (The XDC namespace.)
	Namespace string

	// Control callback events.
	Unfiltered bool // if true get all underlying k8s pod events.

	// Number of workers to allocate to handle events. If not given, 10 is used.
	WorkerPoolSize int
}

// MergePodType describes all possible pod types that Merge will spawn.
type MergePodType int32

const (
	// NOTMERGE is not a merge pod.
	NOTMERGE MergePodType = 0

	// XDC is an XDC
	XDC MergePodType = 1

	// JUMP is an ssh jump host
	JUMP MergePodType = 2
)

// InitPodwatch is required to be called befora any podatch methods. Call in your client init().
func InitPodWatch() {

	log.Info("[podwatch] initializing k8s client")

	config, err := rest.InClusterConfig()
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Fatal("[podwatch] failed to fetch kubeconfig")
	}

	k8c, err = kubernetes.NewForConfig(config)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Fatal("[podwatch] failed to create k8s client")
	}
}

// worker interface for pod events.
type workerArgs interface {
}

type addPodArgs struct {
	Pod *v1.Pod
}

type deletePodArgs struct {
	Pod *v1.Pod
}

type updatePodArgs struct {
	Old *v1.Pod
	New *v1.Pod
}

func (pw *PodWatcher) worker(args workerArgs) {

	if args == nil {
		return
	}

	switch args.(type) {
	case addPodArgs:
		log.Debugf("invoking Pod Add handler")
		pw.OnPodAdd(args.(addPodArgs).Pod)

	case updatePodArgs:
		// trace as update events happen very often.
		log.Tracef("invoking Pod Update handler")
		pw.OnPodUpdate(
			args.(updatePodArgs).Old,
			args.(updatePodArgs).New,
		)

	case deletePodArgs:
		log.Debugf("invoking Pod Delete handler")
		pw.OnPodDelete(args.(deletePodArgs).Pod)

	default:
		log.Debugf("Unexpected argument type: %v", args)
	}
}

func K8SClient() *kubernetes.Clientset {
	return k8c
}

// Watch keeps track of pod events and sets up the configured callbacks. This method does
// not return.
func (pw *PodWatcher) Watch() {

	log.Infof("[podwatch] starting pod watch...")

	if pw.OnPodAdd == nil || pw.OnPodDelete == nil || pw.OnPodUpdate == nil {
		log.Fatalf("All function pointets (OnPodAdd, OnPodUpdate, OnPodDelete) must be non-nil in PodWatcher")
	}

	workerNum := 10
	if pw.WorkerPoolSize != 0 {
		workerNum = pw.WorkerPoolSize
	}

	// init worker pool
	pool, err := ants.NewPoolWithFunc(
		workerNum,
		func(i interface{}) {
			if args, ok := i.(workerArgs); ok {
				pw.worker(args)
			}
		},
	)
	if err != nil {
		log.Fatalf("error starting worker pool: %v", err)
	}
	defer pool.Release()

	// init k8s informer.
	factory := informers.NewSharedInformerFactoryWithOptions(
		k8c,
		time.Second*30,
		informers.WithNamespace(pw.Namespace),
		informers.WithTweakListOptions(
			func(opts *metav1.ListOptions) {
				ls := metav1.LabelSelector{
					MatchLabels: map[string]string{
						"component": "xdc",
					},
				}
				opts.LabelSelector = labels.Set(ls.MatchLabels).String()
			},
		),
	)

	informer := factory.Core().V1().Pods().Informer()

	informer.SetWatchErrorHandler(
		func(r *cache.Reflector, err error) {
			log.Tracef("[podwatch] pod watch error: %s", err)
		},
	)

	stop := make(chan struct{})
	defer close(stop)
	defer runtime.HandleCrash()

	// We do some basic sanity checking before invoking the event callbacks.
	// Pods should be Pods, On add and update, the pods whould be running and
	// all containers should be active.
	informer.AddEventHandler(
		cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				if pw.Unfiltered == false && !podReady(obj) {
					log.Debug("[podwatch] ignoring not-ready pod")
					return
				}

				err = pool.Invoke(addPodArgs{obj.(*v1.Pod)})
				if err != nil {
					log.Errorf("Error invoking worker: %v", err)
				}
			},
			DeleteFunc: func(obj interface{}) {

				_, ok := obj.(*cache.DeletedFinalStateUnknown)
				if ok {
					log.Debugf("[podwatch] Got pod delete, but no pod already deleted.")
					return
				}

				pod, ok := obj.(*v1.Pod)
				if !ok {
					log.Debugf("[podwatch] got a non Pod in pod delete handler: %T", obj)
					if unk, ok := obj.(cache.DeletedFinalStateUnknown); ok {
						// This is k8s telling us there *was* a pod, but it does not
						// know the state maymore so YMMV. We'll pass the pod along
						// anyway so clients can do clean up.
						log.Debugf("[podwatch] the non-pod state unk key: %s", unk.Key)
						log.Debugf("[podwatch] the non-pod state unk obj type: %T", unk.Obj)

						pod, ok = unk.Obj.(*v1.Pod)
						if !ok {
							log.Warn("Got a non-pod in delete event")
							return
						}
					}
				}

				_, err := PodType(pod)
				if err != nil {
					log.Errorf("[podwatch] podtype: %v", err)
					return
				}

				err = pool.Invoke(deletePodArgs{pod})
				if err != nil {
					log.Errorf("Error invoking worker: %v", err)
				}

			},
			UpdateFunc: func(oldObj, newObj interface{}) {
				if pw.Unfiltered == false && !podReady(newObj) {
					log.Debug("[podwatch] ignoring not-ready pod")
					return
				}

				oldPod, ok := oldObj.(*v1.Pod)
				if !ok {
					log.Debug("[podwatch] ignoring update as oldObj is not a Pod")
					return
				}

				err = pool.Invoke(updatePodArgs{oldPod, newObj.(*v1.Pod)})
				if err != nil {
					log.Errorf("Error invoking worker: %v", err)
				}
			},
		},
	)

	// TODO: not sure about the infinite loop here or if I handle sync error correctly
	for {
		go informer.Run(stop)

		if cache.WaitForCacheSync(stop, informer.HasSynced) == false {
			runtime.HandleError(fmt.Errorf("Cache sync timeout"))
			continue
		}
		<-stop
	}
}

func podReady(obj interface{}) bool {

	pod, ok := obj.(*v1.Pod)
	if !ok {
		log.Debugf("[podwatch] got a non Pod in onPodAdd")
		return false
	}

	// make sure the pod is running.
	if pod.Status.Phase != v1.PodRunning {
		log.Debugf("[podwatch] Ignoring not-running pod")
		return false
	}

	// Make sure the containers are ready
	for _, cs := range pod.Status.ContainerStatuses {
		if !cs.Ready {
			log.Debugf("[podwatch] Ignoring pod. At least one container is not ready.")
			return false
		}
	}

	_, err := PodType(pod)
	if err != nil {
		log.Errorf("[podwatch] podtype: %v", err)
		return false
	}

	return true
}

// PodType is a utility function that takes a pod and returns its type.
func PodType(pod *v1.Pod) (MergePodType, error) {
	instance, ok := pod.Labels["instance"]
	if !ok {
		return NOTMERGE, fmt.Errorf("missing instance label")
	}

	switch instance {
	case "xdc":
		return XDC, nil
	case "jump":
		return JUMP, nil
	}

	return NOTMERGE, fmt.Errorf("Not a merge pod. No instance specified")
}

// PodName returns the Merge pod name,project for the pod is it is an XDC.
func PodName(pod *v1.Pod) (string, string, error) {

	name, ok := pod.Labels["name"]
	if !ok {
		return "", "", fmt.Errorf("no name for pod")
	}

	proj, ok := pod.Labels["proj"]
	if !ok {
		return "", "", fmt.Errorf("no project for pod")
	}

	return name, proj, nil
}
