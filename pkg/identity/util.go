package identity

import (
	"fmt"

	ory "github.com/ory/kratos-client-go"
	log "github.com/sirupsen/logrus"
)

func (i *Identity) FromOryID(oryId *ory.Identity) error {

	// TODO: download the schema in id.SchemaURL, decode traits, and confirm
	// the data we need is there. For now, assume email + username exist.

	log.Debugf("traits: %+v", oryId.Traits)

	oryTraits, ok := oryId.Traits.(map[string]interface{})
	if !ok {
		return fmt.Errorf("Unknown identity traits type")
	}

	i.Id = oryId.Id

	return i.Traits.Read(oryTraits)
}

func (i *IdentityTraits) Read(oryTraits map[string]interface{}) error {

	// generics would be nice here.
	getStr := func(key string) string {
		if val, ok := oryTraits[key]; ok {
			r, ok := val.(string)
			if ok {
				return r
			}
		}
		return ""
	}

	i.Email = getStr("email")
	i.Username = getStr("username")

	for k, trait := range oryTraits {

		if k == "email" || k == "username" {
			continue
		}

		if val, ok := trait.(string); ok {
			i.Traits[k] = val
		}
	}

	return nil
}
