package policy

import (
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	me "gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

//
// NOTE: there are no accessmodes for Pools even though the policy states there are.
// There is only `Public` mode for now.
//

type PoolObject struct {
	Pool *portal.Pool

	// If given, evaluate user wrt these fields.
	UpdateFacility     string
	UpdateProject      string
	UpdateOrganization string
}

func (o PoolObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	if o.Pool == nil {
		return nil, nil
	}

	roles := []RoleBinding{}

	if o.Pool.Creator == u.Username {
		roles = append(roles, RoleBinding{PoolScope, CreatorRole})
	}

	if u.Admin {
		roles = append(roles, RoleBinding{PoolScope, CreatorRole})
	}

	if o.UpdateFacility != "" {

		fac := storage.NewFacility(o.UpdateFacility)
		err := fac.Read()
		if err != nil {
			return nil, err
		}

		fRoles, err := FacilityObject{fac.Facility}.UserRoles(u)
		if err != nil {
			return nil, err
		}

		roles = append(roles, fRoles...)
	}

	for f := range o.Pool.Facilities {

		fac := storage.NewFacility(f)
		err := fac.Read()
		if err != nil {
			return nil, err
		}

		fRoles, err := FacilityObject{fac.Facility}.UserRoles(u)
		if err != nil {
			return nil, err
		}

		roles = append(roles, fRoles...)
	}

	if o.UpdateProject != "" {

		proj := storage.NewProject(o.UpdateProject)
		err := proj.Read()
		if err != nil {
			return nil, err
		}

		pRoles, err := ProjectObject{proj.Project}.UserRoles(u)
		if err != nil {
			return nil, err
		}

		roles = append(roles, pRoles...)
	}

	if o.UpdateOrganization != "" {

		org := storage.NewOrganization(o.UpdateOrganization)
		err := org.Read()
		if err != nil {
			return nil, err
		}

		pRoles, err := OrganizationObject{org.Organization}.UserRoles(u)
		if err != nil {
			return nil, err
		}

		roles = append(roles, pRoles...)
	}

	return roles, nil
}

func ReadPools(caller *identity.IdentityTraits) error {

	pol := GetPolicy().Pool[Public].Read
	return Authorize(caller, pol, PoolObject{})
}

func CreatePool(caller *identity.IdentityTraits) error {

	reqs := GetPolicy().Pool[Public].Create
	return AuthorizeCreate(caller, reqs, PoolScope)
}

func ReadPool(caller *identity.IdentityTraits, pool string) error {

	reqs := GetPolicy().Pool[Public].Read
	if p, err := getPool(pool); err == nil {
		return Authorize(caller, reqs, PoolObject{Pool: p})
	}
	return me.NotFoundError("pool does not exist", pool)
}

func UpdatePoolProject(caller *identity.IdentityTraits, pool, project string) error {

	reqs := GetPolicy().Pool[Public].UpdateProject
	if p, err := getPool(pool); err == nil {
		return Authorize(caller, reqs, PoolObject{Pool: p, UpdateProject: project})
	}
	return me.NotFoundError("pool does not exist", pool)
}

func UpdatePoolOrganization(caller *identity.IdentityTraits, pool, organization string) error {

	reqs := GetPolicy().Pool[Public].UpdateOrganization
	if p, err := getPool(pool); err == nil {
		return Authorize(caller, reqs, PoolObject{Pool: p, UpdateOrganization: organization})
	}
	return me.NotFoundError("pool does not exist", pool)
}

func UpdatePoolFacility(caller *identity.IdentityTraits, pool, facility string) error {

	reqs := GetPolicy().Pool[Public].UpdateFacility
	if p, err := getPool(pool); err == nil {
		return Authorize(caller, reqs, PoolObject{Pool: p, UpdateFacility: facility})
	}
	return me.NotFoundError("pool does not exist", pool)
}

func DeletePool(caller *identity.IdentityTraits, pool string) error {

	reqs := GetPolicy().Pool[Public].Delete
	if p, err := getPool(pool); err == nil {
		return Authorize(caller, reqs, PoolObject{Pool: p})
	}
	return me.NotFoundError("pool does not exist", pool)
}

func getPool(pool string) (*portal.Pool, error) {

	p := storage.NewPool(pool)
	err := p.Read()
	if err != nil {
		return nil, err
	}

	return p.Pool, nil
}
