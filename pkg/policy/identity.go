package policy

import (
	// log "github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

type IdentityObject struct {
	Identity    *identity.IdentityTraits
	constituent string
}

func (o IdentityObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	roles := []RoleBinding{}

	// There is no identity user roles, so just map to Portal Maintainer
	// if id is an admin.

	if o.Identity.Username == PolicyAdmin || u.Admin == true {
		// Map all admin to maintainer. There isn't a distinct portal "creator".
		roles = append(roles, RoleBinding{PortalScope, MaintainerRole})
	}

	// See if the user is a requested member of an Organization we are in.
	for name := range u.Organizations {
		org := storage.NewOrganization(name)
		err := org.Read()
		if err != nil {
			log.Warnf("Error getting reading org: %s", name)
			continue
		}

		for name := range org.GetMembers() {
			if name == o.constituent {
				rs, err := OrganizationObject{Organization: org.Organization}.UserRoles(u)
				if err == nil {
					roles = append(roles, rs...)
				}
			}
		}
	}

	return roles, nil
}

func ReadIdentities(caller *identity.IdentityTraits) error {

	reqs := GetPolicy().Identity.Read
	return Authorize(caller, reqs, IdentityObject{Identity: caller})
}

func RegisterUser(caller *identity.IdentityTraits, username string) error {

	pol := GetPolicy().Identity.Register
	return idUserAuthorize(caller, pol, username)
}

func UnregisterUser(caller *identity.IdentityTraits, username string) error {

	pol := GetPolicy().Identity.Unregister
	return idUserAuthorize(caller, pol, username)
}

func InitUser(caller *identity.IdentityTraits, username string) error {

	pol := GetPolicy().Identity.Init
	return idUserAuthorize(caller, pol, username)
}

func ActivateUser(caller *identity.IdentityTraits, username string) error {

	pol := GetPolicy().Identity.UpdateState
	return idUserAuthorize(caller, pol, username)
}

func DeactivateUser(caller *identity.IdentityTraits, username string) error {

	pol := GetPolicy().Identity.UpdateState
	return idUserAuthorize(caller, pol, username)
}

func idUserAuthorize(caller *identity.IdentityTraits, policy []RoleBinding, username string) error {

	return Authorize(caller, policy, IdentityObject{Identity: caller, constituent: username})
}
