package policy

import (
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// Policy Object interface implementation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ProjectObject ...
type ProjectObject struct {
	Project *portal.Project
}

// UserRoles for projects
func (p ProjectObject) UserRoles(u *portal.User) ([]RoleBinding, error) {

	result := []RoleBinding{}

	member, ok := p.Project.Members[u.Username]
	if ok {
		switch member.State {

		case portal.Member_Pending:
			// no role bindings for pending members

		case portal.Member_Active:
			switch member.Role {

			case portal.Member_Creator:
				result = append(result, RoleBinding{ProjectScope, CreatorRole})

			case portal.Member_Maintainer:
				result = append(result, RoleBinding{ProjectScope, MaintainerRole})

			case portal.Member_Member:
				result = append(result, RoleBinding{ProjectScope, MemberRole})

			}

		}
	}

	// apply any Organization roles
	if p.Project.Organization != "" {
		o := storage.NewOrganization(p.Project.Organization)
		err := o.Read()
		if err == nil {
			roles, err := OrganizationObject{Organization: o.Organization}.UserRoles(u)
			if err == nil {
				result = append(result, roles...)
			}
		}
	}

	if u.Admin {
		result = append(result, RoleBinding{ProjectScope, CreatorRole})
	}

	return result, nil
}

// Policy API for Projects ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// CreateProject policy
func CreateProject(user *identity.IdentityTraits, project string) error {

	requirements := GetPolicy().Project[Public].Create
	return AuthorizeCreate(user, requirements, ProjectScope)
}

// ReadProject policy
func ReadProject(user *identity.IdentityTraits, project string) error {

	p := storage.NewProject(project)
	err := p.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Project[Mode(p.AccessMode)].Read
	return Authorize(user, requirements, ProjectObject{p.Project})
}

// UpdateProject policy
func UpdateProject(user *identity.IdentityTraits, project string) error {

	p := storage.NewProject(project)
	err := p.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Project[Mode(p.AccessMode)].Update

	return Authorize(user, requirements, ProjectObject{p.Project})
}

// DeleteProject policy
func DeleteProject(user *identity.IdentityTraits, project string) error {

	p := storage.NewProject(project)
	err := p.Read()
	if err != nil {
		return err
	}

	requirements := GetPolicy().Project[Mode(p.AccessMode)].Delete
	return Authorize(user, requirements, ProjectObject{p.Project})
}
