package policy

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/identity"
	me "gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

type UserObject struct {
	*portal.User
}

func (o UserObject) UserRoles(requestor *portal.User) ([]RoleBinding, error) {

	var roles []RoleBinding

	if requestor.Admin {
		roles = append(roles, RoleBinding{UserScope, CreatorRole})
	}

	// user scope
	if o.Username == requestor.Username {
		roles = append(roles, RoleBinding{Scope: UserScope, Role: CreatorRole})
	}

	// project scope
	for name := range o.Projects {
		p := storage.NewProject(name)
		err := p.Read()
		if err != nil {
			log.Warnf("Error reading project: %s", name)
			continue
		}

		rs, err := ProjectObject{Project: p.Project}.UserRoles(requestor)
		if err != nil {
			log.Warnf("Error getting user roles for project: %s", name)
			continue
		}
		roles = append(roles, rs...)

		if p.Project.Organization != "" {
			o := storage.NewOrganization(p.Project.Organization)
			err := o.Read()
			if err != nil {
				log.Warnf("Error getting user roles for project: %s", name)
			} else {
				rs, err := OrganizationObject{Organization: o.Organization}.UserRoles(requestor)
				if err != nil {
					roles = append(roles, rs...)
				}
			}
		}
	}

	// experiment scope
	for _, e := range o.Experiments {

		tkns := strings.Split(e, ".")
		if len(tkns) != 2 {
			return nil, fmt.Errorf(
				"corrupted experiment reference '%s', expected <name>.<project>",
				e)
		}
		exp := storage.NewExperiment(tkns[0], tkns[1])
		err := exp.Read()
		if err != nil {
			return nil, fmt.Errorf("bad experiment format in user experiment list: %s", exp)
		}

		rs, err := ExperimentObject{Experiment: exp.Experiment}.UserRoles(requestor)
		if err != nil {
			log.Warnf("Unable to get user roles for experiment %s: %s", exp.Name, err.Error())
		} else {
			roles = append(roles, rs...)
		}
	}

	// Organization scope.
	for name := range o.Organizations {
		org := storage.NewOrganization(name)
		err := org.Read()
		if err != nil {
			return nil, fmt.Errorf("inconsitency in user orgs: org %s does not exist, yet user is a member", name)
		}
		oo, err := OrganizationObject{Organization: org.Organization}.UserRoles(requestor)
		if err != nil {
			roles = append(roles, oo...)
		}
	}

	return roles, nil
}

// Policy functions ===========================================================

// ReadUsers is an Identity level action
func ReadUsers(caller *identity.IdentityTraits) error {

	reqs := GetPolicy().Identity.Read
	return Authorize(caller, reqs, IdentityObject{Identity: caller})
}

func ReadUser(caller *identity.IdentityTraits, user string) error {

	u := storage.NewUser(user)
	err := u.Read()
	if err != nil {
		return err
	}

	pol := GetPolicy().User[Mode(u.AccessMode)].Read

	return Authorize(caller, pol, UserObject{u.User})
}

func UpdateUser(caller *identity.IdentityTraits, user string) error {

	u := storage.NewUser(user)
	err := u.Read()
	if err != nil {
		return err
	}

	if u.GetVersion() == 0 {
		return me.NotFoundError("user", user)
	}

	pol := GetPolicy().User[Mode(u.AccessMode)].Update

	return Authorize(caller, pol, UserObject{u.User})
}

func DeleteUser(caller *identity.IdentityTraits, user string) error {

	u := storage.NewUser(user)
	err := u.Read()
	if err != nil {
		return err
	}

	pol := GetPolicy().User[Mode(u.AccessMode)].Delete

	return Authorize(caller, pol, UserObject{u.User})
}
