package commission

import (
	"encoding/binary"
	"fmt"
	"math"
	"net"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/xir/v0.3/go"
)

const (
	blockPoolSize = 64
)

func NewBlockPool(_begin, _end string) (*portal.BlockPool, error) {

	beginIP := net.ParseIP(_begin)
	if beginIP == nil {
		return nil, fmt.Errorf("invalid pool begin")
	}
	begin := binary.BigEndian.Uint32(beginIP.To4())

	endIP := net.ParseIP(_end)
	if endIP == nil {
		return nil, fmt.Errorf("invalid pool end")
	}
	end := binary.BigEndian.Uint32(endIP.To4())

	bp := new(portal.BlockPool)

	buf := []byte{0, 0, 0, 0}

	for i := begin; i <= end; i += blockPoolSize {
		binary.BigEndian.PutUint32(buf, i)
		bp.Blocks = append(bp.Blocks,
			net.IP(buf).String(),
		)
	}

	return bp, nil

}

func AssignInfranetAddrs(tbx *xir.Topology, bp *portal.BlockPool) error {

	// TODO: precondition, blocks & addrs not already assigned

	var switches []*xir.Device
	for _, dev := range tbx.Devices {
		r := dev.Data.(*xir.Resource)
		if r == nil {
			continue
		}
		if r.HasRole(xir.Role_InfraSwitch, xir.Role_Gateway) {
			switches = append(switches, dev)
		}
	}

	for _, sw := range switches {

		r := sw.Resource()

		var services, tenants []*xir.Device
		infrapods := make(map[*xir.Interface]*xir.Device)

		for _, x := range sw.Neighbors() {

			if x.Remote.Device.Resource().HasRole(xir.Role_TbNode) {
				tenants = append(tenants, x.Remote.Device)
			}

			if x.Remote.Device.Resource().HasRole(
				xir.Role_SledHost,
				xir.Role_RallyHost,
				xir.Role_BorderGateway,
			) {
				services = append(services, x.Remote.Device)
			}

			if x.Remote.Device.Resource().HasRole(xir.Role_InfrapodServer) {
				//infrapods = append(infrapods, x.Remote.Device)
				infrapods[x.Local] = x.Remote.Device
			}

		}

		log.Infof("found %d infrapods", len(infrapods))
		log.Infof("%s has %d service neighbors", sw.Id(), len(services))

		serviceBlocks, err := TakeNextN(bp, int(math.Ceil(float64(len(services))/float64(blockPoolSize))))
		if err != nil {
			return err
		}

		infrapodBlocks := make(map[string]*xir.AddressList)
		for ifx, dev := range infrapods {
			blks, err := TakeNextN(bp, calculateInfrapodBlocks(dev))
			for i, x := range blks {
				y, err := incip(x)
				if err != nil {
					return err
				}
				blks[i] = y
			}
			if err != nil {
				return err
			}
			infrapodBlocks[ifx.PortName()] = &xir.AddressList{List: blks}
		}

		tenantBlocks, err := TakeNextN(bp, int(math.Ceil(float64(len(tenants))/float64(blockPoolSize))))
		if err != nil {
			return err
		}

		r.LeafConfig = &xir.LeafConfig{
			ServiceAddressBlocks:  &xir.AddressList{List: serviceBlocks},
			TenantAddressBlocks:   &xir.AddressList{List: tenantBlocks},
			InfrapodAddressBlocks: infrapodBlocks,
		}

		r.InfranetAddr = map[string]*xir.AddressList{
			"service":  new(xir.AddressList),
			"tenant":   new(xir.AddressList),
			"infrapod": new(xir.AddressList),
		}

		for _, x := range serviceBlocks {
			gw, err := incip(x)
			if err != nil {
				return err
			}
			log.Debugf("adding %s to service blocks for %s", gw, r.Id)
			r.InfranetAddr["service"].List = append(
				r.InfranetAddr["service"].List, gw)

		}

		for _, x := range tenantBlocks {
			gw, err := incip(x)
			if err != nil {
				return err
			}
			r.InfranetAddr["tenant"].List = append(
				r.InfranetAddr["tenant"].List, gw)

		}

		for _, blks := range infrapodBlocks {
			for _, x := range blks.List {
				gw, err := incip(x)
				if err != nil {
					return err
				}
				r.InfranetAddr["infrapod"].List = append(
					r.InfranetAddr["infrapod"].List, gw)
			}
		}

		serviceBi := NewBlockIterator(serviceBlocks)
		tenantBi := NewBlockIterator(tenantBlocks)

		for _, x := range sw.Neighbors() {

			nr := x.Remote.Device.Resource()

			if nr.HasRole(xir.Role_TbNode) {
				addInfranetAddr(nr, "tenant", tenantBi.Next().String())
			}

			if nr.HasRole(
				xir.Role_SledHost,
				xir.Role_RallyHost,
				xir.Role_MinIOHost,
			) {
				addInfranetAddr(nr, "service", serviceBi.Next().String())
			}
			if nr.HasRole(xir.Role_BorderGateway) {
				addInfranetAddr(nr, "wan", serviceBi.Next().String())
			}

			/* infrapod neighbor addresses dynamically assigned */

		}

	}

	return nil

}

func addInfranetAddr(r *xir.Resource, vrf, addr string) {

	if r.InfranetAddr == nil {
		r.InfranetAddr = map[string]*xir.AddressList{
			vrf: {List: []string{addr}},
		}
	} else {
		if r.InfranetAddr[vrf] == nil {
			r.InfranetAddr[vrf] = &xir.AddressList{
				List: []string{addr},
			}
		} else {
			found := false
			for _, x := range r.InfranetAddr[vrf].List {
				if x == addr {
					found = true
					break
				}
			}
			if !found {
				r.InfranetAddr[vrf].List = append(r.InfranetAddr[vrf].List, addr)
			}
		}
	}

}

type BlockIterator struct {
	Blocks  []net.IP
	counter uint32
}

func NewBlockIterator(blocks []string) *BlockIterator {

	bi := &BlockIterator{
		counter: 2, //never hand out the .0 or .1 addr
	}

	for _, x := range blocks {
		bi.Blocks = append(bi.Blocks, net.ParseIP(x))
	}

	return bi

}

func incip(addr string) (string, error) {

	ip := net.ParseIP(addr)
	if ip == nil {
		return "", fmt.Errorf("invalid ip %s", addr)
	}

	x := binary.BigEndian.Uint32(ip.To4()) + 1
	buf := []byte{0, 0, 0, 0}
	binary.BigEndian.PutUint32(buf, x)
	return net.IP(buf).String(), nil

}

func (bi *BlockIterator) Next() net.IP {

	i := bi.counter / blockPoolSize
	off := bi.counter % blockPoolSize
	bi.counter++

	start := binary.BigEndian.Uint32(bi.Blocks[i].To4())
	buf := []byte{0, 0, 0, 0}
	binary.BigEndian.PutUint32(buf, start+off)
	return net.IP(buf)

}

func RequiredBlocks(r *xir.Resource) int {

	return 1

}

func TakeNextN(bp *portal.BlockPool, n int) ([]string, error) {

	var blocks []string

	for i := 0; i < n; i++ {
		blk, err := TakeNext(bp)
		if err != nil {
			return nil, err
		}
		blocks = append(blocks, blk)
	}

	return blocks, nil

}

func TakeNext(bp *portal.BlockPool) (string, error) {

	if len(bp.Blocks) == 0 {
		return "", fmt.Errorf("depleted. Pool: %+v", bp)
	}

	blk := bp.Blocks[0]

	if len(bp.Blocks) > 1 {
		bp.Blocks = bp.Blocks[1:]
	} else {
		bp.Blocks = nil
	}

	return blk, nil

}

func calculateInfrapodBlocks(server *xir.Device) int {

	n := 0

	mem := xir.AsGB(server.Resource().Mem())

	log.Infof("%s mem: %d gb", server.Id(), mem)

	if mem == 0 {
		log.Warnf(
			"infrapod server %s has no memory specified, only assigning 1 address block",
			server.Id(),
		)
		n += 1
	} else {
		n += int(math.Ceil(float64(mem) / 2 / blockPoolSize))
	}

	return n

}
