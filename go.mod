module gitlab.com/mergetb/portal/services

go 1.16

require (
	cloud.google.com/go/kms v1.7.0 // indirect
	github.com/go-git/go-git/v5 v5.4.2
	github.com/go-openapi/swag v0.19.5
	github.com/gofrs/flock v0.8.0
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/gogo/googleapis v1.4.0 // indirect
	github.com/gogo/status v1.1.0
	github.com/golang/protobuf v1.5.2
	github.com/goware/modvendor v0.0.0-20180827175348-07a105bce74a
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.1-0.20190118093823-f849b5445de4
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.8.0
	github.com/maruel/natural v1.1.0
	github.com/minio/minio-go/v7 v7.0.9-0.20210210235136-83423dddb072
	github.com/mwitkow/go-proto-validators v0.3.2
	github.com/ory/kratos-client-go v0.11.1
	github.com/panjf2000/ants/v2 v2.8.2
	github.com/prometheus/client_golang v1.11.1
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.8.1
	github.com/smallstep/certificates v0.15.6
	github.com/spf13/cobra v1.6.0
	github.com/stretchr/testify v1.8.2
	github.com/teris-io/shortid v0.0.0-20171029131806-771a37caa5cf
	github.com/vishvananda/netns v0.0.0-20211101163701-50045581ed74
	gitlab.com/mergetb/api v1.1.15
	gitlab.com/mergetb/mcc v0.1.17
	gitlab.com/mergetb/tech/reconcile v1.1.6
	gitlab.com/mergetb/tech/rtnl v0.1.14-0.20231026193731-96b074226119
	gitlab.com/mergetb/tech/shared/storage v1.0.0
	gitlab.com/mergetb/xir v0.3.17
	go.etcd.io/etcd/client/v3 v3.5.4
	golang.org/x/crypto v0.8.0
	golang.org/x/sys v0.13.0
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	golang.zx2c4.com/wireguard/wgctrl v0.0.0-20230429144221-925a1e7659e6
	google.golang.org/genproto v0.0.0-20221201164419-0e50fba7f41c
	google.golang.org/grpc v1.51.0-dev
	google.golang.org/protobuf v1.28.1
	gopkg.in/square/go-jose.v2 v2.5.1
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/api v0.19.2
	k8s.io/apimachinery v0.24.2
	k8s.io/client-go v10.0.0+incompatible
)

replace (
	google.golang.org/grpc/cmd/protoc-gen-go-grpc => google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.0.0
	k8s.io/client-go => k8s.io/client-go v0.19.2
)
