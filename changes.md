# XIR V0.3 Changes

This is a massive patch set. It brings in XIR V0.3 which changes the underlying
data model to have more structure and alters code within the core portal
services to take advantage of the new structure.

There are also some workflow updates based on experience.

## Allocation Data
Allocations are now keyed on the resoures and cables that map to a set of
allocations against that cable/resource
```
:resource_id -> []resource_allocation
:cable_id    -> []cable_allocation
```

## Realization Data
The ralization data structure has been simplified and designed to work in
conjunction with allocation tables.

```
/rz/:mzid -> <rlz>
    xhash 
    :node -> {node_id, resource_id, facility_id}
    :link -> []{cable_id, cable_type, facility_id, lanes}
    :phyo -> {simulator_id, facility_id}
```

## Connecting realization and allocation
```
getAllocation(mzid, node, resource):
    a := alloc.Fetch(resource)
    return a.Find(mzid, node) 
```

## Model analysis over pending realizations.

The idea behind the realize/accept flow was to allow users to look at a
realization relative to the model they coded up, and have the opportunity to say
'no thanks' and start over if the allocation was not successful. This process
has not really been used by users. Rather that offer a chance to backtrack, a
better path forward should be created.

In this patch set we alter this process. We now have the model service, which
includes static analysis tools for experiments. We also have (the almost
completely unused) discovery service - that can be used to discover the
available resources that can underpin a model. So the workflow change looks like
this.

### Old

- develop model
- realize
- reject
- revise model
- realize 
- accept

### New

- develop model
- analyze model
- discover resources for model
- realize

## Factor project info out of node-level MzFragments

We've been pushing user data into every node in an experiment. However, in all
cases (currently) this data is exactly th esame for every node in the experiment
(user data for all users in the project). So now this information is factored
out into the higher level MzRequest message that aggregates MzFragment objects.
