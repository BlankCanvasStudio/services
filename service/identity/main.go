package main

import (
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

type ids struct{}

func init() {
	internal.InitLogging()
}

var (
	Version string
)

func main() {

	log.Infof("portal version: %s", Version)

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatal("storage client init: %v", err)
	}

	grpcServer := grpc.NewServer()
	portal.RegisterIdentityServer(grpcServer, &ids{})

	l, err := net.Listen("tcp", "0.0.0.0:6000")
	if err != nil {
		log.Fatalf("failed to listen: %#v", err)
	}

	log.Info("Listening on tcp://0.0.0.0:6000")
	grpcServer.Serve(l)

}
