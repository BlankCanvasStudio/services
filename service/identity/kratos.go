package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	ory "github.com/ory/kratos-client-go"
	ident "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

type LoginResultAPI struct {
	SessionToken string      `json:"session_token"`
	Session      ory.Session `json:"session"`
}

func readIdentities() ([]*portal.IdentityInfo, error) {

	perPage := int64(1024)
	page := int64(1)

	cli := ident.KratosAdminCli()
	// ugly
	ids, resp, err := cli.IdentityApi.ListIdentities(context.Background()).PerPage(perPage).Page(page).Execute()
	if err != nil {
		log.Errorf("Read Identities: %v", err)
		if resp.StatusCode != 200 {
			return nil, KratosRespError(resp)
		}
		return nil, fmt.Errorf("Kratos error: %v", err)
	}

	pids := []*portal.IdentityInfo{}

	for _, id := range ids {

		userId := &ident.Identity{}
		userId.Traits.Traits = make(map[string]string)
		err := userId.FromOryID(&id)
		if err != nil {
			log.Errorf("Error conveting Ory Identity Traits")
			continue
		}

		log.Debugf("reading user for id %s", userId.Traits.Username)

		// Read admin status from User data.
		admin := false
		user := storage.NewUser(userId.Traits.Username)
		err = user.Read()
		if err == nil {
			admin = user.Admin
		}

		pids = append(pids, &portal.IdentityInfo{
			Email:    userId.Traits.Email,
			Username: userId.Traits.Username,
			Admin:    admin,
			Traits:   userId.Traits.Traits,
		})
	}

	return pids, nil
}

func newIdentity(uid, email, pw string, traits map[string]string) (string, error) {

	// TODO remove admin. Replace with well-known OPs username.

	cli := ident.KratosAdminCli()

	// // This worked one time in my VTE. Now on any new portal, Kratos gives me a redirect from
	// // http://kratos-admin to the https://launch..../auth endpoint. So fuck this.
	// flow, r, err := cli.FrontendApi.CreateNativeRegistrationFlow(context.Background()).Execute()
	// if err != nil {
	// 	log.Errorf("Error FrontendApi.CreateNativeRegistrationFlow: %v\n", err)
	// 	log.Errorf("Full HTTP response: %v\n", r)
	// 	return "", fmt.Errorf("init registration flow: %s", err)
	// }

	// body := ory.UpdateRegistrationFlowBody{
	// 	UpdateRegistrationFlowWithPasswordMethod: &ory.UpdateRegistrationFlowWithPasswordMethod{
	// 		Method:   "password",
	// 		Password: pw,
	// 		Traits: map[string]interface{}{ // traits
	// 			"email":    email,
	// 			"username": uid,
	// 		},
	// 	},
	// }

	// id, resp, err := cli.FrontendApi.UpdateRegistrationFlow(
	// 	context.Background(),
	// ).Flow(
	// 	flow.Id,
	// ).UpdateRegistrationFlowBody(
	// 	body,
	// ).Execute()

	// if err != nil {
	// 	log.Errorf("UpdateRegistrationFlow execute: %v", err)

	// 	if resp.StatusCode != 200 {
	// 		return KratosRespError(resp), err
	// 	}

	// 	return "", fmt.Errorf("kratos err: %s", err)
	// }

	ts := map[string]interface{}{
		"email":    email,
		"username": uid,
	}

	for k, v := range traits {
		ts[k] = v
	}

	id, resp, err := cli.IdentityApi.CreateIdentity(context.Background()).CreateIdentityBody(
		ory.CreateIdentityBody{
			SchemaId: "default",
			Traits:   ts,
			Credentials: &ory.IdentityWithCredentials{
				Password: &ory.IdentityWithCredentialsPassword{
					Config: &ory.IdentityWithCredentialsPasswordConfig{
						Password: &pw,
					},
				},
			},
		},
	).Execute()

	if err != nil {
		log.Errorf("CreateIdentity error: %+v", err)
		msg := KratosRespError(resp)
		log.Errorf("error message: %s", msg)
		return msg.Error(), err
	}

	// response from `AdminCreateIdentity`: Identity
	log.Infof("Created new identity with ID: %v", id)

	return "", nil
}

func delIdentity(uid string) error {

	perPage := int64(1024)
	page := int64(1)

	cli := ident.KratosAdminCli()
	// ugly
	ids, resp, err := cli.IdentityApi.ListIdentities(context.Background()).PerPage(perPage).Page(page).Execute()
	if err != nil {
		e := KratosRespError(resp)
		log.Errorf("error message: %s", e)
		return e
	}

	var idid string

	for _, id := range ids {
		traits, ok := id.Traits.(map[string]interface{})
		if ok {
			if _, ok := traits["username"]; ok {
				username, ok := traits["username"].(string)
				if ok {
					if username == uid {
						idid = id.Id
						break
					}
				}
			}
		}
	}

	if idid != "" {
		resp, err := cli.IdentityApi.DeleteIdentity(context.Background(), idid).Execute()
		if err != nil {
			log.Errorf("DeleteIdentity error: %+v", err)
			e := KratosRespError(resp)
			log.Errorf("error message: %s", e)
			return e
		}
	} else {
		return fmt.Errorf("ID %s not found", uid)
	}

	log.Infof("Deleted identity ID: %s (%s)", uid, idid)

	return nil
}

func loginapi(uid, pw string) (*LoginResultAPI, error) {

	cli := ident.KratosPublicCli()

	// init the flow
	flow, r, err := cli.FrontendApi.CreateNativeLoginFlow(context.Background()).Execute()
	if err != nil {
		log.Errorf("Error FrontendApi.CreateNativeLoginFlow: %v\n", err)
		log.Errorf("Full HTTP response: %v\n", r)
		return nil, fmt.Errorf("init login flow: %s", err)
	}

	b := ory.UpdateLoginFlowBody{
		UpdateLoginFlowWithPasswordMethod: &ory.UpdateLoginFlowWithPasswordMethod{
			Method:     "password",
			Password:   pw,
			Identifier: uid,
		},
	}

	login, resp, err := cli.FrontendApi.UpdateLoginFlow(context.Background()).Flow(flow.Id).UpdateLoginFlowBody(b).Execute()

	if err != nil {
		log.Errorf("Login Flow: %v", err)
		if resp.StatusCode != 200 {
			return nil, KratosRespError(resp)
		}
		return nil, fmt.Errorf("Kratos error: %v", err)
	}

	log.Debugf("got session token: %+v", login.SessionToken)

	return &LoginResultAPI{SessionToken: *login.SessionToken, Session: login.Session}, nil
}

func KratosRespError(resp *http.Response) error {

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("no error body to parse")
	}

	log.Debugf("body: %s", string(body))

	// We may get a few formats for errors here. So we try a few different
	// ones.

	// ory should give us this but they do not because their code is all over the place
	type OryUiErrMessage struct {
		Id   int    `json:"id"`
		Type string `json:"type"`
		Text string `json:"text"`
	}
	type OryUiError struct {
		Ui struct {
			Messages []OryUiErrMessage
		}
	}

	e := OryUiError{}
	err = json.Unmarshal(body, &e)
	if err != nil {
		log.Errorf("kratos response unmarhsal error: %s", err)
		return fmt.Errorf("unparsable error")
	}

	log.Debugf("parsed error: %+v", e)

	if len(e.Ui.Messages) != 0 {
		m := e.Ui.Messages[0]

		// These codes are here for some reason:
		// https://www.ory.sh/docs/kratos/concepts/ui-user-interface#machine-readable-format
		if m.Id == 4000006 { // bad Credentials
			return status.Errorf(codes.PermissionDenied, m.Text)
		}

		return status.Errorf(codes.Unknown, m.Text)
	}

	// else this is a generic error hopefully.
	var eeg ory.NullableErrorGeneric
	err = eeg.UnmarshalJSON(body)

	if eeg.IsSet() {
		kerr := eeg.Get().Error
		log.Debugf("kerr: %+v", kerr)
		log.Debugf("kerr message: %s", kerr.GetMessage())
		if m, ok := kerr.GetMessageOk(); ok {
			return status.Errorf(codes.Unknown, *m)
		}
	}

	return status.Errorf(codes.Unknown, "Unknown auth error")

}
