package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

var Version = ""

func main() {

	log.Infof("portal version: %s", Version)

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("etcd client init: %v", err)
	}

	err = storage.InitPortalMinIOClient()
	if err != nil {
		log.Fatalf("minio client init: %v", err)
	}

	runReconciler()
}
