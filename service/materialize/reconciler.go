package main

import (
	"regexp"

	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	irec "gitlab.com/mergetb/portal/services/pkg/reconcile"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	recName  = "mtz"
	nameExp  = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	mtzRqKey = regexp.MustCompile(
		"^/materializations/" + nameExp + "/" + nameExp + "/" + nameExp + "$",
	)
)

type MtzTask struct {
	Rid string
	Eid string
	Pid string
}

func runReconciler() {

	log.Infof("Starting " + recName + " reconciler")

	t := &reconcile.TaskManager{
		Manager:                    recName,
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: irec.HeartbeatGracePeriodFromConfig(),
		Tasks: []*reconcile.Task{{
			Prefix:  "/materializations/",
			Name:    "mtzs",
			Desc:    "Manage materializations",
			Actions: &MtzTask{},
		}},
	}

	t.Run()
}

func (mt *MtzTask) Parse(key string) bool {

	parts := mtzRqKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		mt.Pid = parts[0][1]
		mt.Eid = parts[0][2]
		mt.Rid = parts[0][3]
		return true
	}

	return false
}

func (mt *MtzTask) Create(value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Info("creating materialization")

	rq := new(portal.MaterializeRequest)
	err := proto.Unmarshal(value, rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	err = handleMaterialize(rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (mt *MtzTask) Update(prev, value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Infof("got update for mztask: doing nothing")
	return nil
}

func (mt *MtzTask) Ensure(prev, value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Infof("got ensure for mztask: doing nothing")
	return reconcile.TaskMessageUndefined()
}

func (mt *MtzTask) Delete(value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	rq := new(portal.MaterializeRequest) // We reuse the materialize request
	err := proto.Unmarshal(value, rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	err = handleDematerialize(rq)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (mt *MtzTask) Expired(t *reconcile.Task) {

	log.Warnf("task expired: %+v", t)
}
