Git Integration
===============

Git integration works as follows

- Whenever a new experiment is created a git repo must be associated with that
  experiment.
  - If the user does not provide a repo, a repo is created within the portal for
    the experiment.
  - The user may delegate repository read permission from GitHub or GitLab to
    the portal, in which case they may provde a URL for an external repository
    hosted on either of these services (TODO).

The Portal-internal git server may be interacted with as follows. We are only
supporting git over HTTPS with token authentication at the current time.

```
git clone https://<token>@git.mergetb.net/<project>/<experiment>
```

The <token> to be provided is the output of

```
mergetb whoami -t
```

In this README we assume that the address that the Portal git is available at is
git.mergetb.net, but the same applies for other portals that could be located at
git.example.com.

When a push event happens in an experiment repo, the Portal will automatically
compile the sources. If the compilation is successful, the resulting XIR and
assets identified in the experiment description will be placed in the
experiments MinIO bucket. For the Portal-internal git server, notification is
accomplished through git hooks. For GitHub or GitLab, notification is
accomplished through web hooks.

## Usage tips

### Password Prompts

If an experiment is public, the source can be read by annonymous users via

```
git clone https://@git.mergetb.net/<project>/<experiment>
```

however, when the user tries to `push`, they will be greeted with a password
prompt. If the user is a member of the project, they can use their `mergetb
whoami -t` token to gain push access. If their git installation is set up for
credential caching, this token will be remembered for future push operations. If
the token expires, a 401 will be passed back by the server, and the git client
will re-present the login prompt.

### Client Credential Caching

Some git configurations can cache credentials. If invalid credentials are cached
this could be confusing. Removing credentials from the cache can be dones as
follows

```
echo "url=https://git.mergetb.net" | git credential reject
```

## Development tips

### Self signed certs

```
GIT_SSL_NO_VERIFY=true git clone -v https://<addr>:<port>/<proj>/<experiment>
```

This will be needed for all git operations unless you set the local or global
config with this option.
