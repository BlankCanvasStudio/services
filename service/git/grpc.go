package main

import (
	"context"
	"errors"
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	me "gitlab.com/mergetb/portal/services/pkg/merror"
)

type srv struct{}

func runGrpcServer() {

	log.Infof("Starting model GRPC service")

	grpcServer := grpc.NewServer()
	portal.RegisterModelServer(grpcServer, &srv{})

	l, err := net.Listen("tcp", "0.0.0.0:6000")
	if err != nil {
		log.Fatalf("failed to listen: %#v", err)
	}

	log.Info("Listening on tcp://0.0.0.0:6000")
	grpcServer.Serve(l)
}

func (m *srv) Compile(
	ctx context.Context, rq *portal.CompileRequest,
) (*portal.CompileResponse, error) {

	resp := &portal.CompileResponse{
		Success: false,
	}

	net, err := compileModel(rq.Model)
	if err != nil {
		// This is slightly odd as the error itself already contains all
		// the error information. The end user may not parse the error so
		// we send it back in the response as well. A smart client would
		// just check the error though.

		var merr *me.MergeError
		if errors.As(err, &merr) {
			resp.Errors = append(resp.Errors, merr.Evidence)
		}

		resp.Success = false
		return resp, me.ToGRPCError(err)
	}

	buf, err := net.ToB64Buf()

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	resp.Success = true
	resp.Network = string(buf)

	return resp, nil
}
