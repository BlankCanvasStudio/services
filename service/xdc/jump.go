package main

import (
	"context"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
)

func initJump(svc string) error {

	// we note the jump machine in etcd so UserTask can find the ssh-jumps' xdcd instances.
	sj := storage.NewSSHJump(svc)
	err := sj.Read()
	if err != nil {
		log.Error(err)
		return err
	}

	_, err = sj.Update()
	if err != nil {
		log.Error(err)
		return err
	}

	users, err := storage.ListUsers()
	if err != nil {
		log.Error(err)
		return err
	}

	ps := []*portal.User{}
	for _, u := range users {
		ps = append(ps, u.User)
	}

	err = withXdcdClient(svc, func(c xdcd.XdcdClient) error {
		_, err := c.AddUsers(
			context.TODO(),
			&xdcd.AddUsersRequest{Users: ps},
		)
		return err
	})
	if err != nil {
		log.Error(err)
		return err
	}

	err = withXdcdClient(svc, func(c xdcd.XdcdClient) error {
		_, err := c.InitHost(
			context.TODO(),
			&xdcd.InitHostRequest{},
		)
		return err
	})
	if err != nil {
		log.Error(err)
		return err
	}

	return nil
}
