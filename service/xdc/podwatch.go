package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"

	"gitlab.com/mergetb/portal/services/pkg/podwatch"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	istor "gitlab.com/mergetb/tech/shared/storage/etcd"
)

var (
	podInitManager string = "podinit"
	podInitTask    string = "host"
	podInitVer     int64  = 1

	workerCount = 16
)

func init() {
	podwatch.InitPodWatch()
}

func runPodWatch() {

	pw := &podwatch.PodWatcher{
		WorkerPoolSize: workerCount,
		OnPodAdd:       handleAdd,
		OnPodUpdate:    handleUpdate,
		OnPodDelete:    handleDelete,
		Namespace:      xdcNs,
	}

	pw.Watch()
}

func handleAdd(pod *v1.Pod) {

	// Init the new xdc pod
	r := podReconciler(pod.Name)

	err := initPod(pod)
	if err != nil {
		log.Errorf("error initing pad: %v", err)
		r.PutError(reconcile.TaskMessageError(err))
	}
	log.Infof("pod init complete for %s", pod.Name)

	r.PutComplete()
}

func handleUpdate(prevPod, newPod *v1.Pod) {

	log.Tracef("XDC update pod callback fired for %s", newPod.Name)

	// Init the new xdc pod
	r := podReconciler(newPod.Name)

	// We have to read the raw status,
	// since there's no actual task key that corresponds to this status.
	err := r.Status.ReadRawStatus(storage.EtcdClient)
	if err != nil {
		log.Warnf("read raw status: %+v", err)
		return
	}

	if !r.Status.IsReconciled(nil, podInitVer) {

		log.Infof("[%s] pod init not complete. trying again", newPod.Name)

		err := initPod(newPod)
		if err != nil {
			log.Info("podinit failed")
			r.PutError(reconcile.TaskMessageError(err))
			return
		}
		r.PutComplete()
	}

	// log.Infof("Updateid XDC pod: %s/%s", opod.Name, npod.Name)
}

func initPod(pod *v1.Pod) error {

	// distinguish between xdc and jumps, both are watched here.
	t, err := podwatch.PodType(pod)
	if err != nil {
		return fmt.Errorf("podtype: %v", err)
	}

	if t == podwatch.XDC {
		name, proj, err := podwatch.PodName(pod)
		if err != nil {
			return fmt.Errorf("podname: %v", err)
		}

		return initXDC(name, proj)

	} else if t == podwatch.JUMP {

		return initJump(pod.Labels["svc"])
	}

	return fmt.Errorf("unknown pod instance")
}

func handleDelete(pod *v1.Pod) {

	log.Infof("XDC delete pod callback fired")

	r := podReconciler(pod.Name)
	r.DelStatus()
	log.Infof("Deleted XDC pod: %s", pod.Name)
}

func podReconciler(name string) *reconcile.Reconciler {
	r := reconcile.NewReconcilerRaw(storage.EtcdClient, name, podInitManager, podInitTask, "", 0)
	r.TaskValue = istor.NewGenericEtcdKey(r.Status.TaskKey, nil)
	r.TaskValue.SetVersion(podInitVer)

	return r
}
