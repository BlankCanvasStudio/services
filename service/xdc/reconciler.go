package main

import (
	log "github.com/sirupsen/logrus"
	irec "gitlab.com/mergetb/portal/services/pkg/reconcile"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

var recName string = "xdc"

func runReconciler() {

	log.Infof("starting xdc reconciler")

	// Things to watch:
	// * xdc events -
	//   * new/del xdc instances - spawn xdcs
	// * project events
	//   * watch membership and mount/unmount user home dirs?
	// * user events -
	//   * add/delete users to/from jump boxes

	t := &reconcile.TaskManager{
		Manager:                    recName,
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: irec.HeartbeatGracePeriodFromConfig(),
		Tasks: []*reconcile.Task{{
			Prefix:  "/xdcs/",
			Name:    "Xdcs",
			Desc:    "Manage XDCs",
			Actions: &XdcTask{},
		}, {
			Prefix:  "/users/", // we want to add users to all jump boxes.
			Name:    "User",
			Desc:    "Manage User access to XDCs",
			Actions: &UserTask{},
		}},
	}

	t.Run()
}
