package main

import (
	"context"
	"fmt"
	"sort"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/merror"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

// Handlers ===================================================================

func (x *xps) ListIdentities(
	ctx context.Context, rq *portal.ListIdentityRequest,
) (*portal.ListIdentityResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadIdentities(caller)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	conn, cli, err := identityClient()
	if err != nil {
		return nil, fmt.Errorf("identity service connection error: %v", err)
	}
	defer conn.Close()

	resp, err := cli.ListIdentities(ctx, rq)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	sort.Slice(resp.Identities, func(i, j int) bool {
		return resp.Identities[i].Username < resp.Identities[j].Username
	})

	return resp, nil
}

func (x *xps) GetIdentity(
	ctx context.Context, rq *portal.GetIdentityRequest,
) (*portal.GetIdentityResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadIdentities(caller)
	if err != nil {
		return nil, merror.ToGRPCError(err)
	}

	conn, cli, err := identityClient()
	if err != nil {
		return nil, fmt.Errorf("identity service connection error: %v", err)
	}
	defer conn.Close()

	return cli.GetIdentity(ctx, rq)
}

func (x *xps) Register(
	ctx context.Context, rq *portal.RegisterRequest,
) (*portal.RegisterResponse, error) {

	conn, cli, err := identityClient()
	if err != nil {
		return nil, fmt.Errorf("identity service connection error: %v", err)
	}
	defer conn.Close()

	return cli.Register(ctx, rq)

}

func (x *xps) Unregister(
	ctx context.Context, rq *portal.UnregisterRequest,
) (*portal.UnregisterResponse, error) {

	caller, err := id.GRPCCallerAllowInactive(ctx)
	if err != nil {
		return nil, err
	}

	// always allow self to unregister; this addresses situations where a non-activated account
	// wants to remove itself; e.g., if they forgot a password or, forgot to enroll in an
	// organization
	if caller.Username != rq.Username {
		err = policy.UnregisterUser(caller, rq.Username)
		if err != nil {
			return nil, merror.ToGRPCError(err)
		}
	}

	conn, cli, err := identityClient()
	if err != nil {
		return nil, fmt.Errorf("identity service connection error: %v", err)
	}
	defer conn.Close()

	resp, err := cli.Unregister(ctx, rq)

	if err == nil {

		us := storage.NewUserStatus(rq.Username)
		err = us.Read()
		if err != nil {
			return nil, status.Error(codes.Internal, "user status read")
		}

		_, derr := us.Delete()
		if derr != nil {
			return nil, status.Error(codes.Internal, "user status delete")
		}
	}

	return resp, err

}

func (x *xps) Login(
	ctx context.Context, rq *portal.LoginRequest,
) (*portal.LoginResponse, error) {

	log.Infof("Login %s", rq.Username)

	conn, cli, err := identityClient()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "identity service connection error: %s", err.Error())
	}
	defer conn.Close()

	resp, err := cli.Login(ctx, rq)
	if err != nil {
		return nil, err
	}

	// If we've successfully logged in, then update the user status

	us := storage.NewUserStatus(rq.Username)
	err = us.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "user status read")
	}
	us.Loggedin = true

	_, err = us.Update()
	if err != nil {
		log.Errorf("update: %+v", err)
		return nil, status.Error(codes.Internal, "user status")
	}

	return resp, nil
}

func (x *xps) Logout(
	ctx context.Context, rq *portal.LogoutRequest,
) (*portal.LogoutResponse, error) {

	log.Infof("Logout %s", rq.Username)

	conn, cli, err := identityClient()
	if err != nil {
		return nil, fmt.Errorf("identity service connection error: %v", err)
	}
	defer conn.Close()

	resp, err := cli.Logout(ctx, rq)

	// If we've successfully logged out, then update the user status
	if err == nil {

		us := storage.NewUserStatus(rq.Username)
		err := us.Read()
		if err != nil {
			return nil, status.Error(codes.Internal, "user status read")
		}
		us.Loggedin = false

		_, err = us.Delete()
		if err != nil {
			log.Errorf("update: %+v", err)
			return nil, status.Error(codes.Internal, "user status")
		}
	}

	return resp, nil
}

// Helpers ====================================================================

func identityClient() (*grpc.ClientConn, portal.IdentityClient, error) {

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", "identity", 6000),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, nil, fmt.Errorf("grpc dial: %v", err)
	}

	return conn, portal.NewIdentityClient(conn), nil

}
