package main

import (
	"context"
	"flag"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"google.golang.org/grpc/reflection"
)

type xps struct{}

func init() {
	internal.InitLogging()

	// Set bootstrap/fallback admin user account.
	value, ok := os.LookupEnv("PORTAL_OPS")
	if ok {
		log.Infof("setting portal OPS to %s", value)
		id.Admin = value
	}
}

var (
	api_endpoint       string
	grpc_endpoint      string
	sshjump_endpoint   string
	allowed_origins    string
	heartbeat_interval int

	logrusLogger *log.Logger
)

var Version = ""

func main() {

	log.Infof("portal version: %s", Version)

	flag.StringVar(&api_endpoint, "api", "", "The REST API endpoint")
	flag.StringVar(&grpc_endpoint, "grpc", "", "The GRPC endpoint")
	flag.StringVar(&sshjump_endpoint, "ssh", "jump.mergetb.net:2022", "The SSH Jump Host endpoint")
	flag.IntVar(&heartbeat_interval, "heartbeat", 60, "Seconds between etcd heartbeats sent")
	flag.StringVar(&allowed_origins, "origins", "https://*.mergetb.net", "Comma-delimited allowed origins for the REST API")

	flag.Parse()

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage client init: %v", err)
	}

	err = storage.InitPortalMinIOClient()
	if err != nil {
		log.Fatalf("minio client init: %v", err)
	}

	if api_endpoint == "" {
		log.Fatalf("must specify the api endpoint via the -api argument")
	}

	// https://gitlab.com/mergetb/portal/services/-/issues/291
	_, ssh_port, err := net.SplitHostPort(sshjump_endpoint)
	if ssh_port != "2022" {
		log.Fatalf("at this time, ssh only supports port 2022")
	}

	log.Infof("api endpoint: %s", api_endpoint)
	log.Infof("grpc endpoint: %s", grpc_endpoint)
	log.Infof("SSH jump host endpoint: %s", sshjump_endpoint)
	log.Infof("etcd heartbeat interval: %d seconds", heartbeat_interval)

	pc, err := storage.GetPortalConfig()
	if err != nil {
		log.Fatalf("get portal config: %s", err)
	}

	pc.Config.APIEndpoint = api_endpoint
	pc.Config.GRPCEndpoint = grpc_endpoint
	pc.Config.SSHJumpEndpoint = sshjump_endpoint
	pc.Config.Reconcile.HeartbeatIntervalSec = int64(heartbeat_interval)

	_, err = pc.Update()
	if err != nil {
		log.Fatalf("update portal config: %s", err)
	}

	go runHeartbeater()
	go runGrpc()
	runGrpcGw()
}

func runGrpcGw() {

	log.Infof("Running experimenter gateway on %s", api_endpoint)

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// TODO don't hardcode
	lh := "localhost:6000"

	mux := runtime.NewServeMux()

	opts := []grpc.DialOption{
		grpc.WithInsecure(),
	}

	opts = append(opts, internal.GRPCMaxMessage)
	err := portal.RegisterWorkspaceHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterRealizeHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterMaterializeHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterCommissionHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterAllocHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterIdentityHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterCredHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterXDCHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterWireguardHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}
	err = portal.RegisterModelHandlerFromEndpoint(ctx, mux, lh, opts)
	if err != nil {
		log.Fatal(err)
	}

	aos := strings.Split(allowed_origins, ",")
	log.Infof("allowed_origins: %v", aos)

	handler := cors.New(cors.Options{
		AllowedOrigins: aos,
		AllowedMethods: []string{
			http.MethodHead,
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
		},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
	}).Handler(mux)

	handler = checkAuthKratos(handler)

	// This is the port that the container listens on, not the external one.
	log.Fatal(http.ListenAndServe(":8081", handler))

}

func runGrpc() {

	log.Infof("Starting the Merge Portal Experimentation API Server")

	// logrusLogger = logrus.New()
	// logrusLogger.Out = os.Stdout

	// logrusEntry := log.NewEntry(log.StandardLogger())
	// opts := []grpc_logrus.Option{
	// 	grpc_logrus.WithLevels(grpc_logrus.DefaultClientCodeToLevel),
	// }
	// // Make sure that log statements internal to gRPC library are logged using the logrus Logger as well.
	// grpc_logrus.ReplaceGrpcLogger(logrusEntry)

	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_validator.StreamServerInterceptor(),
				// grpc_ctxtags.StreamServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
				// grpc_logrus.StreamServerInterceptor(logrusEntry, opts...),
			),
		),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				grpc_validator.UnaryServerInterceptor(),
				// grpc_ctxtags.UnaryServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
				// grpc_logrus.UnaryServerInterceptor(logrusEntry, opts...),
			),
		),
		grpc.MaxRecvMsgSize(internal.GRPCMaxMessageSize),
		grpc.MaxSendMsgSize(internal.GRPCMaxMessageSize),
	)

	portal.RegisterWorkspaceServer(grpcServer, &xps{})
	portal.RegisterRealizeServer(grpcServer, &xps{})
	portal.RegisterMaterializeServer(grpcServer, &xps{})
	portal.RegisterCommissionServer(grpcServer, &xps{})
	portal.RegisterAllocServer(grpcServer, &xps{})
	portal.RegisterIdentityServer(grpcServer, &xps{})
	portal.RegisterCredServer(grpcServer, &xps{})
	portal.RegisterXDCServer(grpcServer, &xps{})
	portal.RegisterWireguardServer(grpcServer, &xps{})
	portal.RegisterModelServer(grpcServer, &xps{})

	ep := "0.0.0.0:6000"
	l, err := net.Listen("tcp", ep)
	if err != nil {
		log.Fatalf("failed to listen: %#v", err)
	}

	log.Infof("Listening on tcp://%s", ep)
	reflection.Register(grpcServer)
	grpcServer.Serve(l)
}
