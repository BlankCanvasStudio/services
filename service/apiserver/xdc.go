package main

import (
	"context"
	"fmt"
	"net"
	"sort"
	"strconv"
	"strings"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func (x *xps) ListXDCs(
	ctx context.Context, rq *portal.ListXDCsRequest,
) (*portal.ListXDCsResponse, error) {

	log.Infof("listing XDCs")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	ps := []string{}

	// If project is specified, use that - else read all projects.
	if rq.Project != "" {

		ps = append(ps, rq.Project)

	} else {

		// Get all user projects and read the XDCs in them.
		u := storage.NewUser(caller.Username)
		err = u.Read()
		if err != nil {
			return nil, status.Error(codes.Internal, err.Error())
		}

		for p := range u.Projects {
			ps = append(ps, p)
		}
	}

	xdcs := []*storage.XDC{}
	for _, p := range ps {

		xs, err := storage.ListXDCs(p)
		if err != nil {
			m := fmt.Sprintf("list xdc internal error: %s", err)
			return nil, status.Error(codes.Internal, m)
		}

		xdcs = append(xdcs, xs...)
	}

	resp := &portal.ListXDCsResponse{}
	for _, x := range xdcs {

		parts := strings.Split(x.Image, "/")
		if len(parts) > 0 {
			x.Image = parts[len(parts)-1]
		}

		url := ""
		fqdn := x.Name + "-" + x.Project

		jc := storage.NewJupyterCfg(x.Name + "-" + x.Project)
		err = jc.Read()
		if err != nil {
			log.Error("no jupyter cfg for this xdc for some reason")
		} else {
			if jc.Domain == "" {
				url = "N/A"
				fqdn = "N/A"
			} else {
				url = jc.Url
				fqdn += "." + jc.Domain
			}
		}

		resp.XDCs = append(resp.XDCs, &portal.XDCInfo{
			Name:            x.Name + "." + x.Project,
			Url:             url,
			Fqdn:            fqdn,
			Creator:         x.Creator,
			Memlimit:        x.MemLimit,
			Cpulimit:        x.CpuLimit,
			Image:           x.Image,
			Materialization: x.Materialization,
		})
	}

	sort.Slice(resp.XDCs, func(i, j int) bool {
		return natural.Less(resp.XDCs[i].Name, resp.XDCs[j].Name)
	})

	return resp, nil
}

func (x *xps) CreateXDC(
	ctx context.Context, rq *portal.CreateXDCRequest,
) (*portal.CreateXDCResponse, error) {

	log.Infof("creating XDC")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	// this process spawns a k8s service. The service name needs to be:
	// a DNS-1035 label must consist of lower case alphanumeric
	// characters or '-', start with an alphabetic character, and end
	// with an alphanumeric character (e.g. 'my-name', or 'abc-123',
	// regex used for validation is '[a-z]([-a-z0-9]*[a-z0-9])?')"
	//
	// Previously, we used a regex to enforce valid names. Now the
	// validity of names is enforced in the Merge API.
	err = policy.SpawnXDC(caller, rq)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	xdc := storage.NewXDC(caller.Username, rq.Xdc, rq.Project)

	// Admin mixins. If we passed policy, this is OK.
	if rq.Image != "" {
		xdc.Image = rq.Image
	}
	if rq.Memlimit != 0 {
		xdc.MemLimit = rq.Memlimit // yay mixed-case
	}
	if rq.Cpulimit != 0 {
		xdc.CpuLimit = rq.Cpulimit
	}

	_, err = xdc.Create()
	if err != nil {
		m := fmt.Sprintf("XDC storage create: %s", err)
		return nil, status.Error(codes.Internal, m)
	}

	return &portal.CreateXDCResponse{}, nil
}

func (x *xps) DeleteXDC(
	ctx context.Context, rq *portal.DeleteXDCRequest,
) (*portal.DeleteXDCResponse, error) {

	log.Infof("deleting XDC")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	xdc := storage.NewXDC(caller.Username, rq.Xdc, rq.Project)
	err = xdc.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "XDC storage read")
	}

	if xdc.Ver == 0 {
		return nil, status.Error(codes.NotFound, "xdc does not exist")
	}

	err = policy.DestroyXDC(caller, rq.Project, rq.Xdc)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	_, err = xdc.Delete()
	if err != nil {
		return nil, status.Error(codes.Internal, "XDC storage delete")
	}

	return &portal.DeleteXDCResponse{}, nil
}

func (x *xps) AttachXDC(
	ctx context.Context, rq *portal.AttachXDCRequest,
) (*portal.AttachXDCResponse, error) {

	log.Infof("attaching XDC")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	xdc := storage.NewXDC(caller.Username, rq.Xdc, rq.Project)
	err = xdc.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "XDC storage read")
	}

	if xdc.Ver == 0 {
		return nil, status.Error(codes.NotFound, "xdc does not exist")
	}

	err = policy.AttachXDC(caller, rq.Project, rq.Xdc)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	// confirm the mtz exists.
	req := storage.NewMaterializeRequest(rq.Realization, rq.Experiment, rq.Project)
	err = req.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "mtz storage read")
	}

	if req.Ver == 0 {
		return nil, status.Error(codes.NotFound, "mtz does not exist")
	}

	// Create the xdc attach data to be reconciled.
	c := storage.NewXdcWgClient(rq)
	err = c.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "XDC client storage read")
	}

	if c.GetVersion() != 0 {
		return nil, status.Error(codes.FailedPrecondition, "XDC already attached.")
	}

	_, err = c.Create()
	if err != nil {
		return nil, status.Error(codes.Internal, "XDC client create")
	}

	return &portal.AttachXDCResponse{}, nil

}

func (x *xps) DetachXDC(
	ctx context.Context, rq *portal.DetachXDCRequest,
) (*portal.DetachXDCResponse, error) {

	log.Infof("detaching XDC")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	xdc := storage.NewXDC(caller.Username, rq.Xdc, rq.Project)
	err = xdc.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "XDC storage read")
	}

	if xdc.Ver == 0 {
		return nil, status.Error(codes.NotFound, "xdc does not exist")
	}

	err = policy.DetachXDC(caller, rq.Project, rq.Xdc)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "forbidden")
	}

	c := storage.NewXdcWgClient(&portal.AttachXDCRequest{Project: rq.Project, Xdc: rq.Xdc})
	err = c.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, "XDC client storage read")
	}

	_, err = c.Delete()
	if err != nil {
		return nil, status.Error(codes.Internal, "XDC client delete")
	}

	return &portal.DetachXDCResponse{}, nil
}

func (x *xps) XDCTunnelGateway(
	ctx context.Context, rq *portal.XDCTunnelGatewayRequest,
) (*portal.XDCTunnelGatewayResponse, error) {

	// read the tunnel gateway inforamtion for the given realization
	// and give it to the caller.

	log.Infof("XDC tunnel gw request")
	return nil, status.Error(codes.Unimplemented, "xdc tunnel gateway")
}

func (x *xps) GetXDCJumpHosts(
	ctx context.Context, rq *portal.GetXDCJumpHostsRequest,
) (*portal.GetXDCJumpHostsResponse, error) {

	log.Infof("GetXDCJumpHostsRequest")

	pc, err := storage.GetPortalConfig()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "read portal config: %+v", err)
	}

	// parse the endpoint into fqdn/port
	fqdn, portstr, err := net.SplitHostPort(pc.Config.SSHJumpEndpoint)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "malformed endpoint: %+v", err)
	}

	port, err := strconv.Atoi(portstr)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "malformed endpoint: cannot convert port to string: %+v", err)
	}

	// XXX For now, only a single jump host config for the portal
	return &portal.GetXDCJumpHostsResponse{
		JumpHosts: []*portal.SSHJump{{
			Name: "ssh-jump",
			Port: uint32(port),
			Fqdn: fqdn,
		}},
	}, nil
}
