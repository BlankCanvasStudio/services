package main

import (
	"context"
	"fmt"
	"sort"

	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/connect"
	id "gitlab.com/mergetb/portal/services/pkg/identity"
	"gitlab.com/mergetb/portal/services/pkg/materialize"
	"gitlab.com/mergetb/portal/services/pkg/policy"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func (x *xps) GetMaterializations(
	ctx context.Context, rq *portal.GetMaterializationsRequest,
) (*portal.GetMaterializationsResponse, error) {

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	u := storage.NewUser(caller.Username)
	err = u.Read()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	// Wrap mtz and status together for later sorting by mzid
	type mtzdata struct {
		mtz    *portal.Materialization
		status *reconcile.TaskSummary
		ings   *portal.Ingresses
	}

	mtzMap := make(map[string]mtzdata)
	keys := []string{}

	mzid := func(m *portal.Materialization) string {
		return fmt.Sprintf("%s.%s.%s", m.Rid, m.Eid, m.Pid)
	}

	// read in all experiments from all projects.
	// We then use policy to filter out projects
	// the caller cannot read.
	projects, err := storage.ListProjects()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	for _, proj := range projects {

		if rq.Filter == portal.FilterMode_ByUser {
			// Only show mtz relevant to the user.
			if member, ok := proj.Members[caller.Username]; ok {
				if member.State != portal.Member_Active {
					// is a user but is not active.
					continue
				}
			} else {
				// user not a member
				continue
			}
		}

		err = policy.ReadProject(caller, proj.Name)
		if err != nil {
			log.Infof("Skipping read project. User read denied.")
			continue
		}

		for _, e := range proj.Experiments {

			err = policy.ReadExperiment(caller, proj.Name, e)
			if err != nil {
				log.Infof("skipping read exp. user access denied")
				continue
			}

			exp := storage.NewExperiment(e, proj.Name)
			err := exp.Read()
			if err != nil {
				return nil, status.Error(codes.Internal, err.Error())
			}

			for revid, model := range exp.Models {

				log.Debugf("EXPERIMENT %s REVISION %s has %d realizations", exp.Name, revid, len(model.Realizations))

				for _, rlz := range model.Realizations {

					err = policy.ReadMaterialization(caller, exp.Project, exp.Name, rlz)
					if err != nil {
						log.Infof("skipping read mtz. user access denied")
						continue
					}

					m, err := storage.ReadMaterialization(exp.Project, exp.Name, rlz)
					if err != nil {
						if se, ok := status.FromError(err); ok {
							log.Infof("read mtz err: %v", err)
							if se.Code() == codes.NotFound {
								// this realization has no mtz.
								continue
							}
						}

						return nil, status.Errorf(codes.Internal,
							"read materialization result for %s.%s.%s: %v",
							rlz, exp.Name, exp.Project, err)
					}

					ings, ts, err := GetLiveMaterializationData(m)
					if err != nil {
						return nil, err
					}

					key := mzid(m)
					keys = append(keys, key)
					mtzMap[key] = mtzdata{m, ts, ings}
				}
			}
		}
	}

	resp := new(portal.GetMaterializationsResponse)

	// Now append mtz/status in sorted order after sorting by mzid.
	sort.Sort(natural.StringSlice(keys))
	for _, k := range keys {
		resp.Materializations = append(resp.Materializations, mtzMap[k].mtz)
		resp.Statuses = append(resp.Statuses, mtzMap[k].status)
		resp.Ingresses = append(resp.Ingresses, mtzMap[k].ings)
	}

	return resp, nil
}

func (x *xps) GetMaterialization(
	ctx context.Context, rq *portal.GetMaterializationRequest,
) (*portal.GetMaterializationResponse, error) {

	log.Info("Get Materialization")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = policy.ReadMaterialization(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	mzn, err := storage.ReadMaterialization(rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, err
	}

	ings, ts, err := GetLiveMaterializationData(mzn)
	if err != nil {
		return nil, err
	}

	return &portal.GetMaterializationResponse{
		Materialization: mzn,
		Status:          ts,
		Ingresses:       ings,
	}, nil
}

func (x *xps) GetMaterializationStatus(
	ctx context.Context, rq *portal.GetMaterializationStatusRequest,
) (*portal.GetMaterializationStatusResponse, error) {

	log.Info("Get Materialization")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	err = policy.ReadMaterialization(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	mzn, err := storage.ReadMaterialization(rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, err
	}

	tf, err := GetMaterializationStatus(mzn)
	if err != nil {
		return nil, err
	}

	return &portal.GetMaterializationStatusResponse{
		Status: tf,
	}, nil
}

func (x *xps) Materialize(
	ctx context.Context, rq *portal.MaterializeRequest,
) (*portal.MaterializeResponse, error) {

	log.Info("Materialize")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	rlz := storage.NewRealizeRequest(rq.Realization, rq.Experiment, rq.Project)
	err = rlz.Read()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "rlz read: %s", err)
	}
	if rlz.Ver == 0 {
		return nil, status.Errorf(
			codes.NotFound,
			"Realization %s.%s.%s does not exist", rq.Realization, rq.Experiment, rq.Project,
		)
	}

	err = policy.Materialize(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	r := storage.NewMaterializeRequest(rq.Realization, rq.Experiment, rq.Project)
	err = r.Read()
	if err != nil {
		return nil, status.Errorf(codes.Internal, err.Error())
	}
	if r.Ver != 0 {
		return nil, status.Error(codes.AlreadyExists, "materialization already exists")
	}

	_, err = r.Create()
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &portal.MaterializeResponse{}, nil
}

func (x *xps) Dematerialize(
	ctx context.Context, rq *portal.DematerializeRequest,
) (*portal.DematerializeResponse, error) {

	log.Info("Dematerialize")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	// we re-use the MaterializeRequest as it contains the same fields.
	r := storage.NewMaterializeRequest(rq.Realization, rq.Experiment, rq.Project)
	err = r.Read()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "mtz read: %s", err)
	}
	if r.Ver == 0 {
		return nil, status.Errorf(
			codes.NotFound,
			"Materialization %s.%s.%s does not exist", rq.Realization, rq.Experiment, rq.Project,
		)
	}

	err = policy.Dematerialize(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	_, err = r.Delete()
	if err != nil {
		return nil, err
	}

	return &portal.DematerializeResponse{}, nil
}

func GetMaterializationStatus(mzn *portal.Materialization) (*reconcile.TaskForest, error) {
	mzid := fmt.Sprintf("%s.%s.%s", mzn.Rid, mzn.Eid, mzn.Pid)

	sites := materialize.SiteList(mzn)

	all_tfs := make([]*reconcile.TaskForest, len(sites))

	if len(sites) == 0 {
		return nil, status.Error(codes.FailedPrecondition, "materialization has no facilities")
	}

	for i, site := range sites {
		err := connect.FacilityClient(
			site,
			func(cli facility.FacilityClient) error {
				resp, err := cli.GetMaterializationStatus(
					context.TODO(),
					&facility.GetMaterializationStatusRequest{
						Pid: mzn.Pid,
						Eid: mzn.Eid,
						Rid: mzn.Rid,
					},
				)

				if err != nil {
					all_tfs[i] = &reconcile.TaskForest{
						Goal: reconcile.NewErrorGoal(
							"",
							sites[i],
							fmt.Sprintf("error: %+v", err),
						),
						LastUpdated:   timestamppb.Now(),
						HighestStatus: reconcile.TaskStatus_Error,
					}
				} else {
					all_tfs[i] = resp.Status
				}

				return err
			},
		)

		if err != nil {
			log.Errorf("[%s] facility connection error @ %s: %v", mzid, site, err)
		} else {
			log.Infof("[%s] materialization status requested @ %s", mzid, site)
		}
	}


	// if there's only 1 site, just return it as is
	if len(sites) == 1 {
		return all_tfs[0], nil
	}

	return nil, status.Error(codes.Unimplemented, "materialization has multiple facilities; not supported yet")

	/* TODO: test the following code for materialization status across multiple sites

	// otherwise, there's multiple sites, so we need to consolidate them
	tg := reconcile.NewSuperTask(mzid, mzid, "Materialization status across all sites")
	tg.Subkeys = sites

	var higheststatus reconcile.TaskStatus_StatusType
	var lastupdated time.Time

	var tfs []*reconcile.TaskForest

	for _, tf := range all_tfs {
		if tf.HighestStatus > higheststatus {
			higheststatus = tf.HighestStatus
		}

		if lastupdated.Before(tf.LastUpdated.AsTime()) {
			lastupdated = tf.LastUpdated.AsTime()
		}

		tfs = append(tfs, tf)
	}

	return &reconcile.TaskForest{
		Goal:          tg,
		HighestStatus: higheststatus,
		Subgoals:      tfs,
		LastUpdated:   timestamppb.New(lastupdated),
	}, nil
	*/
}

func GetLiveMaterializationData(mzn *portal.Materialization) (*portal.Ingresses, *reconcile.TaskSummary, error) {
	mzid := fmt.Sprintf("%s.%s.%s", mzn.Rid, mzn.Eid, mzn.Pid)

	sites := materialize.SiteList(mzn)
	all_tss := make([]*reconcile.TaskSummary, len(sites))
	all_ings := make([]*portal.Ingresses, len(sites))

	if len(sites) == 0 {
		err := fmt.Errorf("no sites for materialization %s", mzid)
		return nil, reconcile.NewTaskSummaryError(err), nil
	}

	for i, site := range sites {
		err := connect.FacilityClient(
			site,
			func(cli facility.FacilityClient) error {
				resp, err := cli.GetMaterializationShortStatus(
					context.TODO(),
					&facility.GetMaterializationShortStatusRequest{
						Pid: mzn.Pid,
						Eid: mzn.Eid,
						Rid: mzn.Rid,
					},
				)

				if err != nil {
					all_tss[i] = reconcile.NewTaskSummaryError(err)
				} else {
					all_tss[i] = resp.Status
				}

				return err
			},
		)

		if err != nil {
			log.Errorf("[%s] facility connection error @ %s: %v", mzid, site, err)
		} else {
			log.Infof("[%s] materialization status requested @ %s", mzid, site)
		}

		err = connect.FacilityClient(
			site,
			func(cli facility.FacilityClient) error {

				resp, err := cli.GetMaterializationIngresses(
					context.TODO(),
					&facility.GetMaterializationIngressesRequest{
						Mzid: mzid,
					},
				)

				if err != nil {
					return err
				}

				all_ings[i] = &portal.Ingresses{}
				for _, ing := range resp.Ingresses {

					var ingpath string
					switch ing.Protocol {
					case xir.Protocol_tcp, xir.Protocol_udp:
						ingpath = fmt.Sprintf("%s:%d", ing.Gateway, ing.Externalport)
					case xir.Protocol_http:
						ingpath = fmt.Sprintf(
							"http://%s:80/%s/%s/%d",
							ing.Gateway,
							ing.Mzid,
							ing.Hostname,
							ing.Hostport,
						)
					case xir.Protocol_https:
						ingpath = fmt.Sprintf(
							"https://%s:443/%s/%s/%d",
							ing.Gateway,
							ing.Mzid,
							ing.Hostname,
							ing.Hostport,
						)
					}

					all_ings[i].Ingresses = append(all_ings[i].Ingresses, &portal.Ingress{
						Mzid:        ing.Mzid,
						Hostname:    ing.Hostname,
						Hostport:    ing.Hostport,
						Hostaddr:    ing.Hostaddr,
						Gateway:     ing.Gateway,
						Gatewayport: ing.Externalport,
						Protocol:    ing.Protocol.String(),
						Ingress:     ingpath,
					})
				}

				return nil
			},
		)

		if err != nil {
			log.Errorf("[%s] facility connection error @ %s: %v", mzid, site, err)
		} else {
			log.Infof("[%s] materialization ingresses requested @ %s", mzid, site)
		}
	}

	// if there's only 1 site, just return it as is
	if len(sites) == 1 {
		return all_ings[0], all_tss[0], nil
	}

	return nil, nil, status.Error(codes.Unimplemented, "materialization has multiple facilities; not supported yet")

	/* TODO: test the following code for materialization status across multiple sites

	all_tss[0].Merge(all_tss[1:]...)

	return all_tss[0], nil

	*/
}

func (x *xps) NewIngress(
	ctx context.Context, rq *portal.NewIngressRequest,
) (*portal.NewIngressResponse, error) {

	// read the mtz. find the site, call the site to add the ingress and return.
	log.Info("New Ingress")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	// new ingress is like creating the materialization.
	// ew may want to add a Read/Write stanza to the mtz policy.
	err = policy.Materialize(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	mzn, err := storage.ReadMaterialization(rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, err
	}

	sites := materialize.SiteList(mzn)
	for _, site := range sites {
		log.Debugf("Passing the new ingress call to %s", mzn.Params.InfranetGw)

		err = connect.FacilityClient(
			site,
			func(cli facility.FacilityClient) error {
				_, err := cli.NewIngress(
					context.TODO(),
					&facility.NewIngressRequest{
						Project:     mzn.Pid,
						Experiment:  mzn.Eid,
						Realization: mzn.Rid,
						Host:        rq.Host,
						Port:        rq.Port,
						Protocol:    rq.Protocol,
					},
				)

				return err
			},
		)

		if err != nil {
			return nil, status.Errorf(codes.Unknown, "Error: %v", err)
		}
	}

	return &portal.NewIngressResponse{}, nil
}

func (x *xps) DeleteIngress(
	ctx context.Context, rq *portal.DeleteIngressRequest,
) (*portal.DeleteIngressResponse, error) {

	// read the mtz. find the site, call the site to del the ingress and return.
	log.Info("Deletet Ingress")

	caller, err := id.GRPCCaller(ctx)
	if err != nil {
		return nil, err
	}

	// delete ingress is like deleting the materialization.
	// ew may want to add a Read/Write stanza to the mtz policy.
	err = policy.Dematerialize(caller, rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, err.Error())
	}

	mzn, err := storage.ReadMaterialization(rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return nil, err
	}

	sites := materialize.SiteList(mzn)
	for _, site := range sites {
		// we assume a single site with a single gateway TODO: don't do that.
		// mzn.Params.InfranetGw
		log.Debugf("Passing the delete ingress call to %s", mzn.Params.InfranetGw)

		err = connect.FacilityClient(
			site,
			func(cli facility.FacilityClient) error {
				_, err := cli.DeleteIngress(
					context.TODO(),
					&facility.DeleteIngressRequest{
						Project:     mzn.Pid,
						Experiment:  mzn.Eid,
						Realization: mzn.Rid,
						Host:        rq.Host,
						Port:        rq.Port,
						Protocol:    rq.Protocol,
					},
				)

				return err
			},
		)

		if err != nil {
			return nil, status.Errorf(codes.Unknown, "Error: %v", err)
		}
	}

	return &portal.DeleteIngressResponse{}, nil
}
