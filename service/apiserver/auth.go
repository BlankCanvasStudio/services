package main

import (
	"context"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	ident "gitlab.com/mergetb/portal/services/pkg/identity"
)

type KratosAuthHandler struct {
	next http.Handler
}

func checkAuthKratos(h http.Handler) http.Handler {

	return &KratosAuthHandler{next: h}

}

func needsAuth(r *http.Request) bool {

	// List of URLs that do not require authorization:
	// * POST /register - request an account, cannot be authorized as there is no account
	// * POST /login - cannot be authorized as user is logging in
	// * POST /organization/membership - user requesting membership in in an org before user
	//    account is authorized. It is then up to the organization creators to approve
	//    the request and init/activate the user portal account.
	// * GET /organization - for listing orgs. We have this here an non-activated users
	//	  need to list orgs to request access to them.
	switch r.Method {
	case "GET":
		switch r.URL.Path {
		case "/organization":
			return false
		}
	case "POST":
		switch r.URL.Path {
		case "/register", "/login", "/organization/membership":
			return false
		}
	}

	return true
}

func (h *KratosAuthHandler) check(w http.ResponseWriter, r *http.Request) error {

	if r.Method == http.MethodOptions {
		return nil
	}

	if !needsAuth(r) {
		return nil
	}

	c, err := r.Cookie("ory_kratos_session")
	if err != nil {
		log.Warnf("error getting kratos session cookie: %v", err)
		w.WriteHeader(401) // unauthorized
		return err
	}

	// We've got the cookie, so check the session.
	cli := ident.KratosPublicCli()
	s, resp, err := cli.FrontendApi.ToSession(context.Background()).Cookie(c.String()).Execute()
	if err != nil {
		log.Debugf("Error `Ory.ToSession``: %v\n", err)
		log.Debugf("Full HTTP response: %v\n", resp)
		w.WriteHeader(401) // unauthorized
		return fmt.Errorf("Unable to get user auth session")
	}

	log.Debugf("valid session: %v", s)

	// valid session. apiserver will confirm user against policy.

	return nil
}

func (h *KratosAuthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	err := h.check(w, r)
	if err != nil {
		log.Errorf("auth check failed: %v", err)
		return
	}

	log.Info("auth check ok")

	h.next.ServeHTTP(w, r)

}
