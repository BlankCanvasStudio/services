package main

import (
	log "github.com/sirupsen/logrus"
	irec "gitlab.com/mergetb/portal/services/pkg/reconcile"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

func runReconciler() {

	log.Infof("Starting %s reconciler", recName)

	t := &reconcile.TaskManager{
		Manager:                    recName,
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: irec.HeartbeatGracePeriodFromConfig(),
		Tasks: []*reconcile.Task{{
			Prefix:  "/harbor",
			Name:    "harbors",
			Desc:    "Manage facility harbors",
			Actions: &HarborTask{},
		}},
	}

	t.Run()
}
