package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	facility "gitlab.com/mergetb/api/facility/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/connect"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

var (
	logLevel string

	root = &cobra.Command{
		Use:   "wgctl",
		Short: "Manage Mergetb Portal WG configuration",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			lvl, err := log.ParseLevel(logLevel)
			if err != nil {
				log.Fatalf("bad log level: %s", err)
			}
			log.SetLevel(lvl)
		},
	}

	listCmd = &cobra.Command{
		Use:   "list",
		Short: "list things",
	}

	newCmd = &cobra.Command{
		Use:   "new",
		Short: "create things",
	}

	showCmd = &cobra.Command{
		Use:   "show",
		Short: "show things",
	}

	delCmd = &cobra.Command{
		Use:   "del",
		Short: "delete things",
	}

	listEncsCmd = &cobra.Command{
		Use:   "enclaves",
		Short: "list existing enclaves",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listEncs()
		},
	}

	showEncCmd = &cobra.Command{
		Use:   "enclave [encid]",
		Short: "Show a specific enclave",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			showEnc(args[0])
		},
	}

	gwNewCmd = &cobra.Command{
		Use:   "gw",
		Short: "create gateway things",
	}

	gwDelCmd = &cobra.Command{
		Use:   "gw",
		Short: "delete gateway things",
	}

	newGwIfCmd = &cobra.Command{
		Use:   "if mzid gwendpoint ifaddr",
		Short: "Create a new gateway interface for the given materialization at the given endpoint",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			newGwIf(args[0], args[1], args[2])
		},
	}

	newGwPeerCmd = &cobra.Command{
		Use:   "peer mzid gwendpoint key allowed_ips",
		Short: "Add a peer to a gw if. allowed_ips is comma separated, but a single string.",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			newGwPeer(args[0], args[1], args[2], args[3])
		},
	}

	delGwIfCmd = &cobra.Command{
		Use:   "if mzid gwep",
		Short: "Delete a wg gateway interface",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			delGwIf(args[0], args[1])
		},
	}

	delGwPeerCmd = &cobra.Command{
		Use:   "peer mzid gwendpoint key",
		Short: "Delete a peer from a gateway interface",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			delGwPeer(args[0], args[1], args[2])
		},
	}

	delXdcCmd = &cobra.Command{
		Use:   "xdc",
		Short: "delete xdc things",
	}

	attachCmd = &cobra.Command{
		Use:   "if xid rlz.eid.pid",
		Short: "Create an interface on the given xdc to the given enclave",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			attachXdc(args[0], args[1])
		},
	}

	detachCmd = &cobra.Command{
		Use:   "if xid.eid",
		Short: "Delete the interface on the given xdc",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			detachXdc(args[0])
		},
	}

	newXdcCmd = &cobra.Command{
		Use:   "xdc",
		Short: "create xdc things",
	}

	newEncCmd = &cobra.Command{
		Use:   "enclave rlz.eid.pid",
		Short: "create an enclave",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			createEnc(args[0])
		},
	}

	delEntireEncCmd = &cobra.Command{
		Use:   "enclave rlz.eid.pid",
		Short: "delete an entire enclave",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			deleteEnc(args[0])
		},
	}

	encDelCmd = &cobra.Command{
		Use:   "enc",
		Short: "delete enclave things",
	}

	delEncIfCmd = &cobra.Command{
		Use:   "if rlz.eid.pid key",
		Short: "Delete a peer interface from an enclave",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			delEncPeerIf(args[0], args[1])
		},
	}
)

func init() {

	root.PersistentFlags().StringVar(
		&logLevel, "loglevel", "info", "Level to log at. One of "+strings.Join(logLevels(), ", "),
	)

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("etcd client init: %v", err)
	}

	err = storage.InitPortalMinIOClient()
	if err != nil {
		log.Fatalf("minio client init: %v", err)
	}
}

func main() {

	cobra.EnablePrefixMatching = true

	listCmd.AddCommand(listEncsCmd)
	showCmd.AddCommand(showEncCmd)

	newXdcCmd.AddCommand(attachCmd)
	delXdcCmd.AddCommand(detachCmd)

	gwNewCmd.AddCommand(newGwIfCmd)
	gwNewCmd.AddCommand(newGwPeerCmd)

	gwDelCmd.AddCommand(delGwIfCmd)
	gwDelCmd.AddCommand(delGwPeerCmd)

	encDelCmd.AddCommand(delEncIfCmd)

	newCmd.AddCommand(gwNewCmd)
	newCmd.AddCommand(newXdcCmd)
	newCmd.AddCommand(newEncCmd)

	delCmd.AddCommand(gwDelCmd)
	delCmd.AddCommand(delXdcCmd)
	delCmd.AddCommand(encDelCmd)
	delCmd.AddCommand(delEntireEncCmd)

	root.AddCommand(listCmd)
	root.AddCommand(showCmd)
	root.AddCommand(newCmd)
	root.AddCommand(delCmd)

	root.Execute()
}

func delGwPeer(eid, gwep, key string) {

	err := connect.FacilityWGClient(
		gwep,
		func(cli facility.WireguardClient) error {
			_, err := cli.DelWgPeer(
				context.TODO(),
				&facility.DelWgPeerRequest{
					Enclaveid: eid,
					Config: &portal.WgIfConfig{
						Key: key,
					},
				},
			)
			return err
		},
	)

	if err != nil {
		fmt.Printf("error: %s\n", err)
	}
}

func newGwPeer(eid, gwep, key, ips string) {

	allowed_ips := strings.Split(ips, ",")

	err := connect.FacilityWGClient(
		gwep,
		func(cli facility.WireguardClient) error {
			_, err := cli.AddWgPeers(
				context.TODO(),
				&facility.AddWgPeersRequest{
					Enclaveid: eid,
					Configs: []*portal.WgIfConfig{{
						Key:        key,
						Allowedips: allowed_ips,
					}},
				},
			)
			return err
		},
	)

	if err != nil {
		fmt.Printf("error: %s\n", err)
	}
}

func delGwIf(eid, gwep string) {

	err := connect.FacilityWGClient(
		gwep,
		func(cli facility.WireguardClient) error {
			_, err := cli.DeleteWgInterface(
				context.TODO(),
				&facility.DeleteWgInterfaceRequest{
					Enclaveid: eid,
				},
			)
			return err
		},
	)

	if err != nil {
		fmt.Printf("error: %s\n", err)
	}
}

func newGwIf(eid, gwep, addr string) {

	err := connect.FacilityWGClient(gwep, func(cli facility.WireguardClient) error {

		_, err := cli.CreateWgInterface(
			context.TODO(),
			&facility.CreateWgInterfaceRequest{
				Enclaveid:  eid,
				Accessaddr: addr,
			},
		)
		return err
	})

	if err != nil {
		fmt.Printf("error: %s\n", err)
	}
}

func listEncs() {

	encs, err := storage.ListWgEnclaves()
	if err != nil {
		log.Errorf("error: %s", err)
		return
	}

	for _, e := range encs {
		fmt.Println(e.Enclaveid)
	}
}

func showEnc(id string) {

	e := storage.NewWgEnclave(id)
	err := e.Read()
	if err != nil {
		fmt.Printf("err: %s", err)
		return
	}

	if e.GetVersion() == 0 {
		log.Fatalf("no such enclave")
	}

	printCfgs := func(cfgs map[string]*portal.WgIfConfig) {
		for _, c := range cfgs {
			fmt.Printf("\tEndpoint: %s\n", c.Endpoint)
			fmt.Printf("\tKey: %s\n", c.Key)
			fmt.Printf("\tAllowed IPs: %s\n", strings.Join(c.Allowedips, ", "))
			fmt.Printf("\t---------------------------\n")
		}
	}

	fmt.Printf("Enclave: %s\n", e.Enclaveid)
	fmt.Printf("Gateways: \n")
	printCfgs(e.Gateways)
	fmt.Printf("Clients: \n")
	printCfgs(e.Clients)
}

func createEnc(id string) {

	enc := storage.NewWgEnclave(id)
	err := enc.Read()
	if err != nil {
		log.Fatalf("new enc error: %s\n", err)
	}

	if enc.Ver > 0 {
		log.Fatalf("enclave exists: %s", id)
	}

	_, err = enc.Create()
	if err != nil {
		log.Fatalf("wg enc create error: %s\n", err)
	}
}

func deleteEnc(id string) {

	enc := storage.NewWgEnclave(id)
	err := enc.Read()
	if err != nil {
		log.Fatalf("new enc error: %s\n", err)
	}

	if enc.Ver == 0 {
		log.Fatalf("enclave does not exist: %s", id)
	}

	_, err = enc.Delete()
	if err != nil {
		log.Fatalf("wg enc delete error: %s\n", err)
	}
}

func attachXdc(xid, rid string) {

	tkns := strings.Split(rid, ".")
	if len(tkns) != 3 {
		log.Fatalf("rid must be of the form rlz.exp.project")
	}

	err := XdcSvc(func(cli portal.XDCClient) error {

		_, err := cli.AttachXDC(
			context.TODO(),
			&portal.AttachXDCRequest{
				Xdc:         xid,
				Realization: tkns[0],
				Experiment:  tkns[1],
				Project:     tkns[2],
			},
		)

		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func detachXdc(xid string) {

	err := XdcSvc(func(cli portal.XDCClient) error {

		_, err := cli.DetachXDC(
			context.TODO(),
			&portal.DetachXDCRequest{
				Xdc: xid,
			},
		)

		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func delEncPeerIf(enc, key string) {

	err := WgSvc(func(cli portal.WireguardClient) error {

		_, err := cli.DelWgIfConfig(
			context.TODO(),
			&portal.DelWgIfConfigRequest{
				Enclaveid: enc,
				Key:       key,
			},
		)

		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func XdcSvc(f func(portal.XDCClient) error) error {

	conn, err := grpcConn("xdc:6000")
	if err != nil {
		return fmt.Errorf("error connecting to xdc svc")
	}
	defer conn.Close()

	cli := portal.NewXDCClient(conn)

	return f(cli)
}

func WgSvc(f func(portal.WireguardClient) error) error {

	conn, err := grpcConn("apiserver:6000")
	if err != nil {
		return fmt.Errorf("error connecting to apiserver")
	}
	defer conn.Close()

	cli := portal.NewWireguardClient(conn)

	return f(cli)
}

func grpcConn(ep string) (*grpc.ClientConn, error) {

	creds := credentials.NewTLS(&tls.Config{
		//XXX XXX XXX
		InsecureSkipVerify: true,
	})

	conn, err := grpc.Dial(ep, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.WithFields(log.Fields{
			"ep":  ep,
			"err": err,
		}).Error("bad connect")
		return nil, fmt.Errorf("error connecting to grpc server")
	}

	return conn, nil
}

func logLevels() []string {

	r := []string{}
	for _, l := range log.AllLevels {
		r = append(r, l.String())
	}
	return r
}
