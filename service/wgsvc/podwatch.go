package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/podwatch"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	v1 "k8s.io/api/core/v1"
)

var (
	attachedLabel = "attached"
	workerCount   = 4
)

func runPodWatch() {

	podwatch.InitPodWatch()

	pw := &podwatch.PodWatcher{
		OnPodAdd:       onPodAdd,
		OnPodUpdate:    onPodUpdate,
		OnPodDelete:    onPodDelete,
		WorkerPoolSize: workerCount,
		Namespace:      xdcNs,
	}

	pw.Watch()
}

func onPodAdd(pod *v1.Pod) {
	handlePod(pod)
}

func onPodDelete(pod *v1.Pod) {
	// NOOP
}

func onPodUpdate(prevPod, newPod *v1.Pod) {

	handlePod(newPod)
}

func handlePod(pod *v1.Pod) {

	// TODO: may want to create a reconciler here and log the
	// status. See the xdc podwatch handler for an example.

	t, err := podwatch.PodType(pod)
	if err != nil {
		log.Errorf("podtype: %v", err)
		return
	}

	if t != podwatch.XDC {
		log.Debugf("Ignoring new non-XDC (ssh-jump) pod")
		return
	}

	name, proj, err := podwatch.PodName(pod)
	if err != nil {
		log.Errorf("podname: %v", err)
		return
	}

	// Is the XDC already attached?
	xdc := storage.NewXDC("", name, proj)
	err = xdc.Read()
	if err != nil {
		log.Errorf("xdc read: %v", err)
		return
	}

	if xdc.Materialization == "" {
		log.Debug("Ignoring new XDC as it was not previously attached")
		return
	}

	// XDC is attached although the pod may not be. So reattach it.
	mtzLabel, ok := pod.Labels["attached"]
	if ok && mtzLabel == xdc.Materialization {
		log.Debugf("XDC pod %s.%s is already attached according to pod label", name, proj)
		return
	}

	log.Infof("Reattaching XDC %s-%s to %s", xdc.Name, xdc.Project, xdc.Materialization)
	// log.Infof("[reattach] - pod status: %v", pod.Status)

	encId := xdc.Materialization

	//err = doAttach(xdc.Name, xdc.Project, encId, true) // attach with existing enclave data.
	err = doAttach(xdc.Name, xdc.Project, encId) // attach with existing enclave data.
	if err != nil {
		log.Errorf("attach error: %v", err)
		return
	}

	// Finally add a label so we know we're attached. This is not really great, but it should work.
	err = podwatch.AddLabel(pod, attachedLabel, xdc.Materialization)
	if err != nil {
		log.Errorf("labling XDC as attached: %s", err)
		return
	}
}
