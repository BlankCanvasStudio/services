package main

import (
	log "github.com/sirupsen/logrus"
	irec "gitlab.com/mergetb/portal/services/pkg/reconcile"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

var recName = "wgsvc"

func runReconciler() {

	log.Infof("Starting " + recName + " reconciler")

	t := &reconcile.TaskManager{
		Manager:                    recName,
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: irec.HeartbeatGracePeriodFromConfig(),
		Tasks: []*reconcile.Task{{
			Prefix:  "/wgclient/xdc/",
			Name:    "XdcWgClient",
			Desc:    "Manage wireguard xdc connections",
			Actions: &XdcTask{},
		}, {
			Prefix:  "/wgif/",
			Name:    "WgIf",
			Desc:    "Manage wireguard interfaces",
			Actions: &WgIfTask{},
		}, {
			Prefix:  "/materializations/",
			Name:    "WgEnclave",
			Desc:    "Manage wg enclave data",
			Actions: &MtzTask{},
		}},
	}

	t.Run()
}
