package main

import (
	"context"
	"fmt"
	"io"
	"os/exec"
	"time"

	log "github.com/sirupsen/logrus"
	mcc "gitlab.com/mergetb/mcc/pkg"
	me "gitlab.com/mergetb/portal/services/pkg/merror"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func runPyModel(src string) ([]byte, error) {

	// Do not let the command take "too long" and steal resources.
	// Slight protection against DoS attack. 15 seconds is probably not enough
	// to protect against a determined attacker. C'est la vie.
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	findTopologyPy := "/model/bin/py2xir.py"
	cmd := exec.CommandContext(ctx, "python3", findTopologyPy)
	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Fatalf("faied to get python stdin pipe: %v", err)
	}

	go func() {
		defer stdin.Close()
		io.WriteString(stdin, src)
	}()

	out, err := cmd.Output()
	if err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			e := me.MxCompileError(string(exiterr.Stderr))
			return nil, me.Log(e)
		}
		return nil, me.Log(me.UncategorizedError("cmd error", err))
	}

	return out, nil
}

func compileModel(src string) (*xir.Network, error) {

	buf, err := runPyModel(src)
	if err != nil {
		return nil, err
	}

	// 2.25 run checkers and reticulators over the xir
	net, err := xir.NetworkFromB64String(string(buf))
	if err != nil {
		return nil, me.ModelReticulationError(err)
	}

	for _, c := range mcc.Checkers() {

		log.Infof("Running checker %s", c.Name())

		ok, diags, err := c.Check(net)
		if err != nil {
			return nil, fmt.Errorf("checker %s failed: %v", c.Name(), err)
		}
		if !ok {
			return nil, fmt.Errorf("Failed check %s: %s. Diagnostics: %v", c.Name(), err, diags)
		}
	}

	// 2.35 run reticulators over the network
	for _, r := range mcc.Reticulators() {

		log.Infof("Running reticulator %s", r.Name())

		ok, err := r.Reticulate(net)
		if err != nil {
			return nil, me.ModelReticulationError(err)
		}

		if !ok {
			return nil, fmt.Errorf("Reticulation %s failed: %+v", r.Name(), err)
		}
	}

	log.Info("compilation successful")

	return net, nil

}
