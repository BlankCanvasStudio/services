package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"strings"

	log "github.com/sirupsen/logrus"
)

var (
	xdcSysDir string = "/etc/xdc"
)

func ClearTunnelData() error {

	log.Infof("Clearing tunnel data if it exists.")

	// If there's an existing cached resolv.conf use it
	if _, err := os.Stat("/etc/xdc/resolv"); err == nil {

		src, err := os.Open("/etc/xdc/resolv")
		if err != nil {
			return fmt.Errorf("open: %s", err)
		}
		defer src.Close()

		dst, err := os.Create("/etc/resolv.conf")
		if err != nil {
			return fmt.Errorf("create: %s", err)
		}
		defer dst.Close()

		_, err = io.Copy(dst, src)
		if err != nil {
			return fmt.Errorf("copy: %s", err)
		}
	} else {
		return fmt.Errorf("Unable to restore /etc/resolv.conf as there is not one cached.")
	}

	log.Infof("Restored /etc/resolv.conf")

	files := []string{"rid", "eid", "pid", "resolv"}
	for _, f := range files {
		err := os.Remove("/etc/xdc/" + f)
		if err != nil {
			log.Warnf("Remove %s: %s", f, err)
		} else {
			log.Infof("Removed /etc/xdc/" + f)
		}
	}

	return nil
}

func ConfigureTunnel(ns, rid string) error {

	tkns := strings.Split(rid, ".")

	if len(tkns) != 3 {
		return fmt.Errorf("bad realization id format: %s. Should be rid.eid.pid.", rid)
	}

	return updateResolve(ns, rid)
}

func updateResolve(ns, rid string) error {

	// We want to put our rid.eid.pid name at the start of "search"
	// and our DNS addr as the first nameserver. LIke this:
	// search demo.rohus.losangeles xdc.svc.cluster.local svc.cluster.local cluster.local
	// nameserver 172.30.0.1
	// nameserver 10.77.0.2

	// There is probably a smarter way to do this. And it will probably break once the XDC
	// base OS changes.

	// Save existing.
	orig, err := ioutil.ReadFile("/etc/resolv.conf")
	if err != nil {
		return err
	}

	// Do not overwrite existing as that it likely the actual original file.
	if _, err := os.Stat(path.Join(xdcSysDir, "resolv")); errors.Is(err, os.ErrNotExist) {
		err = writeFile(string(orig), "resolv")
		if err != nil {
			return err
		}
	}

	out := []string{}

	buf, err := os.Open("/etc/resolv.conf")
	if err != nil {
		return err
	}

	searchFound := false
	searchUpdated := false

	scnr := bufio.NewScanner(buf)
	for scnr.Scan() {
		line := scnr.Text()

		if strings.HasPrefix(line, "search") {
			tkns := strings.Split(line, " ")

			// if not there, add it.
			if !strings.Contains(line, rid) {
				l := fmt.Sprintf("search infra.%s %s", rid, strings.Join(tkns[1:], " "))
				out = append(out, l)
				out = append(out, "nameserver "+ns)
				searchUpdated = true
				continue
			}

			// rid is already there
			searchFound = true
		}

		out = append(out, line)
	}

	err = scnr.Err()
	if err != nil {
		buf.Close()
		return err
	}

	buf.Close()

	if searchFound {
		log.Infof("Found existing 'search' line in resolv.conf. Not writing new file'")
		return nil
	}

	if !searchUpdated {
		log.Warnf("Did not find 'search' line in resolv.conf. Not writing new file.")
		return fmt.Errorf("Missing 'search' line in resolv.conf. Cannot continue")
	}

	data := strings.Join(out, "\n")
	err = ioutil.WriteFile("/etc/resolv.conf", []byte(data), 0666)
	if err != nil {
		return err
	}

	return nil
}

func writeFile(data, filename string) error {

	err := os.MkdirAll(xdcSysDir, 0700)
	if err != nil {
		return fmt.Errorf("failed to mk %s dir: %w", xdcSysDir, err)
	}

	err = ioutil.WriteFile(
		fmt.Sprintf("%s/%s", xdcSysDir, filename),
		[]byte(data),
		0664,
	)
	if err != nil {
		return fmt.Errorf("failed to write data to %s/%s: %w", xdcSysDir, filename, err)
	}

	return nil
}
