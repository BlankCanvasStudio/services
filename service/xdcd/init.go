package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"path"
	"strings"

	"io/ioutil"

	log "github.com/sirupsen/logrus"
)

func InitHost() error {

	// todo:
	// setup sshd_config and start
	// password-less sudo
	// possibly setup /etc/resolv.conf to be read/write?
	// setup mrg server setting.

	log.Info("init'ing the host")

	err := configSshd()
	if err != nil {
		return err
	}

	err = configSudo()
	if err != nil {
		return err
	}

	err = configUserCli("root")
	if err != nil {
		return err
	}

	err = configGlobalCli()
	if err != nil {
		return err
	}

	return nil
}

func configSudo() error {

	// A better idea is to make a merge group and have all users in that. Alas.

	p := path.Join("/", "etc", "sudoers.d")

	if _, err := os.Stat(p); os.IsNotExist(err) {
		return handleError(fmt.Errorf("[configSudo] no sudoers.d directory found"))
	}

	conf := "%sudo  ALL=(ALL)       NOPASSWD: ALL"

	p = path.Join(p, "10-merge") // file cannot have . in it or end in ~

	err := ioutil.WriteFile(p, []byte(conf), 0600)
	if err != nil {
		return handleError(fmt.Errorf("[configSudo] write file %s error: %v", p, err))
	}

	return nil
}

func configSshd() error {

	// # setup ssh certs
	// echo 'TrustedUserCAKeys /etc/step-ca/data/certs/ssh_user_ca_key.pub' >> /etc/ssh/sshd_config
	// echo 'HostKey /etc/ssh/merge/merge_key' >> /etc/ssh/sshd_config
	// echo 'HostCertificate /etc/ssh/merge/merge_key-cert.pub' >> /etc/ssh/sshd_config
	// service ssh reload
	//
	// Having the paths passed to xdcd might be nice....
	//
	// NOTE: this code assumes that the mergefs and cred reconciler have placed the sshd credentials.
	//
	conf := `
#
# setup merge generated ssh certs and keys
#
TrustedUserCAKeys /etc/step-ca/data/certs/ssh_user_ca_key.pub
HostKey /etc/ssh/auth/merge_key
HostCertificate /etc/ssh/auth/merge_key-cert.pub
`
	p := path.Join("/", "etc", "ssh", "sshd_config.d", "10-merge.conf")
	err := ioutil.WriteFile(p, []byte(conf), 0600)
	if err != nil {
		return handleError(fmt.Errorf("[configSshd] write file %s: %v", p, err))
	}

	// restart sshd

	_, err = exec.LookPath("supervisorctl")
	if err == nil {
		err := exec.Command("supervisorctl", "restart", "sshd").Run()
		if err != nil {
			return handleError(fmt.Errorf("[configSshd] restart supervisord: %v", err))
		}
	} else {
		_, err = exec.LookPath("service")
		if err == nil {
			err := exec.Command("service", "ssh", "restart").Run()
			if err != nil {
				return handleError(fmt.Errorf("[configSshd] service restart sshd: %v", err))
			}
		} else {
			// just kill and restart by hand. (Alpine container)
			_ = exec.Command("pkill", "sshd").Run()
			err = exec.Command("/usr/sbin/sshd", "-e").Run()
			if err != nil {
				return handleError(fmt.Errorf("[configSshd] restart sshd: %v", err))
			}
		}
	}

	return nil
}

func configUserCli(user string) error {

	log.Infof("configuring the merge cli for %s", user)

	srv, ok := os.LookupEnv("MERGE_GRPC_SERVER")
	if !ok {
		log.Info("MERGE_GRPC_SERVER not set. nothing to do")
		return nil // not really an error
	}

	mrg, err := exec.LookPath("mrg")
	if err != nil {
		return handleError(fmt.Errorf("[configUserCli] mrg not found in path. unable to configure grcp server: %s", err))
	}

	args := []string{
		user, "-c", mrg + " config set server " + srv,
	}

	log.Infof("running cmd: su %s", strings.Join(args, " "))

	err = exec.Command("su", args...).Run()
	if err != nil {
		return handleError(fmt.Errorf("[configUserCli] Unable to set mrg grpc server: %s", err))
	}

	log.Infof("Set mrg grpc server for %s to %s", user, srv)

	return nil
}

func configGlobalCli() error {

	// This does probably not need to be a fail condition.

	log.Info("configuring the merge cli for all")

	mrg, err := exec.LookPath("mrg")
	if err != nil {
		return handleError(fmt.Errorf("[configGlobalCli] mrg not found in path. unable to configure cli: %s", err))
	}

	cmd := exec.Command(mrg, []string{"completion", "bash"}...)
	if err != nil {
		return handleError(fmt.Errorf("[configGlobalCli] error creating mrg completion command: %s", err))
	}

	var out bytes.Buffer
	cmd.Stdout = &out

	err = cmd.Run()
	if err != nil {
		return handleError(fmt.Errorf("[configGlobalCli] error creating mrg bash completion: %s", err))
	}

	err = ioutil.WriteFile("/etc/bash_completion.d/mrg", out.Bytes(), 0644)
	if err != nil {
		log.Errorf("unable to write mrg bash completion: %s", err)
		// we don't really care if this fails.
	}

	return nil
}
