package main

import (
	"context"
	"fmt"
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	xdcd "gitlab.com/mergetb/api/xdcd/v1/go"
	"google.golang.org/grpc"
)

var (
	srv     string
	Version = "undefined"
)

func init() {
	root.PersistentFlags().StringVarP(
		&srv, "server", "s", "0.0.0.0:6000", "Where the server is listening",
	)
}

var root = &cobra.Command{
	Use:   "xdcdcmd",
	Short: "command line interface to xdcd api",
}

func main() {

	ver := &cobra.Command{
		Use:   "version",
		Short: "Show version and exit",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("version: %s\n", Version)
		},
	}
	root.AddCommand(ver)

	clear := &cobra.Command{
		Use:   "clear",
		Short: "Clear existing tunnel meta-data",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			clear()
		},
	}
	root.AddCommand(clear)

	confTunnel := &cobra.Command{
		Use:   "configtunnel rlz.exp.project",
		Short: "Configure the XDC to use the tunnel",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			configTunnel(args[0])
		},
	}
	root.AddCommand(confTunnel)

	adduser := &cobra.Command{
		Use:   "adduser <name> <uid> <gid>",
		Short: "add a user to the system",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			addUser(args[0], args[1], args[2])
		},
	}
	root.AddCommand(adduser)

	deluser := &cobra.Command{
		Use:   "deluser <name>",
		Short: "remove a user from the system",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			delUser(args[0])
		},
	}
	root.AddCommand(deluser)

	init := &cobra.Command{
		Use:   "init",
		Short: "Configure the node for XDC use",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			initHost()
		},
	}
	root.AddCommand(init)

	root.Execute()
}

func withClient(f func(xdcd.XdcdClient) error) error {

	conn, err := grpc.Dial(srv, grpc.WithInsecure())

	if err != nil {
		return fmt.Errorf("dial: %s", err)
	}
	defer conn.Close()

	cli := xdcd.NewXdcdClient(conn)

	return f(cli)
}

func clear() {

	err := withClient(func(c xdcd.XdcdClient) error {

		_, err := c.ClearTunnelData(
			context.Background(),
			&xdcd.ClearTunnelDataRequest{},
		)
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func configTunnel(rid string) {

	err := withClient(func(c xdcd.XdcdClient) error {

		_, err := c.ConfigureTunnel(
			context.Background(),
			&xdcd.ConfigureTunnelRequest{
				Nameserver: "172.30.0.1", // TODO make configurable
				Rid:        rid,
			},
		)
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func initHost() {

	err := withClient(func(c xdcd.XdcdClient) error {

		_, err := c.InitHost(
			context.Background(),
			&xdcd.InitHostRequest{},
		)
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func addUser(user, uid, gid string) {

	u, err := strconv.Atoi(uid)
	if err != nil {
		log.Fatal(err)
	}

	g, err := strconv.Atoi(gid)
	if err != nil {
		log.Fatal(err)
	}

	withClient(func(c xdcd.XdcdClient) error {

		_, err := c.AddUsers(
			context.TODO(),
			&xdcd.AddUsersRequest{
				Users: []*portal.User{{
					Username: user,
					Uid:      uint32(u),
					Gid:      uint32(g),
				}},
			},
		)

		if err != nil {
			log.Fatal(err)
		}

		return nil
	})
}

func delUser(user string) {

	withClient(func(c xdcd.XdcdClient) error {

		_, err := c.DeleteUsers(
			context.TODO(),
			&xdcd.DeleteUsersRequest{
				Users: []*portal.User{{
					Username: user,
				}},
			},
		)

		if err != nil {
			log.Fatal(err)
		}

		return nil
	})
}
