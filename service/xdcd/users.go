package main

import (
	"fmt"
	"math/rand"
	"os/exec"
	"os/user"
	"strings"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func AddUsers(users []*portal.User) error {

	for _, u := range users {

		log.Debugf("Adding user %+v", u)

		err := mkGroup(u)
		if err != nil {
			return err
		}

		err = mkUser(u)
		if err != nil {
			return err
		}
	}

	return nil
}

func DeleteUsers(users []*portal.User) error {

	for _, u := range users {

		_, err := user.Lookup(u.Username)
		if err != nil {
			log.Debugf("User %s does not exist", u.Username)
		} else {
			cmd := exec.Command("userdel", u.Username)

			o, err := cmd.CombinedOutput()
			if err != nil {
				return handleError(fmt.Errorf("%s: %s", "userdel", o))
			}
		}

		_, err = user.LookupGroup(u.Username)
		if err != nil {
			log.Debugf("Group %s does not exist", u.Username)
		} else {
			cmd := exec.Command("groupdel", u.Username)

			o, err := cmd.CombinedOutput()
			if err != nil {
				return handleError(fmt.Errorf("%s: %s", "groupdel", o))
			}
		}
	}

	return nil
}

func mkGroup(u *portal.User) error {

	_, err := user.LookupGroup(u.Username)
	if err == nil {
		log.Debugf("Group %s exists already", u.Username)
		return nil
	}

	var gid string
	gid = fmt.Sprintf("%d", u.Gid)

	var args []string
	var exe string

	if _, err := exec.LookPath("groupadd"); err == nil {

		args = []string{
			"-g", gid,
			"--non-unique",
			u.Username,
		}
		exe = "groupadd"

	} else if _, err := exec.LookPath("addgroup"); err == nil {

		args = []string{
			"-g", gid,
			u.Username,
		}
		exe = "addgroup"

	} else {

		return handleError(fmt.Errorf("unable to add group %s/%s", u.Username, gid))
	}

	o, err := exec.Command(exe, args...).CombinedOutput()
	if err != nil {
		log.Errorf("%s: %+v", exe, err)
		log.Errorf("cmd: %s %s", exe, strings.Join(args, " "))

		// exit val 9 == group exists. This is OK. https://linux.die.net/man/8/groupadd
		if exiterr, ok := err.(*exec.ExitError); ok {
			if exiterr.ExitCode() == 9 {
				log.Infof("group %s already exists", u.Username)
				return nil
			}
		}

		return handleError(fmt.Errorf("error running cmd %s: %s", exe, o))
	}

	log.Infof("added group %s (%d)", u.Username, u.Gid)
	return nil
}

func mkUser(u *portal.User) error {

	_, err := user.Lookup(u.Username)
	if err == nil {
		log.Debugf("User %s exists already", u.Username)
		return nil
	}

	var uid, gid string
	uid = fmt.Sprintf("%d", u.Uid)
	gid = fmt.Sprintf("%d", u.Gid)

	var args []string
	var exe string

	if _, err := exec.LookPath("useradd"); err == nil {

		args = []string{
			"-M", // Do not make HOME dir
			"-d", "/home/" + u.Username,
			"-g", gid, // set default group to user gid
			"-G", gid + ",sudo,users",
			"-s", "/bin/bash",
			"--non-unique", // Allow UID clash. Probably a bad idea.
			"--no-user-group",
			"-u", uid,
			u.Username,
		}
		exe = "useradd"

	} else if _, err := exec.LookPath("adduser"); err == nil {
		// Alpine/Busybox - this assumes any apline machine is an ssh jump box
		// which is a really had assumption to make.
		args = []string{
			"-H",                  // do not make home dir
			"-D",                  // no passwd
			"--disabled-password", // allow non-password logins
			"-s", "/sbin/nologin",
			"-u", uid,
			"-G", u.Username, // can only add one group
			"-h", "/home/" + u.Username,
			u.Username,
		}
		exe = "adduser"

	} else {
		return handleError(fmt.Errorf("unable to add user on this OS"))
	}

	o, err := exec.Command(exe, args...).CombinedOutput()
	if err != nil {
		log.Errorf("%s: %+v", exe, err)
		log.Errorf("cmd: %s %s", exe, strings.Join(args, " "))

		// exit val 9 == existing user. This is OK.
		// https://linux.die.net/man/8/{useradd,adduser}
		if exiterr, ok := err.(*exec.ExitError); ok {
			if exiterr.ExitCode() == 9 {
				log.Infof("group %s already exists", u.Username)
				return nil
			}
		}

		return handleError(fmt.Errorf("error running cmd %s: %s", exe, o))
	}

	// sshd wants the accounts to have passwords (or at least not a "!" in the passwd
	// field in /etc/shadow. So create random passwords for users.
	chpw := fmt.Sprintf("echo %s:%s | chpasswd", u.Username, randomString(16))
	err = exec.Command("bash", "-c", chpw).Run()
	if err != nil {
		return handleError(fmt.Errorf("chpasspw %s: %w", u.Username, err))
	}

	// setup the mrg cli
	configUserCli(u.Username)

	log.Infof("added user %s (%d)", u.Username, u.Uid)
	return nil
}

func randomString(n int) string {

	var letter = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	b := make([]rune, n)
	for i := range b {
		b[i] = letter[rand.Intn(len(letter))]
	}
	return string(b)
}
