package main

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"regexp"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/realize"
	irec "gitlab.com/mergetb/portal/services/pkg/reconcile"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

var (
	Version = ""

	nameExp = "([a-z]+[a-z0-9]{0,62})"

	// /realizations/battlestar/galactica/raptor
	rlzRqKey = regexp.MustCompile(
		"^/realizations/" + nameExp + "/" + nameExp + "/" + nameExp + "$",
	)

	// /realizations/battlestar/galactica/raptor/realize
	rlzKey = regexp.MustCompile(
		"^/realizations/" + nameExp + "/" + nameExp + "/" + nameExp + "/realize$",
	)
)

func main() {

	log.Infof("portal version: %s", Version)

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("etcd client init: %v", err)
	}

	err = storage.InitPortalMinIOClient()
	if err != nil {
		log.Fatalf("minio client init: %v", err)
	}

	t := &reconcile.TaskManager{
		Manager:                    "realize",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: irec.HeartbeatGracePeriodFromConfig(),
		Tasks: []*reconcile.Task{{
			Prefix:  "/realizations/",
			Name:    "rlzs",
			Desc:    "Manage realizations",
			Actions: &RlzTask{},
		}},
	}

	t.Run()

}

type RlzTask struct {
	Key         string
	Project     string
	Experiment  string
	Realization string
}

func (t *RlzTask) Parse(key string) bool {

	tkns := rlzRqKey.FindAllStringSubmatch(key, -1)
	if len(tkns) > 0 {
		t.Key = key
		return true
	}

	return false

}

func (t *RlzTask) Create(value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Infof("[%s] handling realization request", t.Key)

	rzrq := new(portal.RealizeRequest)
	err := proto.Unmarshal(value, rzrq)
	if err != nil {
		return reconcile.TaskMessageErrorf("realize request unmarshal: %v", err)
	}

	// get the compiled XIR from MinIO
	bucket := fmt.Sprintf("xp-%s-%s", rzrq.Project, rzrq.Experiment)

	obj, err := storage.MinIOClient.GetObject(
		context.TODO(),
		bucket,
		rzrq.Revision,
		minio.GetObjectOptions{},
	)
	if err != nil {
		return reconcile.TaskMessageErrorf("minio get revision: %s/%s: %v", bucket, rzrq.Revision, err)
	}

	buf, err := ioutil.ReadAll(obj)
	if err != nil {
		return reconcile.TaskMessageErrorf("read xir: %v", err)
	}

	xpnet, err := xir.NetworkFromB64String(string(buf))
	if err != nil {
		return reconcile.TaskMessageErrorf("parse xir: %v", err)
	}

	if xpnet == nil {
		return reconcile.TaskMessageErrorf("cannot realize %s/%s: nil xpnet", bucket, rzrq.Revision)
	}

	if xpnet.Nodes == nil || len(xpnet.Nodes) == 0 {
		return reconcile.TaskMessageErrorf("cannot realize %s/%s: no nodes in xpnet", bucket, rzrq.Revision)
	}

	return reconcile.CheckErrorToMessage(dorealize(t.Key, version, xpnet, rzrq))

}

func (t *RlzTask) Update(prev, value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Infof("[%s] received update; treating as create", t.Key)
	return t.Create(value, version, r)

}

func (t *RlzTask) Ensure(prev, value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Infof("[%s] received ensure; ignoring", t.Key)
	return reconcile.TaskMessageUndefined()

}

func (t *RlzTask) Delete(value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Infof("[%s] handling relinquish", t.Key)

	rq := new(portal.RealizeRequest)
	err := proto.Unmarshal(value, rq)
	if err != nil {
		return reconcile.TaskMessageErrorf("realize request unmarshal for %s: %v", t.Key, err)
	}

	mzid := fmt.Sprintf("%s.%s.%s", rq.Realization, rq.Experiment, rq.Project)

	// update resource allocation table

	current, err := realize.ReadTable()
	if err != nil {
		return reconcile.TaskMessageErrorf("failed to read allocation table: %v", err)
	}

	updated, err := realize.Dealloc(current, mzid)
	if err != nil {
		return reconcile.TaskMessageErrorf("failed to deallocate resources for %s: %v", mzid, err)
	}

	err = realize.WriteTable(updated)
	if err != nil {
		return reconcile.TaskMessageErrorf("failed to write allocation table update for %s: %v", mzid, err)
	}

	// update vlan allocation table

	bucket := fmt.Sprintf("realize-%s-%s-%s", rq.Project, rq.Experiment, rq.Realization)

	res, err := storage.ReadRealizationResult(rq.Project, rq.Experiment, rq.Realization)
	if err != nil {
		return reconcile.TaskMessageErrorf("[%s] read realization result: %v", mzid, err)
	}

	// we didn't allocate vsets if the diagnostics had an error
	if !res.Diagnostics.Error() {
		err = realize.Relinquish(res.Realization)
		if err != nil {
			return reconcile.TaskMessageErrorf("failed to relinquish rlz: %v", err)
		}
	}

	// clear emulation endpoint count from etcd

	err = storage.DeleteEndpoints(res.Realization)
	if err != nil {
		log.Errorf("remove emulation endpoints: %v", err)
	}

	// remove realization from minio

	err = storage.MinIOClient.RemoveObject(
		context.TODO(),
		bucket,
		"diagnostics",
		minio.RemoveObjectOptions{},
	)
	if err != nil {
		log.Warnf("minio diagnostics remove: %v", err)
	}

	err = storage.MinIOClient.RemoveObject(
		context.TODO(),
		bucket,
		"realization",
		minio.RemoveObjectOptions{},
	)
	if err != nil {
		log.Warnf("minio realization remove: %v", err)
	}

	err = storage.MinIOClient.RemoveBucket(context.TODO(), bucket)
	if err != nil {
		log.Warnf("minio bucket remove: %v", err)
	}

	return nil

}

func (t *RlzTask) Expired(rt *reconcile.Task) {
	log.Infof("expired task: %s", t.Key)
}

func dorealize(key string, version int64, xpnet *xir.Network, rq *portal.RealizeRequest) error {

	a, err := realize.ReadTable()
	if err != nil {
		return fmt.Errorf("read alloc table: %v", err)
	}

	tbx, err := realize.BuildResourceInternet()
	if err != nil {
		return fmt.Errorf("build resource internet: %v", err)
	}

	mzid := fmt.Sprintf("%s.%s.%s", rq.Realization, rq.Experiment, rq.Project)

	resourcePool, err := GetResourcePool(rq.Project)
	if err != nil {
		return fmt.Errorf("No resource pool available for realization: %s", err.Error())
	}

	log.Infof("Using resource pool %s", resourcePool.Name)

	rlzError := false

	rlz, diags, err := realize.Realize(
		tbx.Device("the-internet"),
		tbx,
		xpnet.Lift(),
		a,
		resourcePool,
		realize.RealizeParameters{
			Mzid:    mzid,
			Hash:    rq.Revision,
			Creator: rq.Creator,
		},
	)
	if err != nil {
		rlzError = true
		log.Warnf("realize: %v", err)
	}
	if diags.Error() {
		rlzError = true
		log.Warnf("realize: error in the diagnostics")
	}

	// free realization if anything goes awry
	defer func() {
		if err != nil {
			err2 := realize.Relinquish(rlz)
			if err2 != nil {
				log.Warnf("realize: failed to relinquish: %v", err2)
			}
		}
	}()

	dbuf, err := proto.Marshal(&portal.DiagnosticList{Value: diags})
	if err != nil {
		return fmt.Errorf("marshal diags: %v", err)
	}

	// on an rlzError, this is likely to fail (like if rlz is nil)
	rbuf, err := proto.Marshal(rlz)
	if err != nil {
		if rlzError {
			rbuf = nil
		} else {
			return fmt.Errorf("marshal realization: %v", err)
		}
	}

	//TODO this needs a rollback, and more generally the allocation table should
	//     be moved into the storage library

	if !rlzError {
		err = realize.WriteTable(a)
		if err != nil {
			return fmt.Errorf("allocation table write: %v", err)
		}
	}

	// if this realization uses network emulation, record the number of endpoints on the emu server
	// this is used by the realization engine to load balance across emulation servers
	err = storage.RecordEndpoints(rlz)
	if err != nil {
		log.Errorf("record emulation endpoints: %v", err)
		// not a failure per-se, but could impact future realizations...
	}

	bucket := fmt.Sprintf("realize-%s-%s-%s", rq.Project, rq.Experiment, rq.Realization)

	found, err := storage.MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("minio check bucket: %v", err)
	}
	if !found {
		err := storage.MinIOClient.MakeBucket(
			context.TODO(), bucket, minio.MakeBucketOptions{})
		if err != nil {
			return fmt.Errorf("minio make bucket: %v", err)
		}
	}

	_, err = storage.MinIOClient.PutObject(
		context.TODO(),
		bucket,
		"diagnostics",
		bytes.NewReader(dbuf),
		int64(len(dbuf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio diagnostics put: %v", err)
	}

	_, err = storage.MinIOClient.PutObject(
		context.TODO(),
		bucket,
		"realization",
		bytes.NewReader(rbuf),
		int64(len(rbuf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio realization put: %v", err)
	}

	return nil

}

func GetResourcePool(project string) (*portal.Pool, error) {

	pools, err := storage.GetPools()
	if err != nil {
		return nil, err
	}

	for _, pool := range pools {
		for _, proj := range pool.Projects {
			if project == proj {
				return pool.Pool, nil
			}
		}
	}

	// if project is in no pool, it uses the default.
	pool := storage.NewPool("default") // "default" should be parameterized
	err = pool.Read()
	if err != nil {
		return nil, err
	}

	return pool.Pool, nil
}
