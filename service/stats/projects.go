package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

func projects() []string {
	var ret []string

	p, err := storage.ListProjects()
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("get projects")
		return nil
	}
	numProjects.Set(float64(len(p)))

	for _, s := range p {
		ret = append(ret, s.Name)
	}

	return ret
}
