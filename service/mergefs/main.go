package main

import (
	"os"
	"strconv"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/services/internal"
	"gitlab.com/mergetb/portal/services/pkg/storage"
)

var (
	ensurePeriod int64 = 60

	// Version filled in by build system.
	Version = ""
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()

	// Read optional ensure period form environment.
	value, ok := os.LookupEnv("ENSURE_PERIOD")
	if ok {
		log.Infof("setting ensure period to %s", value)
		ep, err := strconv.Atoi(value)
		if err != nil {
			log.Fatalf("Bad value for ENSURE_PERIOD: %s. Must be a number.", value)
		}
		ensurePeriod = int64(ep)
	} else {
		log.Infof("using default ensure period: %d", ensurePeriod)
	}
}

func main() {

	log.Infof("portal version: %s", Version)
	log.Infof("Starting " + recName + " service")

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage client init: %v", err)
	}

	runReconciler()
}
