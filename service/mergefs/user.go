package main

import (
	"path"
	"regexp"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/proto"
)

var (
	nameExp = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	userKey = regexp.MustCompile("^/users/" + nameExp + "$")
)

type UserTask struct {
	User string
}

func (ut *UserTask) Parse(k string) bool {

	tkns := userKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		ut.User = tkns[0][1]
		return true
	}

	return false
}

func (ut *UserTask) Create(value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Debug("[%s] handling new create", ut.User)

	u := new(portal.User)
	err := proto.Unmarshal(value, u)
	if err != nil {
		return reconcile.TaskMessageErrorf("[%s] user unmarshal", ut.User)
	}

	dirs := []string{
		path.Join(userRoot, u.Username),
		path.Join(userRoot, u.Username, ".ssh"),
	}

	for _, d := range dirs {

		err = mkDir(d, u.Uid, u.Gid, 0700)
		if err != nil {
			return reconcile.TaskMessageError(err)
		}
		err = setUserGroup(d, int(u.Uid), int(u.Gid))
		if err != nil {
			return reconcile.TaskMessageError(err)
		}
	}

	return nil
}

func (ut *UserTask) Update(prev, value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {
	// Could check if it uid or gid has changed or something? Don't know whyit would thuogh.
	return ut.Create(value, version, r)
}

func (ut *UserTask) Ensure(prev, value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {
	// Could check if it uid or gid has changed or something? Don't know whyit would thuogh.
	return ut.Create(value, version, r)
}

func (ut *UserTask) Delete(value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Debugf("[%s] handling delete user", ut.User)

	u := storage.NewUser(ut.User)
	err := u.Read()
	if err != nil {
		return reconcile.TaskMessageErrorf("[%s] user read", ut.User)
	}

	rmDir(path.Join(userRoot, u.Username))

	return nil
}
