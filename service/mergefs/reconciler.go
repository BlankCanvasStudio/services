package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	irec "gitlab.com/mergetb/portal/services/pkg/reconcile"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	recName     = "mergefs"
	fsRoot      = "/mergefs"
	userRoot    = fsRoot + "/user"
	projectRoot = fsRoot + "/project"
	orgRoot     = fsRoot + "/organizations"
	xdcRoot     = fsRoot + "/xdc"

	privkeyName = "merge_key"
	pubkeyName  = privkeyName + ".pub"
	certName    = privkeyName + "-cert.pub"
)

//
// Locations of things in mergefs
// user home dir: <userRoot>/<username>
// user pub keys: <userRoot>/<username>/.ssh/merge_key{,.pub}
//
// xdc host keys: <xdcRoot>/<host>/auth/merge_key{,.pub}
// xdc host cert: <xdcRoot>/<host>/auth/merge_cert
// xdc passwd: <xdcRoot>/<host>/passwd
// xdc group: <xdcRoot>/<host>/group
//

func runReconciler() {

	log.Infof("starting " + recName + " reconciler")

	// Things to watch:
	// * user events
	//   * add/rm create home dirs
	// * user ssh keys
	//   * writei/delete keys to/from user home dir
	// * project events
	//   * create/destroy - create/dest project dirs
	// * rally events?
	// *

	initMergeFS()

	t := &reconcile.TaskManager{
		Manager:                    recName,
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: irec.HeartbeatGracePeriodFromConfig(),
		Tasks: []*reconcile.Task{{
			Prefix:          "/users/",
			Name:            "User",
			Desc:            "MergeFS User Management",
			Actions:         &UserTask{},
			EnsureFrequency: ensurePeriod,
		}, {
			Prefix:          "/projects/",
			Name:            "Project",
			Desc:            "MergeFS Project Management",
			Actions:         &ProjectTask{},
			EnsureFrequency: ensurePeriod,
		}, {
			Prefix:          "/organizations/",
			Name:            "Organization",
			Desc:            "MergeFS Organization Management",
			Actions:         &OrganizationTask{},
			EnsureFrequency: ensurePeriod,
		}, {
			Prefix:          "/auth/ssh/",
			Name:            "Creds",
			Desc:            "MergeFS Credentials Management",
			Actions:         &CredTask{},
			EnsureFrequency: ensurePeriod,
		}, {
			Prefix:          "/xdcs/",
			Name:            "XcdFS",
			Desc:            "MergeFS XDC Management",
			Actions:         &XdcTask{},
			EnsureFrequency: ensurePeriod,
		}, {
			Prefix:          "/pubkeys/",
			Name:            "PubkeyFS",
			Desc:            "Manage public user keys in $HOME",
			Actions:         &PubkeyTask{},
			EnsureFrequency: ensurePeriod,
		}},
	}

	t.Run()
}

func initMergeFS() {

	ds := []string{userRoot, projectRoot, xdcRoot}

	for _, d := range ds {

		_, err := os.Stat(d)
		if os.IsNotExist(err) {

			err := os.Mkdir(d, 0775)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}
