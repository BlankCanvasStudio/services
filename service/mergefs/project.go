package main

import (
	"fmt"
	"path"
	"regexp"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"google.golang.org/protobuf/proto"
)

var (
	projectKey = regexp.MustCompile("^/projects/([a-zA-Z_]+[a-zA-Z0-9_]+)$")
)

type ProjectTask struct {
	Name string
}

func (pt *ProjectTask) Parse(k string) bool {

	tkns := projectKey.FindAllStringSubmatch(k, -1)
	if len(tkns) > 0 {
		pt.Name = tkns[0][1]
		return true
	}

	return false
}

func (pt *ProjectTask) Create(value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	p := new(portal.Project)
	err := proto.Unmarshal(value, p)
	if err != nil {
		return reconcile.TaskMessageErrorf("project unmarshal: %v", err)
	}

	err = mkProjectDir(p)
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func (pt *ProjectTask) Update(prev, value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {
	return pt.Create(value, version, r)
}

func (pt *ProjectTask) Ensure(prev, value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {
	return pt.Create(value, version, r)
}

func (pt *ProjectTask) Delete(value []byte, version int64, r *reconcile.Reconciler) *reconcile.TaskMessage {

	log.Infof("[%s] delete project", pt.Name)

	p := new(portal.Project)
	err := proto.Unmarshal(value, p)
	if err != nil {
		return reconcile.TaskMessageErrorf("project unmarshal: %v", err)
	}

	err = rmDir(path.Join(projectRoot, p.Name))
	if err != nil {
		return reconcile.TaskMessageError(err)
	}

	return nil
}

func mkProjectDir(p *portal.Project) error {

	var uid uint32 = 0
	c, err := projectCreator(p)
	if err != nil {
		log.Debug("no project creator set, using root UID for project dir")
	} else {
		uid = c.Uid
	}

	err = mkDir(path.Join(projectRoot, p.Name), uid, p.Gid, 0700)
	if err != nil {
		return fmt.Errorf("[%s] mkDir: %v", p.Name, err)
	}

	return nil
}

func projectCreator(p *portal.Project) (*storage.User, error) {

	for mid, m := range p.Members {
		if m.Role == portal.Member_Creator {

			u := storage.NewUser(mid)
			err := u.Read()
			if err != nil {
				return nil, fmt.Errorf("member/user read: %v", err)
			}

			return u, nil
		}
	}

	return nil, fmt.Errorf("[%s] no project creator found", p.Name)
}
