package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"

	"gitlab.com/mergetb/portal/services/pkg/storage"
	"golang.org/x/crypto/ssh"

	log "github.com/sirupsen/logrus"
)

// generateSSHKeyPair - generate a public private keypair
// suitable for use with SSH. Return values are encoded and ready
// to write to files in ~/.ssh.
func generateSSHKeyPair(id string) (string, string, error) {

	log.Infof("[%s] generating keys", id)

	privKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		log.Errorf("rss gen key: %+v", err)
		return "", "", err
	}

	err = privKey.Validate()
	if err != nil {
		log.Errorf("priv key validate: %+v", err)
		return "", "", err
	}

	pubKey, err := ssh.NewPublicKey(privKey.Public())
	if err != nil {
		log.Errorf("new pub key: %+v", err)
		return "", "", err
	}

	pub := ssh.MarshalAuthorizedKey(pubKey)

	priv := pem.EncodeToMemory(
		&pem.Block{
			Type:    "RSA PRIVATE KEY",
			Headers: nil,
			Bytes:   x509.MarshalPKCS1PrivateKey(privKey),
		},
	)
	return string(pub), string(priv), nil
}

// getUserSSHPubKey - returns the merge portal generated
// ssh keys for the user given in ssh-rsa format (file format)
func getUserSSHKeys(username string) (string, string, error) {

	keys := storage.NewSSHUserKeyPair(username)
	err := keys.Read()
	if err != nil {
		return "", "", err
	}

	return keys.Public, keys.Private, nil
}

func getUserSSHCert(username string) (string, error) {

	c := storage.NewSSHUserCert(username)
	err := c.Read()
	if err != nil {
		return "", err
	}

	return c.Cert, nil
}

func getHostSSHKeys(host string) (string, string, error) {

	keys := storage.NewSSHHostKeyPair(host)
	err := keys.Read()
	if err != nil {
		return "", "", err
	}

	return keys.Public, keys.Private, nil
}

func getUserHostCert(host string) (string, error) {

	c := storage.NewSSHHostCert(host)
	err := c.Read()
	if err != nil {
		return "", err
	}

	return c.Cert, nil
}
