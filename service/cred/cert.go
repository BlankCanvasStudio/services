package main

import (
	"encoding/json"
	"fmt"
	mrand "math/rand"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	caapi "github.com/smallstep/certificates/api"
	"github.com/smallstep/certificates/ca"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"golang.org/x/crypto/ssh"
	jose "gopkg.in/square/go-jose.v2"
	jwt "gopkg.in/square/go-jose.v2/jwt"
)

func generateUserSSHCert(username string) (string, error) {

	userKeys := storage.NewSSHUserKeyPair(username)
	err := userKeys.Read()
	if err != nil {
		return "", fmt.Errorf("no keys: %w", err)
	}

	ps := []string{username}
	cert, err := generateSSHCert(userKeys, ps)
	if err != nil {
		return "", fmt.Errorf("user cert gen: %w", err)
	}

	// Looks strange, but the ssh user cert format is the same as an ssh user key.
	c := ssh.MarshalAuthorizedKey(cert)
	if err != nil {
		return "", fmt.Errorf("cert marshal: %w", err)
	}

	// log.Infof("cert: \n%s\n", c)

	return string(c), nil
}

func generateHostSSHCert(principals []string, host string) (string, error) {

	keys := storage.NewSSHHostKeyPair(host)
	err := keys.Read()
	if err != nil {
		return "", fmt.Errorf("no keys: %w", err)
	}

	cert, err := generateSSHCert(keys, principals)
	if err != nil {
		return "", fmt.Errorf("host cert gen: %w", err)
	}

	c := ssh.MarshalAuthorizedKey(cert)

	return string(c), nil
}

func generateSSHCert(keys *storage.SSHKeyPair, principals []string) (*ssh.Certificate, error) {

	client, err := ca.NewClient(caEndpoint, ca.WithRootFile(caCertsDir+"/root_ca.crt"))
	if err != nil {
		return nil, fmt.Errorf("CA client connection: %w", err)
	}

	// Get the health of the CA
	health, err := client.Health()
	if err != nil {
		return nil, fmt.Errorf("CA Health: %w", err)
	}

	if health.Status != "ok" {
		return nil, fmt.Errorf("CA health is not OK")
	}

	tkn, err := getCAOneTimeToken(keys.Type(), client, keys.User, principals)
	if err != nil {
		return nil, fmt.Errorf("get CA OOT: %w", err)
	}

	// convert from ssh file to ssh pubkey bytes
	userPubKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(keys.Public))
	if err != nil {
		log.Infof("bad pub key: %s", keys.Public)
		return nil, fmt.Errorf("bad pub key format: %w", err)
	}

	log.Infof("principals: %s", strings.Join(principals, ","))

	req := &caapi.SSHSignRequest{
		PublicKey:  userPubKey.Marshal(),
		OTT:        tkn,
		CertType:   keys.Type(), // "host" or "user"
		Principals: principals,
		KeyID:      keys.User,
		// ValidBefore: before,
		// ValidAfter:  after,
		// AddUserPublicKey: userPubKey.Marshal(),
		// IdentityCSR:      identityCSR,
		// TemplateData:     templateData,
	}

	err = req.Validate()
	if err != nil {
		return nil, fmt.Errorf("ssh sign req validate: %w", err)
	}

	log.Infof("validated ssh sign req. requesting cert")
	log.Infof("sign req: %v", req)

	resp, err := client.SSHSign(req)
	if err != nil {
		return nil, fmt.Errorf("CA SSH Cert Req: %w", err)
	}

	log.Infof("resp: /n%+v/n", resp)
	// log.Infof("cert: /n%+v/n", resp.Certificate.Certificate)

	return resp.Certificate.Certificate, nil
}

func getCAOneTimeToken(kind string, client *ca.Client, uid string, principals []string) (string, error) {

	pw := caProvPw
	provName := caProvName

	resp, err := client.Provisioners()
	if err != nil {
		return "", err
	}

	kid, kbytes := "", ""
	for _, p := range resp.Provisioners {
		if p.GetName() == provName {
			kid, kbytes, _ = p.GetEncryptedKey()
			log.Debugf("Found cert provider: %s", provName)
			log.Debugf("kid: %s", kid)
			log.Debugf("key:%s\n", kbytes)
		}
	}

	if kid == "" {
		return "", fmt.Errorf("merge provisioner key not found")
	}

	// Decrypt the signing key
	enc, err := jose.ParseEncrypted(kbytes)
	if err != nil {
		log.Debugf("error decrypting signing key: %v", err)
		return "", err
	}
	log.Debugf("Decrypted signing key")

	log.Debugf("decrypting pw: %s", pw)
	key, err := enc.Decrypt(pw)
	if err != nil {
		log.Debugf("error decrypting pw: %v", err)
		return "", err
	}

	log.Debugf("decrypted key: %s", key)

	// rebuild the jose key
	signKey := new(jose.JSONWebKey)
	err = json.Unmarshal(key, signKey)
	if err != nil {
		log.Debugf("error unmarshalling provisioning key")
		return "", fmt.Errorf("error unmarshalling provisioning key")
	}

	log.Debugf("jose key %+v", signKey.Key)

	// The JWK contents for the step-ca token are not published. I've looked at the generated contents
	// though so can recreate it. I can either create by hand, or use a non-pkg call into the
	// smallstep cautils repo. Let's start with just building it by hand to minimize risky imports.

	// sample step-ca JWT provisioner OTT:
	// header:
	// {
	// 	"alg": "ES256",
	// 	"kid": "BOnKZZvmGR-unIv8TqvfqCNiKZ0rWB17bOUrMDCyUgQ",
	// 	"typ": "JWT"
	// }
	// payload:
	// {
	// "aud": "https://step-ca/1.0/ssh/sign",
	// "exp": 1611097164,
	// "iat": 1611096864,
	// "iss": "merge@mergetb.example.net",
	// "jti": "c5793e644074688f8d89156fc964ffcc1de312dd8966c6ae25e15dc4dcab8c0d",
	// "nbf": 1611096864,
	// "sha": "27be60b5c831472a75a49c6263a4e174540f0ce2216c2a4289fdb7ec32adcafd",
	// "step": {
	// 	"ssh": {
	// 		"certType": "user",
	// 		"keyID": "glawler@isi.edu",
	// 		"principals": [],
	// 		"validAfter": "",
	// 		"validBefore": ""
	// 	}
	// },
	// "sub": "glawler@isi.edu"
	// }
	type SshClaims struct {
		CertType    string   `json:"certType"`
		KeyID       string   `json:"keyID"`
		Principals  []string `json:"principals"`
		ValidBefore string   `json:"validBefore"`
		ValidAfter  string   `json:"validAfter"`
	}

	type StepSshClaims struct {
		Ssh SshClaims `json:"ssh"`
	}

	type StepClaims struct {
		jwt.Claims
		Step StepSshClaims `json:"step"`
	}

	now := time.Now()

	claims := StepClaims{
		jwt.Claims{
			Audience:  []string{caEndpoint + "/1.0/ssh/sign"},
			Expiry:    jwt.NewNumericDate(now.Add(time.Minute * 5)),
			ID:        randHex(64),
			IssuedAt:  jwt.NewNumericDate(now),
			Issuer:    provName,
			NotBefore: jwt.NewNumericDate(now),
			Subject:   uid,
		},
		StepSshClaims{
			SshClaims{
				CertType:    kind,
				KeyID:       uid,
				Principals:  principals,
				ValidBefore: "", // TODO: add actual times here
				ValidAfter:  "",
			},
		},
	}

	opts := new(jose.SignerOptions).WithType("JWT").WithHeader("kid", kid)
	signer, err := jose.NewSigner(
		jose.SigningKey{
			Algorithm: jose.ES256,
			Key:       signKey.Key,
		},
		opts,
	)

	tkn, err := jwt.Signed(signer).Claims(claims).CompactSerialize()
	if err != nil {
		return "", err
	}

	log.Debugf("OTT tkn: %+v", tkn)

	return tkn, nil
}

func randHex(n int) string {

	runes := []rune("0123456789abcdef")
	b := make([]rune, n)
	for i := range b {
		b[i] = runes[mrand.Intn(len(runes))]
	}

	return string(b)
}
