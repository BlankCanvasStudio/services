package main

import (
	log "github.com/sirupsen/logrus"
	irec "gitlab.com/mergetb/portal/services/pkg/reconcile"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

// ***************************************************************************
// TODO:
// * Delete user SSH certs (if they exist) when a user state is set to
//     non-active.
// * Implement reconciler status during actions.
// * Check the validity of the host and user certs from teh CA and set a
//   etcd lease for that amount of time in order to generate a new cert before
//   experation.
// ***************************************************************************

func runReconciler() {

	log.Info("Starting credentials management reconciler...")

	t := &reconcile.TaskManager{
		Manager:                    "credmgr",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: irec.HeartbeatGracePeriodFromConfig(),
		Tasks: []*reconcile.Task{{
			Prefix:  "/users/", // This should not be hardcoded, but gotten from storage.User.Bucket().
			Name:    "UserKeys",
			Desc:    "user key management",
			Actions: &UserTask{},
		}, {
			Prefix:  "/userstatus/",
			Name:    "UserCerts",
			Desc:    "user cert management",
			Actions: &UserStatusTask{},
		}, {
			Prefix:  "/xdcs/",
			Name:    "XDCCreds",
			Desc:    "Host credentials management",
			Actions: &XdcTask{},
		},
		}}

	t.Run()
}
