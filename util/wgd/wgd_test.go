package main

//
// import (
// 	"testing"
//
// 	wgd "gitlab.com/mergetb/portal/api/wgd/v1/go"
// )
//
// func setup() func() {
// 	// Since we are now adding routes, some things must exist.
// 	RunCommand("ip link add dummy type dummy")
// 	RunCommand("ip addr add 192.168.254.1 dev dummy")
// 	RunCommand("ip link set up dev dummy")
//
// 	return func() {
// 		RunCommand("ip link del dev dummy")
// 	}
// }
//
// func createIF(encid, namespace string, t *testing.T) error {
// 	key, err := CreateContainerInterface(
// 		encid,
// 		"192.168.254.2/24",
// 		"192.168.254.0/24",
// 		"localhost:36000",
// 		"SU4r57A5m0ycAjR1VoqOn2lFvsHxPe+kXtvpYpy0QXE=",
// 		namespace,
// 		uint8(code),
// 	)
// 	if err != nil {
// 		return err
// 	}
// 	if namespace != "" {
// 		t.Log("created wg in ns " + namespace + " if w/pubkey: " + key)
// 	} else {
// 		t.Log("created wg if w/pubkey: " + key)
// 	}
// 	return nil
// }
//
// func TestDevName(t *testing.T) {
// 	name, err := DevName("")
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	t.Logf("got dev name " + name)
// }
//
// func TestCreateClient(t *testing.T) {
// 	defer setup()() // setup and teardown
// 	encid := "proj.exp.real.mat"
// 	if err := createIF(encid, "", t); err != nil {
// 		t.Fatal(err)
// 	}
//
// 	names, err := GetWgDevNames("")
// 	if err != nil {
// 		t.Fatal("error getting dev name")
// 	}
// 	out, _ := RunOutput("wg show " + names[0])
// 	t.Log(string(out))
// 	out, _ = RunOutput("ip addr show " + names[0])
// 	t.Log(string(out))
//
// 	// remove the wg if
// 	RunCommand("ip link del dev " + names[0])
// }
//
// func TestCreateClientInNamespace(t *testing.T) {
// 	encid := "proj.exp.real.mat"
// 	ns := "roomenoughformuffins"
//
// 	RunCommand("ip netns add " + ns)
// 	defer RunCommand("ip netns del " + ns)
//
// 	if err := createIF(encid, ns, t); err != nil {
// 		t.Fatal(err)
// 	}
//
// 	names, err := GetWgDevNames(ns)
// 	if err != nil {
// 		t.Fatal("error getting dev name")
// 	}
//
// 	out, _ := RunOutput("ip netns exec " + ns + " wg show " + names[0])
// 	t.Log(string(out))
// }
//
// func TestDeleteClient(t *testing.T) {
// 	encid := "proj.exp.real"
// 	ns := ""
// 	if err := createIF(encid, ns, t); err != nil {
// 		t.Fatal(err)
// 	}
//
// 	names, err := GetWgDevNames(ns)
// 	if err != nil {
// 		t.Fatal("error getting dev name")
// 	}
//
// 	out, err := RunOutput("wg show " + names[0])
// 	t.Log(string(out))
// 	out, err = RunOutput("ip addr show " + names[0])
// 	t.Log(string(out))
//
// 	// Now delete if from default namespace.
// 	key, err := DoDeleteInterface(ns, uint8(wgd.WgNsCode_Namespace))
// 	if err != nil {
// 		RunCommand("ip link del " + names[0])
// 		t.Fatal(err)
// 	}
// 	t.Logf("deleted %s from interface", key)
// }
//
// func TestDeleteClientInNamespace(t *testing.T) {
// 	encid := "proj.exp.real"
// 	ns := "roomenoughformuffins"
//
// 	RunCommand("ip netns add " + ns)
// 	defer RunCommand("ip netns del " + ns)
//
// 	if err := createIF(encid, ns, t); err != nil {
// 		t.Fatal(err)
// 	}
//
// 	names, err := GetWgDevNames(ns)
// 	if err != nil {
// 		t.Fatal("error getting dev name")
// 	}
//
// 	out, _ := RunOutput("ip netns exec " + ns + " wg show " + names[0])
// 	t.Log(string(out))
//
// 	// Now delete it.
// 	key, err := DoDeleteInterface(ns, uint8(wgd.WgNsCode_Namespace))
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	t.Logf("deleted %s from interface", key)
// }
//
// func TestCreateDeleteGatewayIF(t *testing.T) {
// 	encid := "proj.exp.real"
// 	ns := "roomenoughformuffins"
//
// 	RunCommand("ip netns add " + ns)
// 	defer RunCommand("ip netns del " + ns)
//
// 	key, port, err := DoCreateGatewayInterface(encid, ns, "172.30.1.1/32")
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	t.Logf("created gateway - %d@%s", port, key)
//
// 	names, err := GetWgDevNames(ns)
// 	if err != nil {
// 		t.Fatal("error getting dev name")
// 	}
//
// 	out, _ := RunOutput("ip netns exec " + ns + " wg show " + names[0])
// 	t.Log(string(out))
//
// 	key, err = DoDeleteInterface(ns, uint8(wgd.WgNsCode_Namespace))
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	t.Logf("deleted %s from interface", key)
// }
//
// func TestIdempotentCreateGwIf(t *testing.T) {
// 	encid := "proj.exp.real"
// 	ns := "roomenoughformuffins"
//
// 	RunCommand("ip netns add " + ns)
// 	defer RunCommand("ip netns del " + ns)
//
// 	for i := 1; i <= 5; i++ {
// 		key, port, err := DoCreateGatewayInterface(encid, ns, "172.30.1.1/32")
// 		if err != nil {
// 			t.Fatal(err)
// 		}
// 		t.Logf("created gateway - %d@%s", port, key)
// 	}
//
// 	DoDeleteInterface(ns, uint8(wgd.WgNsCode_Namespace))
// }
//
// func TestStatus(t *testing.T) {
// 	defer setup()() // setup and teardown
// 	encid := "proj.exp.real"
// 	ns := ""
// 	if err := createIF(encid, ns, t); err != nil {
// 		t.Fatal(err)
// 	}
// 	defer DoDeleteInterface(ns, uint8(wgd.WgNsCode_Namespace))
//
// 	status, err := DoInterfaceStatus(encid, ns, 0)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	t.Logf("status:\n%s", status)
// }

// func TestSaveConfig(t *testing.T) {
// 	e0, e1, e2, n1, n2 := "enc0", "enc1", "enc2", "fakens1", "fakens2"
// 	confPath := "./config"
//
// 	for _, n := range []string{n1, n2} {
// 		RunCommand("ip netns add " + n)
// 		defer RunCommand("ip netns del " + n)
// 	}
//
// 	// Create a few wg interfaces.
// 	if err := createIF(e2, n2, t); err != nil {
// 		t.Fatal(err)
// 	}
// 	if err := createIF(e1, n1, t); err != nil {
// 		t.Fatal(err)
// 	}
// 	if err := createIF(e0, "", t); err != nil {
// 		t.Fatal(err)
// 	}
//
// 	// save all configs.
// 	conf := &WgdConfig{ConfPath: confPath}
// 	for _, wgdev := range WgIfMap {
// 		conf.SaveConfig(wgdev)
// 	}
//
// 	// kill all wg interfaces and recreate from configuration.
// 	for _, e := range []string{e0, e1, e2} {
// 		DoDeleteInterface(e)
// 	}
//
// 	// Loading from config is a WIP.
// 	// rm config for now.
// 	os.RemoveAll(confPath)
// }
