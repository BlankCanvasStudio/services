package main

import (
	"fmt"
	"math/rand"
	"net"
	"runtime"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/vishvananda/netns"

	"golang.org/x/sys/unix"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	wgd "gitlab.com/mergetb/api/wgd/v1/go"

	"gitlab.com/mergetb/tech/rtnl"
)

var (
	staticiCIDRRoutes = []string{
		"192.168.254.0/24",
		"172.30.0.0/16",
	}

	wgKeepaliveIntervalSec = 25
)

func getDeviceAccessAddr(netns string, dev *wgtypes.Device) (string, error) {
	// open a context into the netns
	ctx, err := rtnl.OpenContext(netns)
	if err != nil {
		return "", fmt.Errorf("open netns %s: %+v", netns, err)
	}
	defer ctx.Close()

	lnk, err := rtnl.GetLink(ctx, dev.Name)
	if err != nil {
		return "", fmt.Errorf("read link %s: %+v", dev.Name, err)
	}

	addrs, err := lnk.Addrs(ctx)
	if err != nil {
		return "", fmt.Errorf("read addrs %s: %+v", dev.Name, err)
	}

	for _, a := range addrs {
		log.Infof("ifx %s address %v", dev.Name, a.Info.Address.String())
	}

	switch len(addrs) {
	case 0:
		log.Warnf("no addr found on WG ifx %s", dev.Name)
		return "", nil
	case 1:
	default:
		log.Warnf("%d addrs found on WG ifx %s: %+v", dev.Name, addrs)
	}

	return addrs[0].Info.Address.IP.String(), nil
}

func CreateContainerInterface(enclaveid, accessaddr, containerid string, peers []*portal.WgIfConfig,
) (string, error) {

	pubkey, err := createInterface(enclaveid, accessaddr, containerid)
	if err != nil {
		return "", err
	}

	if len(peers) > 0 {
		err = AddWgPeers(containerid, peers, true)
		if err != nil {
			return "", err
		}
	}

	return pubkey, nil
}

func createInterface(enclaveid, accessaddr, containerid string) (string, error) {
	var htx *rtnl.Context = nil
	var ctx *rtnl.Context = nil
	var lnk *rtnl.Link
	var err error

	// function to make sure a partially configured device is not leaked
	defer func() {
		if err != nil && lnk != nil {
			if htx != nil {
				lnk.Absent(htx)
			}
			if ctx != nil {
				lnk.Absent(ctx)
			}
		}

		if htx != nil {
			htx.Close()
		}

		if ctx != nil {
			ctx.Close()
		}
	}()

	l := log.WithFields(log.Fields{
		"enclaveid":   enclaveid,
		"accessaddr":  accessaddr,
		"containerid": containerid,
	})

	ns, err := cid2NSName(containerid)
	if err != nil {
		l.Errorf("containerid to namespace err: %+v", err)
		return "", err
	}

	// generate a name for the wg device, ensuring uniqueness
	dev, err := generateDeviceName(ns)
	if err != nil {
		l.Errorf("generate name: %+v", err)
		return "", err
	}

	// open a context in the default netns where the wgdev will be initialized
	htx, err = rtnl.OpenDefaultContext()
	if err != nil {
		l.Errorf("open default context: %+v", err)
		return "", err
	}
	defer htx.Close()

	lnk = rtnl.NewLink()
	lnk.Info.Name = dev
	lnk.Info.Mtu = 1420
	lnk.Info.Wireguard = &rtnl.Wireguard{}

	// create link in default netns
	err = lnk.Present(htx)
	if err != nil {
		l.Errorf("create wg ifx %s: %+v", dev, err)
		return "", err
	}

	// generate keypair for the WG device
	priv, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		l.Errorf("create wg private key: %+v", err)
		return "", err
	}

	// wg configuration
	c, err := wgctrl.New()
	if err != nil {
		l.Errorf("wgctrl.New(): %+v", err)
		return "", err
	}
	defer c.Close()

	cfg := wgtypes.Config{
		PrivateKey: &priv,
	}
	err = c.ConfigureDevice(dev, cfg)
	if err != nil {
		l.Errorf("wgtrl.ConfigureDevice(): %+v", err)
		return "", err
	}

	// UP the inferface, which needs to be done in the base netns
	err = lnk.Up(htx)
	if err != nil {
		l.Errorf("up wg ifx %s: %+v", dev, err)
		return "", err
	}

	// open a context into the the container's netns
	ctx, err = rtnl.OpenContext(ns)
	if err != nil {
		l.Errorf("open netns %s context: %+v", ns, err)
		return "", err
	}

	// move the link from the base netns into the container
	err = lnk.ChangeContext(htx, ctx)
	if err != nil {
		l.Errorf("change context to netns %s: %+v", ns, err)
		return "", err
	}

	htx.Close()
	htx = nil

	// moving into the new netns removes addresses and DOWNs the interface. So bring back up, add
	// address, and add routes now
	err = lnk.Up(ctx)
	if err != nil {
		l.Errorf("up wg ifx %s: %+v", dev, err)
		return "", err
	}

	ip, ipnet, err := net.ParseCIDR(accessaddr + "/32")
	if err != nil {
		l.Errorf("ParseCIDR(%s): %+v", accessaddr, err)
		return "", err
	}

	// add address
	addr := rtnl.NewAddress()
	addr.Info.Address = &net.IPNet{
		IP:   ip,
		Mask: ipnet.Mask,
	}

	err = lnk.AddAddr(ctx, addr)
	if err != nil {
		l.Errorf("add address %s dev %s: %+v", accessaddr, dev, err)
		return "", err
	}

	// add routes (TODO: these should come from the portal, not be hardcoded in this file)
	for _, rt := range staticiCIDRRoutes {
		rip, ripnet, err := net.ParseCIDR(rt)
		if err != nil {
			l.Errorf("ParseCIDR(%s): %+v", rt, err)
			return "", err
		}

		length, _ := ripnet.Mask.Size()
		route := &rtnl.Route{
			Hdr: unix.RtMsg{
				Family:   unix.AF_INET,
				Dst_len:  uint8(length),
				Protocol: unix.RTPROT_STATIC,
				Scope:    unix.RT_SCOPE_LINK,
			},
			Dest: rip,
			Oif:  uint32(lnk.Msg.Index),
		}
		err = route.Present(ctx)
		if err != nil {
			l.Errorf("add route for %s dev %s: %+v", rt, dev, err)
			return "", err
		}

		l.Debugf("added route %s for dev %s", rt, dev)
	}

	l.Debugf("successfully created wg interface: %s", dev)

	return priv.PublicKey().String(), nil
}

func DeleteContainerInterface(containerid string) (string, error) {
	l := log.WithFields(log.Fields{
		"containerid": containerid,
	})
	l.Debug("delete interface")

	netns, err := cid2NSName(containerid)
	if err != nil {
		return "", err
	}

	cnt, hostns, err := setNetnsName(netns)
	if err != nil {
		l.Errorf("set netns %s: %+v", netns, err)
		return "", err
	}
	// make sure we reset to host namespace by calling the provided continuation
	defer cnt(hostns)

	// wg configuration
	c, err := wgctrl.New()
	if err != nil {
		l.Errorf("wgctrl.New(): %+v", err)
		return "", err
	}
	defer c.Close()

	// see if a WG device exists
	devs, err := c.Devices()
	if err != nil {
		l.Errorf("wgctrl.Devices(): %+v", err)
		return "", err
	}

	if len(devs) == 0 {
		l.Warn("no WG interfaces exist")
		return "", nil
	} else if len(devs) > 1 {
		l.Warnf("%d WG devices found; only 1 expected", len(devs))
	}

	// open a context into the netns
	ctx, err := rtnl.OpenContext(netns)
	if err != nil {
		l.Errorf("open context %s: %+v", netns, err)
		return "", err
	}
	defer ctx.Close()

	// delete first ifx
	dev := devs[0]
	lnk := rtnl.NewLink()
	lnk.Info.Name = dev.Name
	err = lnk.Absent(ctx)
	if err != nil {
		l.Errorf("remove %s: %+v", dev.Name, err)
		return "", err
	}

	return dev.PublicKey.String(), nil
}

func GetContainerInterface(containerid string) (*wgd.GetContainerInterfaceResponse, error) {
	l := log.WithFields(log.Fields{
		"containerid": containerid,
	})
	l.Debug("get interface")

	netns, err := cid2NSName(containerid)
	if err != nil {
		return nil, err
	}

	cnt, hostns, err := setNetnsName(netns)
	if err != nil {
		l.Errorf("set netns %s: %+v", netns, err)
		return nil, err
	}
	// make sure we reset to host namespace by calling the provided continuation
	defer cnt(hostns)

	// wg configuration
	c, err := wgctrl.New()
	if err != nil {
		l.Errorf("wgctrl.New(): %+v", err)
		return nil, err
	}
	defer c.Close()

	// see if a WG device exists
	devs, err := c.Devices()
	if err != nil {
		l.Errorf("wgctrl.Devices(): %+v", err)
		return nil, err
	}

	if len(devs) == 0 {
		l.Warn("no WG interfaces exist")
		return nil, nil
	} else if len(devs) > 1 {
		l.Warnf("%d WG devices found; only 1 expected", len(devs))
	}

	dev := devs[0]
	addr, err := getDeviceAccessAddr(netns, dev)
	if err != nil {
		l.Error(err)
		return nil, err
	}

	return &wgd.GetContainerInterfaceResponse{
		Accessaddr:  addr,
		Containerid: containerid,
		Key:         dev.PublicKey.String(),
	}, nil
}

func getWgPeers(containerid string) ([]*portal.WgIfConfig, error) {
	l := log.WithFields(log.Fields{
		"containerid": containerid,
	})
	l.Debug("getWgPeers")

	ns, err := cid2NSName(containerid)
	if err != nil {
		l.Errorf("containerid to namespace err: %+v", err)
		return nil, err
	}

	// change into the netns
	cnt, hostns, err := setNetnsName(ns)
	if err != nil {
		l.Errorf("set netns %s: %+v", ns, err)
		return nil, err
	}
	// make sure we reset to host namespace by calling the provided continuation
	defer cnt(hostns)

	// wg configuration
	c, err := wgctrl.New()
	if err != nil {
		l.Errorf("wgctrl.New(): %+v", err)
		return nil, err
	}
	defer c.Close()

	// see if the device exists
	devs, err := c.Devices()
	if err != nil {
		l.Errorf("wgctrl.Devices(): %+v", err)
		return nil, err
	}

	if len(devs) == 0 {
		l.Warnf("no WG interfaces exist")
		return nil, nil
	} else if len(devs) > 1 {
		l.Warnf("%d WG devices found; only 1 expected", len(devs))
	}

	dev := devs[0]
	addr, err := getDeviceAccessAddr(ns, dev)
	if err != nil {
		l.Error(err)
		return nil, err
	}

	var peers []*portal.WgIfConfig
	for _, p := range dev.Peers {

		var ips []string
		for _, ip := range p.AllowedIPs {
			ips = append(ips, ip.String())
		}

		peers = append(peers, &portal.WgIfConfig{
			Endpoint:   p.Endpoint.String(),
			Key:        p.PublicKey.String(),
			Allowedips: ips,
			Accessaddr: addr,
		})
	}

	return peers, nil
}

func modWgPeers(containerid string, peers []*portal.WgIfConfig, remove, exclusive bool) error {
	l := log.WithFields(log.Fields{
		"containerid": containerid,
		"peers":       peers,
		"remove":      remove,
	})
	l.Debug("modWgPeers")

	ns, err := cid2NSName(containerid)
	if err != nil {
		l.Errorf("containerid to namespace err: %+v", err)
		return err
	}

	// change into the netns
	cnt, hostns, err := setNetnsName(ns)
	if err != nil {
		l.Errorf("set netns %s: %+v", ns, err)
		return err
	}
	// make sure we reset to host namespace by calling the provided continuation
	defer cnt(hostns)

	// wg configuration
	c, err := wgctrl.New()
	if err != nil {
		l.Errorf("wgctrl.New(): %+v", err)
		return err
	}
	defer c.Close()

	// see if the device exists
	devs, err := c.Devices()
	if err != nil {
		l.Errorf("wgctrl.Devices(): %+v", err)
		return err
	}

	if len(devs) == 0 {
		err = fmt.Errorf("no WG interfaces exist")
		l.Error(err)
		return err
	} else if len(devs) > 1 {
		l.Warnf("%d WG devices found; only 1 expected", len(devs))
	}

	// build list of peers
	wgpeers := []wgtypes.PeerConfig{}
	for _, peer := range peers {
		pkey, err := wgtypes.ParseKey(peer.Key)
		if err != nil {
			l.Errorf("parse wg key %s: %+v", peer.Key, err)
			return err
		}

		var ips []net.IPNet
		for _, ip := range peer.Allowedips {
			_, ipnet, err := net.ParseCIDR(ip)
			if err != nil {
				l.Errorf("parseCIDR(%s): %+v", ip, err)
				return err
			}

			ips = append(ips, *ipnet)
		}

		wgep, err := WgEndpointParse(peer.Endpoint)
		if err != nil {
			l.Errorf("WgEndpointParse(%s): %+v", peer.Endpoint, err)
			return err
		}

		t := time.Duration(wgKeepaliveIntervalSec) * time.Second
		wgp := wgtypes.PeerConfig{
			PublicKey:  pkey,
			AllowedIPs: ips,
			Remove:     remove,
			Endpoint: &net.UDPAddr{
				IP:   wgep.IP,
				Port: wgep.Port,
			},
			PersistentKeepaliveInterval: &t,
		}
		wgpeers = append(wgpeers, wgp)

		l.Debugf("generated wgpeer: %+v", wgp)
	}

	cfg := wgtypes.Config{
		ReplacePeers: exclusive,
		Peers:        wgpeers,
	}

	// modify all devices
	var errs []error
	for _, dev := range devs {
		err = c.ConfigureDevice(dev.Name, cfg)
		if err != nil {
			l.Errorf("wgtrl.ConfigureDevice(): %+v", err)
			errs = append(errs, err)
		}
	}

	if len(errs) == 0 {
		return nil
	}

	return fmt.Errorf("%+v", errs)
}

func AddWgPeers(containerid string, peers []*portal.WgIfConfig, exclusive bool) error {
	return modWgPeers(containerid, peers, false, exclusive)
}

func DeleteWgPeers(containerid string, peers []*portal.WgIfConfig) error {
	return modWgPeers(containerid, peers, true, false)
}

func GetWgPeers(containerid string) ([]*portal.WgIfConfig, error) {
	return getWgPeers(containerid)
}

func randName() string {
	const size = 4
	const letters = "abcedfghijklmnopqrstuvwxyz"
	s := make([]byte, size)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return "wgd" + string(s)
}

func generateDeviceName(namespace string) (string, error) {
	// Did a bit of work trolling through namespaces calling 'ip link...'
	// and just realized using random names is much less error prone.
	htx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return "", fmt.Errorf("failed to open default netns: %+v", err)
	}
	defer htx.Close()

	// open a link in the netns. The name must be unique in the default netns and the target
	// netns for the device
	ctx, err := rtnl.OpenContext(namespace)
	if err != nil {
		return "", fmt.Errorf("failed to open netns %s context: %+v", namespace, err)
	}
	defer ctx.Close()

	hlinks, err := rtnl.ReadLinks(htx, nil)
	if err != nil {
		return "", fmt.Errorf("failed to read links in default netns: %+v", err)
	}

	clinks, err := rtnl.ReadLinks(ctx, nil)
	if err != nil {
		return "", fmt.Errorf("failed to read links in netns %s: %+v", namespace, err)
	}

	tries := 0
	for tries < 10 {
		name := randName()

		// ensure this name does not exist
		for _, lnk := range append(hlinks, clinks...) {
			if lnk.Info.Name == name {
				// hit a name in use, try again
				tries++
				continue
			}
		}

		return name, nil
	}

	return "", fmt.Errorf("unable to generate valid device name")
}

// set the netns to 'name' and return a "continuation" that will reset to the current state when
// called
func setNetnsName(name string) (func(netns.NsHandle), netns.NsHandle, error) {
	var err error

	// Lock the OS thread to prevent namespace switching
	// See: https://pkg.go.dev/github.com/vishvananda/netns#section-readme
	runtime.LockOSThread()

	// if anything errs, unlock the OS thread
	defer func() {
		if err != nil {
			runtime.UnlockOSThread()
		}
	}()

	curns, err := netns.Get()
	if err != nil {
		return nil, 0, fmt.Errorf("netns.Get(): %v", err)
	}

	ns, err := netns.GetFromName(name)
	if err != nil {
		return nil, 0, fmt.Errorf("netns.GetFromName(): %v", err)
	}

	err = netns.Set(ns)
	if err != nil {
		return nil, 0, fmt.Errorf("netns.Set(): %v", err)
	}

	// return a "continuation" that will reset to the current netns
	return func(handle netns.NsHandle) {
		netns.Set(handle)
		handle.Close()
		runtime.UnlockOSThread()
	}, curns, nil
}
