package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"

	"gitlab.com/mergetb/portal/services/internal"
)

var Version = "undefined"

func init() {
	internal.InitLogging()
}

func main() {
	listen := flag.String("listen", "0.0.0.0:36000", "Where and how to listen as a daemon")
	// cert := flag.String("cert", "/etc/wgd/wgd.pem", "Path to the wgd cert")
	// key := flag.String("key", "/etc/wgd/wgd-key.pem", "Path to the wgd private key")
	version := flag.Bool("version", false, "Show version and exit.")
	flag.Parse()

	if *version == true {
		fmt.Println(Version)
		os.Exit(0)
	}

	rand.Seed(time.Now().UTC().UnixNano())

	// err := Daemonize(*listen, *cert, *key)
	err := Daemonize(*listen, "", "")
	if err != nil {
		fmt.Println("Error starting daemon: ", err)
		os.Exit(666)
	}
	os.Exit(0)
}
