package main

import (
	"context"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/portal/services/pkg/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"go.etcd.io/etcd/client/v3"
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)

const (
	version = "0.0"
)

var (
	logLevel string
)

var root = &cobra.Command{
	Use:     "status",
	Short:   "View or change reconcilation status",
	Version: version,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		lvl, err := log.ParseLevel(logLevel)
		if err != nil {
			log.Fatalf("bad log level: %s", err)
		}
		log.SetLevel(lvl)
	},
}

var del = &cobra.Command{
	Use:   "delete <key>",
	Short: "Delete the given status",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		deleteStatus(args[0])
	},
}

var show = &cobra.Command{
	Use:   "show",
	Short: "show all status",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		showStatus()
	},
}

func main() {

	log.SetFormatter(&log.TextFormatter{
		DisableTimestamp: true,
	})

	cobra.EnablePrefixMatching = true

	root.PersistentFlags().StringVar(
		&logLevel, "loglevel", "info", "Level to log at. One of "+strings.Join(logLevels(), ", "),
	)

	err := storage.InitPortalEtcdClient()
	if err != nil {
		log.Fatalf("storage client init: %v", err)
	}

	root.AddCommand(show)
	root.AddCommand(del)

	root.Execute()
}

func showStatus() {
	c := storage.EtcdClient

	resp, err := c.Get(
		context.TODO(),
		"/STATUS",
		clientv3.WithLease(0),
		clientv3.WithPrefix(),
	)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(tw, "Reconciler\tEvent\tResult\tKey\tWorker\tVersion\tInfo\t\n")

	for _, kv := range resp.Kvs {

		s, err := reconcile.StatusFromBytes(kv.Value)
		if err != nil {
			log.Fatal(err)
		}

		key := strings.TrimLeft(string(kv.Key), "/STATUS/")

		fmt.Fprintf(tw,
			"%s\t%s\t%s\t%s\t%s\t%d\t%s\n",
			s.Reconciler,
			s.Event,
			s.Result.String(),
			key,
			s.Worker,
			s.Version,
			s.Info,
		)
	}

	tw.Flush()
}

func deleteStatus(k string) {

	// example key: mergefs//auth/ssh/cert/host/barf.glawler
	tkns := strings.SplitN(k, "/", 2)

	log.Debugf("key: %s\nsubkey: %s", tkns[0], tkns[1])

	r := reconcile.NewReconciler("/"+tkns[0], tkns[1], storage.EtcdClient)
	if !r.DelStatus() {
		log.Fatal("error deleting %s/%s", tkns[0], tkns[1])
	}
}

func logLevels() []string {

	r := []string{}
	for _, l := range log.AllLevels {
		r = append(r, l.String())
	}

	return r
}
