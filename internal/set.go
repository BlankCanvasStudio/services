package internal

import (
	"fmt"
	"encoding/json"
)

type Set struct {
	Map map[uint64]struct{}
	Name string
	Ver int64
}

// create a new set with range [offset, offset+size) in the set
func NewSet(name string) *Set {
	m := make(map[uint64]struct{})

	return &Set{
		Map: m,
		Name: name,
		Ver: 0,
	}
}

// check for set membership
func (s Set) Contains(v uint64) bool {
	_, contains := s.Map[v]
	return contains
}

func (s Set) GetAnyElement() (uint64, error) {
	if len(s.Map) == 0 {
		return 0, fmt.Errorf("set '%s' is empty", s.Name)
	}

	// get "random" element
	for idx, _ := range(s.Map) {
		return idx, nil
	}

	// cannot reach
	return 0, fmt.Errorf("set '%s' is corrupted", s.Name)
}

// delete an arbitrary element from the set and return it
func (s Set) DelAnyElement() (uint64, error) {
	v, err := s.GetAnyElement()
	if err != nil {
		return 0, err
	}

	delete(s.Map, v)
	return v, nil
}

// delete a specific element from the set
func (s Set) DelElement(v uint64) error {
	if !s.Contains(v) {
		return fmt.Errorf("set '%s' does not have element '%d'", s.Name, v)
	}

	delete(s.Map, v)
	return nil
}

// add an element to the set
func (s Set) AddElement(v uint64) error {
	if s.Contains(v) {
		return fmt.Errorf("set '%s' already has element '%d'", s.Name, v)
	}

	s.Map[v] = struct{}{}
	return nil
}

func (s Set) Marshal() ([]byte, error) {
	return json.Marshal(s)
}

func (s Set) Unmarshal(bytes []byte) error {
	if len(bytes) == 0 {
		s = *NewSet("nil")
		return nil
	}

	return json.Unmarshal(bytes, &s)
}
